/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using Gtk;
 
namespace Gnome.Scan {
	public class AcquisitionWidget : Alignment {
		private ProgressBar progressbar;
		private Label status_label;

		public Job		job			{ get; set construct; }
		public string	primary		{ get; set construct; }
		public string	secondary	{ get; set construct; }
		public string	icon_name	{ get; set construct; }

		construct {
			this.xscale = 0;
			this.yscale = 0;
			this.border_width = 6;

			var box = new VBox(false, 6);
			this.add(box);

			var head_box = new HBox(false, 6);
			box.pack_start(head_box, false, true, 0);

			var image = new Gtk.Image.from_icon_name(this.icon_name, IconSize.DIALOG);
			image.set_alignment(0, 0);
			image.set_padding(6, 6);
			head_box.pack_start(image, false, true, 0);

			var message_box = new VBox(false, 4);
			head_box.pack_start(message_box, false, true, 0);

			var primary_label = new Label(null);
			primary_label.set_markup("<b><big>%s</big></b>".printf(this.primary));
			primary_label.set_alignment(0, 0);
			message_box.pack_start(primary_label, false, true, 0);

			var secondary_label = new Label(this.secondary);
			secondary_label.set_alignment(0, 0);
			secondary_label.set_line_wrap(true);
			message_box.pack_start(secondary_label, true, true, 0);

			this.progressbar = new ProgressBar();
			box.pack_start(this.progressbar, false, true, 0);

			this.status_label = new Label(null);
			this.status_label.set_alignment(0, 0);
			this.status_label.set_line_wrap(true);
			box.pack_start(this.status_label, false, true, 0);
		}

		public AcquisitionWidget(Job job, string primary, string secondary, string? icon_name)
		{
			if (icon_name == null)
				icon_name = "scanner";

			this.job = job;
			this.primary = primary;
			this.secondary = secondary;
			this.icon_name = icon_name;
		}

		// to be add to a timeout source
		public bool monitor_job()
		{
			this.progressbar.set_fraction(double.min(this.job.progress, 1.0));
			return this.job.is_running();
		}
	}
}