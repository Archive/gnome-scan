/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using Gtk;

namespace Gnome.Scan {
    public class OptionBox : VBox {
		public string label {get; construct set;}
		public bool expand {set; get; default = false;}

		// The actual box containing option widget.
		Table table;
		int child_count = 0;
		int child_visible_count = 0;
		int child_expanding_count = 0;
		new HashTable<Gnome.Scan.Option,OptionWidget> children;
		HashTable<OptionWidget,Gtk.Label> labels;

		construct {
			this.spacing = 6;
			this.no_show_all = true;
			Label label = new Label(null);
			label.set_markup("<b>%s</b>".printf(this.label));
			label.set_alignment((float)0, (float)0.5);
			this.pack_start(label, false, false, 0);
			Alignment alignment = new Alignment(0, 0, 1, 1);
			alignment.set_padding(0, 0, 24, 0);
			this.pack_start(alignment, true, true, 0);
			this.table = new Table(0, 2, false);
			alignment.add(this.table);

			children = new HashTable<Gnome.Scan.Option, OptionWidget>(GLib.direct_hash, GLib.direct_equal);
			labels = new HashTable<OptionWidget, Gtk.Label>(GLib.direct_hash, GLib.direct_equal);
		}

		public OptionBox(string label)
		{
			this.label = label;
		}

		public void pack_option(Option option)
		{
			GLib.Type wtype = option_manager.get_widget_type_for(option);
			if (!wtype.is_a(typeof(Gtk.Widget))) {
				warning("No widget for option %s", option.name);
				return;
			}

			OptionWidget widget = (OptionWidget) new Gtk.Widget(wtype, "option", option);
			widget.hide.connect(this.on_option_widget_visibility_changed);
			widget.show.connect(this.on_option_widget_visibility_changed);

			this.child_count++;
			// compute attach option
			var opts = AttachOptions.FILL;
			if (widget.expand) {
				opts|=AttachOptions.EXPAND;
			}

			// insert directly the widget, spanning two columns
			if (widget.no_label) {
				this.table.attach(widget, 0, 2, this.child_count-1, this.child_count, opts, opts, 0, 0);
			}
			else {
				// translator: %s is the name of an option and is
				// prepended before the option widget. Accord ":" to
				// locale typographic rules.
				Label label = new Label(_("%s:").printf(option.title));
				label.tooltip_text = option.desc;
				label.set_alignment((float)0, (float)0.5);
				label.no_show_all = widget.no_show_all;
				this.labels.insert(widget, label);
				this.table.attach(label, 0, 1, this.child_count-1, this.child_count,
								  AttachOptions.FILL, AttachOptions.FILL, 0, 0);
				this.table.attach(widget, 1, 2, this.child_count-1, this.child_count,
								  AttachOptions.FILL|AttachOptions.EXPAND, opts, 0, 0);
			}

			this.children.insert(option, widget);

			// consider option visible to hide it after attach
			if (!option.active)
				this.child_visible_count++;

			this.on_option_widget_visibility_changed(widget);
		}

		// return whether the box is empty
		public bool unpack_option(Option option)
		{
			weak OptionWidget widget = this.children.lookup(option);
			if (widget == null)
				return false;

			// hide widget
			widget.no_show_all = true;
			widget.hide();

			// destroy label
			weak Label label = this.labels.lookup(widget);
			if (label != null) {
				this.table.remove(label);
				this.labels.remove(widget);
			}

			// destroy widget
			this.table.remove(widget);
			this.children.remove(option);

			return this.table.get_children().length() == 0;
		}

		private void on_option_widget_visibility_changed(Widget widget)
		{
			// ignore no_show_all widget.
			if (widget.no_show_all)
				return;

			var label = this.labels.lookup((OptionWidget)widget);
			if (label != null) {
				label.no_show_all = widget.no_show_all;
				if (widget.visible) {
					label.show();
				}
				else {
					label.hide();
				}
			}

			if (((OptionWidget)widget).expand) {
				this.child_expanding_count += widget.visible ? +1 : -1;
				var parent = (Container) this.parent;
				this.expand = this.child_expanding_count > 0;
				parent.child_set(this, "expand", this.expand);
			}

			this.child_visible_count += widget.visible ? +1 : -1;
			if (this.child_visible_count == 0) {
				this.no_show_all = true;
				this.hide();
			}
			else {
				this.no_show_all = false;
				this.show_all();
			}
		}
    }
}
