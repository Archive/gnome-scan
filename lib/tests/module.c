/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include <gnome-scan.h>

static void
name_extract ()
{
	gchar* name;
	GError *error = NULL;

	name = gnome_scan_module_manager_module_name_from_filename("libgsane.so", &error);

	g_assert_no_error(error);
	g_assert_cmpstr(name, ==, "gsane");

	g_free (name);
}

static void
name_validate_so ()
{
	const gchar* name = "libgsane.so";
	gboolean valid;

	valid = gnome_scan_module_manager_is_valid_module_name(name);

	g_assert (valid);
}

static void
name_validate_la ()
{
	const gchar* name = "libgsane.la";
	gboolean valid;

	valid = gnome_scan_module_manager_is_valid_module_name(name);

	g_assert (valid);
}

static void
name_validate_a ()
{
	const gchar* name = "libgsane.a";
	gboolean valid;

	valid = gnome_scan_module_manager_is_valid_module_name(name);

	g_assert (!valid);
}

static void
name_validate_not_lib ()
{
	const gchar* name = "gsane.a";
	gboolean valid;

	valid = gnome_scan_module_manager_is_valid_module_name(name);

	g_assert (!valid);
}


gint
main(gint argc, gchar**argv)
{
	g_set_prgname("module");
	g_type_init();
	g_test_init(&argc, &argv, NULL);
	gnome_scan_init_test(&argc, &argv);
	g_test_add_func("/module/name/extract", name_extract);
	g_test_add_func("/module/name/validate/so", name_validate_so);
	g_test_add_func("/module/name/validate/la", name_validate_la);
	g_test_add_func("/module/name/validate/a", name_validate_a);
	g_test_add_func("/module/name/validate/not_lib", name_validate_not_lib);
	return g_test_run();
}
