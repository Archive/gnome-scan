/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include <gnome-scan.h>

static void
unit_pixel ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_PIXEL;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "px");
}

static void
unit_point ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_POINTS;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "pt");
}

static void
unit_millimeter ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_MM;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "mm");
}

static void
unit_bit ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_BIT;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "bit");
}

static void
unit_dpi ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_DPI;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "dpi");
}

static void
unit_percent ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_PERCENT;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "%");
}

static void
unit_inch ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_INCH;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "in");
}

static void
unit_microsecond ()
{
	GnomeScanUnit unit = GNOME_SCAN_UNIT_MICROSECOND;
	const gchar* string;

	string = gnome_scan_unit_to_string(unit);

	g_assert_cmpstr(string, ==, "µs");
}

static void
enum_value_nick ()
{
    gchar *nick;

    nick = gnome_scan_enum_get_nick (GNOME_SCAN_TYPE_UNIT, GNOME_SCAN_UNIT_MM);

    g_assert_cmpstr (nick, ==, "mm");
}

gint
main(gint argc, gchar**argv)
{
	g_set_prgname("module");
	g_type_init();
	g_test_init(&argc, &argv, NULL);
	gnome_scan_init_test(&argc, &argv);

	g_test_add_func("/common/unit/pixel", unit_pixel);
	g_test_add_func("/common/unit/point", unit_point);
	g_test_add_func("/common/unit/millimeter", unit_millimeter);
	g_test_add_func("/common/unit/bit", unit_bit);
	g_test_add_func("/common/unit/dpi", unit_dpi);
	g_test_add_func("/common/unit/percent", unit_percent);
	g_test_add_func("/common/unit/inch", unit_inch);
	g_test_add_func("/common/unit/microsecond", unit_microsecond);
    g_test_add_func("/common/enum_value_nick", enum_value_nick);

	return g_test_run();
}
