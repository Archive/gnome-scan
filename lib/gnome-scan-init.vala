/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using Config;
using GLib;
using Gegl;
using Gtk;

namespace Gnome.Scan {
	// should be private !
	public ModuleManager module_manager;

	public void init_test([CCode (array_length_pos = 0.9)]ref string[] argv)
	{
		Gegl.init(ref argv);
	}

    public void init([CCode (array_length_pos = 0.9)]ref weak string[] argv)
    {
		debug("Initializing GNOME Scan %s for %s", Config.VERSION, GLib.Environment.get_prgname());
 
		string module_path;

		// TODO: i18n
		// TODO: install stock items

		option_manager = new OptionManager();
		option_manager.register_rule_by_type(typeof(OptionBool), typeof(CheckboxWidget));
		option_manager.register_rule_by_type(typeof(OptionNumber), typeof(ScaleWidget));
		option_manager.register_rule_by_type(typeof(OptionString), typeof(EntryWidget));
		option_manager.register_rule_by_type(typeof(OptionEnum), typeof(ComboBoxWidget));
		option_manager.register_rule_by_type(typeof(OptionPaperSize), typeof(PaperSizeWidget));
		option_manager.register_rule_by_name("page-orientation", typeof(PageOrientationWidget));

		// If we are in source tree.
		if (GLib.FileUtils.test("modules/gsfile", GLib.FileTest.IS_DIR)) {
			module_path = string.join(GLib.Path.SEARCHPATH_SEPARATOR_S,
									  "modules/gsfile", "modules/gsane");
		}
		// Installed
		else {
			module_path = MODULE_DIR;
		}

		module_manager = new ModuleManager(module_path);
		module_manager.query_modules();

		// init Gegl after query_module so that modules can provide
		// gegl operation.
		Gegl.init(ref argv);
    }

    public void exit()
    {
		module_manager = null;
		option_manager = null;

		Gegl.exit();
		// TODO: uninstall stock items
    }
}