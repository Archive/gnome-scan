/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;

namespace Gnome.Scan {
	public const string OPTION_GROUP_SCANNER	= N_("Scan Options");
	public const string OPTION_GROUP_SINK		= N_("Output Options");
	public const string OPTION_GROUP_FORMAT		= N_("Format");

	/**
	 * GnomeScanOptionHint:
	 *
	 * Hint for packing option widget in UI.
	 */
	public enum OptionHint {
		HIDDEN,
		PRIMARY,
		SECONDARY,
		PREVIEW,
		REPEAT, // imply PRIMARY
	}

	public abstract class Option : Object {
		public string			name		{get; set construct;}
		public string			title		{get; set construct;}
		public string			desc		{get; set construct;}
		public string			group		{get; set construct;}
		public string			domain		{get; set construct;}

		public Gnome.Scan.Unit	unit		{get; set construct; default = Unit.NONE;}
		public OptionHint		hint		{get; set construct;}
		public bool				active		{get; set; default = true;}

		public virtual void set_g_value(Value value)
		{
		}

		// Helper function for set_g_value() implementation that copy
		// or transform a value from src to dest.  Returns whether the
		// operation was successful.
		protected bool copy_or_transform_g_value(Value src, ref Value dest)
		{
			Type stype = src.type();
			Type dtype = dest.type();

			if (Value.type_compatible(stype, dtype)) {
				src.copy(ref dest);
				return true;
			}
			else if (Value.type_transformable(stype, dtype)) {
				src.transform(ref dest);
				return true;
			}
			else {
				warning("Unable to set %s value for %s",
						stype.name(), dtype.name());
				return false;
			}
		}
	}

	public class OptionBool : Option {
		public bool value {set; get;}

		public OptionBool(string name, string title, string desc, string group, string domain, bool value, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.hint = hint;
			this.value = value;
		}

		public override void set_g_value(Value value)
		{
			Value oval = Value(typeof(bool));
			if (this.copy_or_transform_g_value(value, ref oval))
				this.value = oval.get_boolean();
		}
	}

	public class OptionNumber : Option
	{
		public double value {set; get;}
		public Range range {set construct; get;}

		public OptionNumber(string name, string title, string desc, string group, string domain, double value, Range range, Gnome.Scan.Unit unit, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.range = range;
			this.unit = unit;
			this.hint = hint;
			this.value = value;
		}

		public override void set_g_value(Value value)
		{
			Value oval = Value(typeof(double));
			if (this.copy_or_transform_g_value(value, ref oval))
				this.value = oval.get_double();
		}
	}

	public class OptionString : Option {
		public string value {set; get;}

		public OptionString(string name, string title, string desc, string group, string domain, string value, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.hint = hint;
			this.value = value;
		}
	}

	public class OptionEnum : Option {
		public Gnome.Scan.EnumValue value {set; get;}
		public weak SList<Gnome.Scan.EnumValue?> values {set construct; get;}

		public OptionEnum(string name, string title, string desc, string group, string domain, Gnome.Scan.EnumValue value, SList<Gnome.Scan.EnumValue?> values, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.hint = hint;
			this.values = values;
			this.value = value;
		}

		public virtual void append(Gnome.Scan.EnumValue value)
		{
			foreach(weak Gnome.Scan.EnumValue val in values)
				if (val.label == value.label)
					return;

			values.append(value);
		}
	}

	public class OptionPaperSize : OptionEnum {
		public Extent extent {set construct; get; }

		public OptionPaperSize(string name, string title, string desc, string group, string domain, Extent extent, Gnome.Scan.EnumValue value, SList<Gnome.Scan.EnumValue?> values, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.extent = extent;
			this.hint = hint;
			this.values = values;
			this.value = value;
		}

		public override void append(Gnome.Scan.EnumValue value)
		{
			// Ensure the wanted papersize fit the device extent.
			weak Gtk.PaperSize ps = (Gtk.PaperSize) value.value.get_boxed();

			debug("Adding %s (%.2fx%.2f)", ps.get_display_name(),
				  ps.get_width(Gtk.Unit.MM), ps.get_height(Gtk.Unit.MM));

			if (ps.get_width(Gtk.Unit.MM) <= extent.width
				&& ps.get_height(Gtk.Unit.MM) <= extent.height)
				base.append(value);
		}
	}

	public class OptionBoxed : Option {
		weak Value value {set; get;}

		public OptionBoxed(string name, string title, string desc, string group, string domain, Value value, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.hint = hint;
			this.value = value;
		}
	}

	public class OptionPointer : Option {
		public weak void* value {set; get;}

		public OptionPointer(string name, string title, string desc, string group, string domain, void* value, OptionHint hint)
		{
			this.name = name;
			this.title = title;
			this.desc = desc;
			this.group = group;
			this.domain = domain;
			this.hint = hint;
			this.value = value;
		}
	}
}
