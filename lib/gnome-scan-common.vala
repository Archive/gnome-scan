/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using GLib;
using Gtk;
using Config;

namespace Gnome.Scan {
	public enum Unit {
		NONE	= -1,
		PIXEL,
		POINTS,
		INCH,
		MM,
		BIT,
		DPI,
		PERCENT,
		MICROSECOND
	}
	public string unit_to_string(Gnome.Scan.Unit unit)
	{
		switch(unit) {
		case Unit.PIXEL:
			return _("px");
		case Unit.POINTS:
			return _("pt");
		case Unit.MM:
			return _("mm");
		case Unit.INCH:
            /* translators: Shortname for Inch unit */
			return _("in");
		case Unit.BIT:
			return _("bit");
		case Unit.DPI:
			return _("dpi");
		case Unit.PERCENT:
			return "%";
		case Unit.MICROSECOND:
			return _("µs");
		default:
			return "";
		}
	}

	// Status in reverse order of priority
    public enum Status {
		UNKNOWN		= 0,
		FAILED		= 100,
		INITIALIZING= 200,
		// Device is up but doing some work (after or before scan).
		BUSY		= 300,
		// needs option to be set by user
		UNCONFIGURED= 400,
		// ready to start a process
		READY		= 500,
		// acquiring/processing/outputting
		PROCESSING	= 600,
		// processing done;
		DONE		= 700;
    }

	public struct Point {
		public double x;
		public double y;
	}

	public struct Extent {
		public double width;
		public double height;

		public Extent(double width, double height)
		{
			this.width = width;
			this.height = height;
		}
	}

	public struct Range {
		public double min;
		public double step;
		public double max;
	}

    /* do we actually need a GObject here ??? */
	public class Format : GLib.Object {
		public weak string	name;
		public weak string	desc;
		public weak string	domain;
		public weak string	icon_name;
		public string[]		mime_types;
		public string[]		suffixes;

		public Format(string name, string desc, string domain, string icon_name, string[] mime_types, string[] suffixes)
		{
			this.name		= name;
			this.desc		= desc;
			this.domain		= domain;
			this.icon_name	= icon_name;
			this.mime_types	= mime_types;
			this.suffixes	= suffixes;
		}
	}

	public struct EnumValue {
		public Value value;
		public string label;
		public string? domain;

		public EnumValue(Value value, string label, string? domain)
		{
			this.value = value;
			this.label = label;
			this.domain = domain;
		}
	}

	private const double MM_PER_INCH	= 25.4;

	// debugging facility, return the nick of value.
	public string enum_get_nick(GLib.Type enum_type,
								int value)
	{
		EnumClass klass = (EnumClass) enum_type.class_ref();
		weak GLib.EnumValue enum_value = klass.get_value(value);
		if (enum_value == null)
			return "%i".printf(value);
		else
			return enum_value.value_nick;
	}

	// debugging facility. return the list of flags in value as "{nick,nick}"
	public string flags_get_nicks(GLib.Type flags_type,
								 int value)
	{
		FlagsClass klass = (FlagsClass) flags_type.class_ref();
		weak FlagsValue flags_value = null;
		string nicks = "{";
		for (; value > 0 && ((flags_value = klass.get_first_value(value)) != null); value = value ^ flags_value.value) {
			if (flags_value != null) {
				if (nicks.len() > 1)
					nicks = nicks.concat(",");
				nicks = nicks.concat(flags_value.value_nick);
			}
		}

		return nicks.concat("}");
	}

	private void warn_unsupported_unit(Unit unit)
	{
		warning("Unit %s conversion not supported.",
				enum_get_nick(typeof(Unit), unit));
	}

	/**
	 * gnome_scan_convert:
	 * @val:	the value to convert
	 * @from:	the current @val unit
	 * @to:		the target unit fro @val
	 * @res:	the resolution in dpi
	 *
	 * Convert @val from @from unit to @to at @res resolution. Useful for
	 * preview area, #GnomeScanScanner, etc.
	 **/
	public double convert(double val,
						  Unit from,
						  Unit to,
						  double res)
	{
		if (from == to)
			return val;

		switch(from) {
		case Unit.NONE:
		case Unit.BIT:
		case Unit.PERCENT:
		case Unit.MICROSECOND:
		case Unit.DPI:
			warn_unsupported_unit(from);
			return val;
		default:
			switch (to) {
			case Unit.NONE:
			case Unit.BIT:
			case Unit.PERCENT:
			case Unit.MICROSECOND:
			case Unit.DPI:
				warn_unsupported_unit(to);
				return val;
			default:
				return convert_from_mm (convert_to_mm (val, from, res),
										to, res);
			}
		}
	}

	public double convert_from_mm(double val,
								  Unit to,
								  double res)
	{
		switch (to) {
		case Unit.NONE:
		case Unit.BIT:
		case Unit.PERCENT:
		case Unit.MICROSECOND:
		case Unit.DPI:
			warn_unsupported_unit(to);
			return val;
		case Unit.MM:
			return val;
		case Unit.PIXEL:
			return val / (MM_PER_INCH / res);
		}
		return 0;
	}

	public double convert_to_mm(double val,
								Unit from,
								double res)
	{
		switch (from) {
		case Unit.NONE:
		case Unit.BIT:
		case Unit.PERCENT:
		case Unit.MICROSECOND:
		case Unit.DPI:
			warn_unsupported_unit(from);
			return val;
		case Unit.MM:
			return val;
		case Unit.PIXEL:
			return val * (MM_PER_INCH / res);
		}
		return 0;
	}

	[Compact]
	public class Rectangle {
		public double	x;
		public double	y;
		public double	width;
		public double	height;

		public void	rotate(Rectangle	area,
						   int		angle)
		{
			angle%= 360;
	
			switch (angle)
			{
			case 0:
				break;
			case 270:
				this.width = this.height;
				this.height = this.width;
				this.x = this.y;
				this.y = double.max(area.width - this.x - this.width, area.x);
				break;
			case 180:
				this.x = double.max(area.width - this.x - this.width, area.x);
				this.y = double.max(area.height - this.y - this.height, area.y);
				break;
			case 90:
				this.width = this.height;
				this.height = this.width;
				this.y = this.x;
				this.x = double.max(area.height - this.y - this.height, area.y);
				break;
			default:
				warning("%i degree rotation is not supported", angle);
				break;
			}
		}

		public void convert(Unit	from,
							Unit	to,
							double	res)
		{
			convert_to_mm(from, res);
			convert_from_mm(to, res);
		}

		public void	convert_to_mm(Unit	from,
								  double	res)
		{
			this.x		= Gnome.Scan.convert_to_mm(this.x,		from, res);
			this.y		= Gnome.Scan.convert_to_mm(this.y,		from, res);
			this.width	= Gnome.Scan.convert_to_mm(this.width,	from, res);
			this.height = Gnome.Scan.convert_to_mm(this.height,	from, res);
		}

		public void	convert_from_mm(Unit	to,
									double	res)
		{
			this.x		= Gnome.Scan.convert_from_mm(this.x,		to, res);
			this.y		= Gnome.Scan.convert_from_mm(this.y,		to, res);
			this.width	= Gnome.Scan.convert_from_mm(this.width,	to, res);
			this.height = Gnome.Scan.convert_from_mm(this.height,	to, res);
		}
	}
}
