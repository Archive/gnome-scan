/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;

namespace Gnome.Scan {
	// option_manager instance.
	public OptionManager option_manager;

	public class OptionManager : Object {
		// option name -> Type
		private HashTable<weak string,Type>		rules_by_name;
		// option type -> Type
		private HashTable<Type,Type>		rules_by_type;
		// option name -> option instance
		private HashTable<weak string,Option>	options;

		construct {
			this.rules_by_name	= new HashTable<weak string,Type>(GLib.str_hash, GLib.str_equal);
			this.rules_by_type	= new HashTable<Type,Type>(GLib.direct_hash, GLib.direct_equal);
			this.options		= new HashTable<weak string,Option>(GLib.str_hash, GLib.str_equal);
		}

		public void register_option(Option option)
		{
			this.options.insert(option.name, option);
		}

		public void register_rule_by_name(string option_name, Type handler_type)
		{
			this.rules_by_name.insert(option_name, handler_type);
		}

		public void register_rule_by_type(Type option_type, Type handler_type)
		{
			this.rules_by_type.insert(option_type, handler_type);
		}

		public Type get_widget_type_for(Option option)
		{
			Type wtype;
			wtype = this.rules_by_name.lookup(option.name);
			if (wtype != Type.INVALID)
				return wtype;

			wtype = this.rules_by_type.lookup(option.get_type());
			if (wtype != Type.INVALID)
				return wtype;

			return Type.INVALID;
		}
	}
}
