/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
/**
 * SECTION: gnome-scan-dialog
 * @short_description: Scan configuration dialog
 * @include: gnome-scan.h
 *
 **/

using Gtk;
using Gdk;

namespace Gnome.Scan {
    enum Pages {
		GENERAL,
		FRONT_SCANNER,
		FRONT_SINK,
		PREVIEW,
		ADVANCED,
		PROCESSING,
		OUTPUT,
		ACQUISITION,
		LAST
    }

    public class Dialog : Gtk.Window {
		public	Job job { get; set construct; }

		private Notebook			notebook;
		private Box					acquisition_page;
		private AcquisitionWidget	acquisitor;

		// buttons
		private Button bcancel;
		private Button bclose;
		private Button bscan;
		private Button bnext;
		private Button bback;

		// tabs
		private Widget[]	pages;

		// nodes
		private Gnome.Scan.Node scanner	= null;

		// general
		private ScannerSelector selector;

		construct {
			Box 		box;

			this.border_width	= 6;
			this.default_width	= 320;
			this.default_height	= 440;
			this.icon_name		= "scanner";
			this.title			= _("Scan");
			this.window_position= WindowPosition.CENTER;
			this.delete_event.connect(this.on_window_delete_event);
			this.type_hint		= Gdk.WindowTypeHint.DIALOG;

			this.job.notify["scanner"]	+= this.on_scanner_selected;
			this.job.notify["status"]	+= this.on_status_changed;

			box = new VBox(false, 6);
			box.border_width = 6;
			this.add(box);

			// notebook
			this.notebook = new Notebook();
			this.notebook.border_width = 6;
			box.pack_start(this.notebook, true, true, 0);

			this.acquisition_page = new VBox(false, 6);
			this.acquisition_page.no_show_all = true;
			box.pack_start(this.acquisition_page, true, true, 0);

			// build basic UI
			this.pages = new Widget[(int) Pages.LAST];
			this.build_general_tab();
			this.build_scanner_page();
			this.build_node_ui(this.job.sink);

			// buttons
			this.add_buttons(box);
		}
		
		public Dialog(Gtk.Window? parent, Job job)
		{
			this.job = job;

			if (parent != null) {
				this.set_transient_for(parent);
				this.window_position = Gtk.WindowPosition.CENTER_ON_PARENT;
			}
		}

		public void run()
		{
			this.selector.probe_scanners();
			this.init_buttons();
			this.show_all();
			this.window.set_cursor(new Cursor(CursorType.WATCH));
			Gtk.main();
		}


		// INTERNAL
		private void add_buttons(Box container)
		{
			ButtonBox	button_box;

			button_box = new HButtonBox();
			button_box.spacing = 6;
			button_box.layout_style = ButtonBoxStyle.END;
			container.pack_start(button_box, false, true, 0);

			this.bclose	= this.add_button(button_box, Gtk.STOCK_CLOSE);
			this.bclose.clicked.connect(this.on_button_close_clicked);
			// TODO: register stock item "scan".
			this.bscan	= this.add_button(button_box, Gtk.STOCK_APPLY);
			this.bscan.clicked.connect(this.on_button_scan_clicked);
			this.bback	= this.add_button(button_box, Gtk.STOCK_EDIT);
			this.bback.clicked.connect(this.on_button_back_clicked);
			this.bcancel = this.add_button(button_box, Gtk.STOCK_CANCEL);
			this.bcancel.clicked.connect(this.on_button_cancel_clicked);
			this.bnext	= this.add_button(button_box, Gtk.STOCK_GO_FORWARD);
			this.bnext.clicked.connect(this.on_button_next_clicked);
		}

		private Button add_button(Box button_box, string stock_id)
		{
			Button button = new Button.from_stock(stock_id);
			button.sensitive = false;
			button_box.pack_start(button, false, false, 0);
			return button;
		}

		private Widget build_page(Pages page_id)
		{
			if (this.pages[(int) page_id] is Widget)
				return this.pages[(int) page_id];

			switch(page_id) {
			case Pages.GENERAL:
				this.build_general_tab();
				break;
			case Pages.FRONT_SCANNER:
				this.build_scanner_page();
				break;
			case Pages.FRONT_SINK:
				this.build_sink_page();
				break;
			case Pages.ADVANCED:
			case Pages.PROCESSING:
			case Pages.OUTPUT:
				this.build_option_page(page_id);
				break;
			case Pages.ACQUISITION:
				this.build_acquisition_page();
				break;
			default:
				warning("Don't know how to build %s page",
						enum_get_nick(typeof(Pages), page_id));
				break;
			}
			return this.pages[(int) page_id];
		}

		private void build_general_tab()
		{
			var paned = new VPaned();
			selector = new ScannerSelector(this.job);
			selector.border_width = 6;
			selector.probe_done.connect(this.on_probe_done);
			paned.pack1(this.selector, true, false);
			paned.border_width = 6;

			var box = new VBox(false, 6);
			box.border_width = 6;
			paned.pack2(box, false, false);

			this.append_page(Pages.GENERAL, _("_General"), paned);
		}

		private void build_scanner_page()
		{
			OptionPage opage = new OptionHPage();
			var paned = this.pages[(int) Pages.GENERAL] as Paned;
			var box = paned.get_child2() as Box;
			box.pack_start(opage, false, true, 0);
			this.append_page(Pages.FRONT_SCANNER, null, opage);
		}

		private void build_sink_page()
		{
			OptionPage opage = new OptionHPage();
			weak Paned paned = this.pages[(int) Pages.GENERAL] as Paned;
			weak Box box = paned.get_child2() as Box;
			box.pack_start(opage, false, true, 0);
			this.append_page(Pages.FRONT_SINK, null, opage);
		}

		private void build_acquisition_page()
		{
			this.acquisitor = new AcquisitionWidget(this.job,
													_("Acquisition"),
													_("The software now acquires and processes images according to settings."),
													"scanner");
			this.acquisition_page.pack_start(this.acquisitor, true, true, 0);

			var page = new OptionVPage() as Container;
			page.border_width = 12;
			this.acquisition_page.pack_start(page, false, true, 0);

			this.pages[(int) Pages.ACQUISITION] = page;
		}

		private void build_option_page(Pages page_id)
		{
			string label;

			switch(page_id) {
			case Pages.ADVANCED:
				label = _("_Advanced");
				break;
			case Pages.PROCESSING:
				label = _("_Processing");
				break;
			case Pages.OUTPUT:
				label = _("_Output");
				break;
			default:
				return;
			}

			this.append_page(page_id, label, new OptionVPage(true));
		}

		private void append_page(int tab, string? label, Container page)
		{
			if (this.pages[tab] != null)
				return;

			if (label != null) {
				Widget tab_label = new Label.with_mnemonic(label);
				if (page.border_width == 0)
					page.border_width = 12;
				this.notebook.append_page(page, tab_label);
			}
			this.pages[tab] = page;
			page.show_all();
		}

		private void build_node_ui(Node node)
		{
			Pages page_id;
			OptionPage page;

			foreach(weak Option option in node.options) {
				if (option.hint == OptionHint.HIDDEN)
					continue;

				page_id = this.get_page_id_for_option(node, option);
				page = this.build_page(page_id) as OptionPage;
				if (!(page is OptionPage))
					continue;

				page.pack_option(option);
				page.show_all();
			}
		}

		private void destroy_node_ui(Node node)
		{
			Pages page_id = Pages.ADVANCED;
			weak OptionPage page;
			foreach(weak Option option in node.options) {
				if (option.hint == OptionHint.HIDDEN)
					continue;

				page_id = this.get_page_id_for_option(node, option);
				page = this.pages[(int) page_id] as OptionPage;
				page.unpack_option(option);
				page.show_all();
			}
		}

		private Pages get_page_id_for_option(Node node, Option option)
		{
			switch(option.hint) {
			case OptionHint.HIDDEN:
				warning("Option %s of node %s is hidden.", node.get_type().name(), option.name);
				return Pages.LAST;
			case OptionHint.PRIMARY:
			case OptionHint.REPEAT:
				if (node is Scanner)
					return Pages.FRONT_SCANNER;
				else if (node is Sink)
					return Pages.FRONT_SINK;
				else
					return Pages.ADVANCED;
			case OptionHint.SECONDARY:
				return Pages.ADVANCED;
			case OptionHint.PREVIEW:
				return Pages.PREVIEW;
			}
			return Pages.ADVANCED;
		}

		private void init_buttons()
		{
			this.bclose.no_show_all	= false;
			this.bclose.show();
			this.bscan.no_show_all	= false;
			this.bscan.show();
			
			this.bback.no_show_all	= true;
			this.bback.hide();
			this.bcancel.no_show_all= true;
			this.bcancel.hide();
			this.bnext.no_show_all	= true;
			this.bnext.hide();
		}

		private void scan()
		{
			try {
				Timeout.add(42, this.acquisitor.monitor_job);
				Thread.create(this.job_run_thread, false);
			}
			catch (ThreadError e) {
				// run in same thread ?
				warning("Unable to create thread for acquiring");
			}
		}

		private void back()
		{
			this.notebook.no_show_all			= false;
			this.acquisition_page.no_show_all	= true;
			this.acquisition_page.hide();
			this.init_buttons();
			this.notebook.show_all();
		}
		
		private void close()
		{
			Gtk.main_quit();
		}


		private void* job_run_thread()
		{
			this.job.run();

			return null;
		}

		// CALLBACKS
		private void on_probe_done(ScannerSelector selector)
		{
			this.window.set_cursor(null);
		}

		private void on_scanner_selected(Job job, ParamSpec pspec)
		{
			if (job.scanner == this.scanner)
				return;

			if (this.scanner != null)
				this.destroy_node_ui(this.scanner);

			this.scanner = job.scanner;

			if (this.scanner != null)
				this.build_node_ui(this.scanner);
		}

		private void on_status_changed(Job job, ParamSpec pspec)
		{
			debug("job status updated to %s", enum_get_nick(typeof(Status), job.status));
			switch(job.status) {
			case Status.UNCONFIGURED:
				this.bclose.sensitive	= true;
				this.bscan.sensitive	= false;
				break;
			case Status.READY:
				this.bclose.sensitive	= true;
				this.bscan.sensitive	= true;
				break;
			case Status.PROCESSING:
				this.bclose.sensitive	= false;
				this.bscan.sensitive	= false;
				this.bscan.no_show_all	= true;
				this.bscan.hide();

				this.bcancel.no_show_all= false;
				this.bcancel.sensitive	= true;
				this.bnext.no_show_all	= false;
				this.bnext.sensitive	= false;
				this.bback.no_show_all	= false;
				this.bback.sensitive	= false;
				break;
			case Status.DONE:
				this.bclose.sensitive	= true;
				this.bback.sensitive	= true;
				this.bcancel.sensitive	= false;
				this.bnext.sensitive	= true;
				this.bscan.sensitive	= true;
				break;
			case Status.FAILED:
				// TODO: error dialog?
				break;
			default:
				break;
			}

			this.show_all();
		}

		private void on_button_scan_clicked(Button button)
		{
			this.build_page(Pages.ACQUISITION);
			this.notebook.no_show_all = true;
			this.notebook.hide();
			this.acquisition_page.no_show_all = false;
			this.show_all();
			this.scan();
		}

		private void on_button_next_clicked(Button button)
		{
			this.scan();
		}

		private void on_button_cancel_clicked(Button button)
		{
			this.job.cancel();
			this.back();
		}

		private void on_button_back_clicked(Button button)
		{
			this.job.end();
			this.back();
		}

		private void on_button_close_clicked(Button button)
		{
			this.job.end();
			this.close();
		}

		private bool on_window_delete_event(Widget window, Event evt)
		{
			this.close();
			return false;
		}
    }
}
