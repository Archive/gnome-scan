/* GNOME Scan - Scan as easy as you print
 * Copyright © 2007  Étienne Bersac <bersace03@laposte.net>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

namespace Gnome.Scan {
	public abstract class Backend : Object {
		public abstract void* probe_scanners();

		public signal void scanner_added(Scanner scanner);
		public signal void scanner_removed(Scanner scanner);
		public signal void probe_done();
	}
}
