/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using Gtk;

namespace Gnome.Scan {
	public class ScaleWidget : OptionWidget {
		private Adjustment adj;

		construct {
			this.expand = false;
			this.no_label = false;

			SpinButton spin;
			Scale scale;
			int digits = 0;

			var option = this.option as OptionNumber;

			adj = new Adjustment(option.value, option.range.min, option.range.max, option.range.step, option.range.step, 0);

			if (option.range.step < 1.0)
				digits = 2;

			scale = new HScale(adj);
			scale.set_increments(option.range.step, option.range.step);
			scale.draw_value	= false;
			scale.update_policy	= UpdateType.DISCONTINUOUS;
			scale.digits		= digits;

			spin = new SpinButton(adj, option.range.step, digits);
			spin.set_increments(option.range.step, option.range.step);
			spin.update_policy	= SpinButtonUpdatePolicy.IF_VALID;
			spin.snap_to_ticks	= true;
			spin.numeric		= true;
			spin.wrap			= true;

			adj.notify["value"] += this.on_adj_value_changed;
			option.notify["value"] += this.on_option_value_changed;

			this.pack_start(scale, true, true, 0);
			this.pack_start(spin, false, true, 0);
		}

		private void on_option_value_changed()
		{
			var option = option as OptionNumber;
			adj.value = option.value;
		}

		private void on_adj_value_changed()
		{
			var option = option as OptionNumber;
			option.value = adj.value;
		}
	}
}
