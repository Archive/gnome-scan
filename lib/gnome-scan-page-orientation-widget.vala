/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2009  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using Gtk;

namespace Gnome.Scan {
	public class PageOrientationWidget : OptionWidget {
		HashTable<RadioButton,weak Gnome.Scan.EnumValue?> radio_map;

		construct {
			this.no_label = true;
			this.radio_map = new HashTable<RadioButton,weak Gnome.Scan.EnumValue?>(GLib.direct_hash, GLib.direct_equal);

			var option = this.option as OptionEnum;

			RadioButton radio;
			Image icon;
			Label label;
			VBox parent = new VBox(true, 0);
			this.pack_start(parent, false, true, 0);
			HBox box;
			weak StockItem item;
			weak SList group = null;
                        foreach (weak Gnome.Scan.EnumValue? value in option.values) {
                                box = new HBox(false, 4);
				// icon
				icon = new Image.from_stock(value.label, IconSize.BUTTON);
				box.pack_start(icon, false, false, 0);
				// label
				stock_lookup(value.label, item);
				label = new Label(item.label);
				box.pack_start(label, true, true, 0);
				// radio
				radio = new RadioButton(group);
				radio.add(box);
				radio.toggled.connect(this.on_radio_toggled);
				group = radio.get_group();
				parent.pack_start(radio, false, true, 0);
				this.radio_map.insert(radio, value);
			}
		}

		private void on_radio_toggled(ToggleButton button)
		{
			if (!button.active)
				return;

			var option = this.option as OptionEnum;
			weak Gnome.Scan.EnumValue? value = this.radio_map.lookup((RadioButton)button);
			option.value = value;
		}
	}
}
