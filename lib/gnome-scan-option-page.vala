/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using GLib; 
using Gtk;

namespace Gnome.Scan {
	// waiting for Gtk+ 2.16 for extendings directly Gtk.Box :)
    public abstract class OptionPage : Alignment {
		public bool scrolled {get; set construct; default = false; }
		private Box _container;
		public Box container {
			get {
				return _container;
			}
			set {
				if (this.scrolled) {
					value.border_width = 6;
					viewport.add(value);
				}
				else {
					this.add(value);
				}
				_container = value;
			}
		}
		private ScrolledWindow scrowin;
		private Viewport viewport;
		private HashTable<string,OptionBox> boxes;
		int scroll_height;
		int scroll_width;
		int box_visible_count = 0;
		int box_expanding_count = 0;

		construct {
			this.boxes = new HashTable<string,OptionBox>(GLib.str_hash, GLib.str_equal);
			this.no_show_all = true;
			if (scrolled) {
				this.border_width = 6;
				scrowin = new ScrolledWindow(null, null);
				this.add(scrowin);
				scrowin.shadow_type = ShadowType.NONE;
				scrowin.hscrollbar_policy = PolicyType.AUTOMATIC;
				scrowin.vscrollbar_policy = PolicyType.AUTOMATIC;
				this.viewport = new Viewport(scrowin.hadjustment, scrowin.vadjustment);
				viewport.shadow_type = ShadowType.NONE;
				scrowin.add(viewport);
				scrowin.size_allocate.connect(this.on_size_allocate);
				viewport.size_allocate.connect(this.on_viewport_size_allocate);
			}
		}

		public void pack_option(Option option)
		{
			OptionBox box = this.boxes.lookup(option.group);
			if (box == null) {
				box = new OptionBox(option.group);
				box.hide.connect(this.on_box_visibility_changed);
				box.show.connect(this.on_box_visibility_changed);
				this.box_visible_count++;
				this.container.pack_start(box, box.expand, true, 0);
				this.boxes.insert(option.group, box);
				this.on_box_visibility_changed(box);
			}
			box.pack_option(option);
		}

		public void unpack_option(Option option)
		{
			weak OptionBox box = this.boxes.lookup(option.group);
			if (box.unpack_option(option)) {
				this.container.remove(box);
				this.boxes.remove(option.group);
			}
		}

		private void on_box_visibility_changed(Widget box)
		{
			if (((OptionBox)box).expand) {
				this.box_expanding_count+= box.visible ? +1 : -1;
				var parent = (Container) this.parent;
				parent.child_set(this, "expand", this.box_expanding_count > 0);
			}

			this.box_visible_count += box.visible ? +1 : -1;
			if (this.box_visible_count == 0) {
				this.no_show_all = true;
				this.hide();
			}
			else {
				this.no_show_all = false;
				this.show_all();
			}
		}

		private void update_shadow_type()
		{
			Requisition req;
			viewport.size_request(out req);
			if (this.scroll_height < req.height || this.scroll_width < req.width) {
				scrowin.shadow_type = ShadowType.IN;
			}
			else {
				scrowin.shadow_type = ShadowType.NONE;
			}
		}

		private void on_size_allocate(Widget widget, Gdk.Rectangle alloc)
		{
			this.scroll_height = alloc.height;
			this.scroll_width = alloc.width;
			this.update_shadow_type();
		}

		private void on_viewport_size_allocate(Widget widget, Gdk.Rectangle alloc)
		{
			this.update_shadow_type();
		}
    }

    public class OptionVPage : OptionPage {
		construct {
			this.container = new VBox(false, 6);
		}

		public OptionVPage(bool scrolled = false)
		{
			this.scrolled = scrolled;
		}
    }

    public class OptionHPage : OptionPage {
		construct {
			this.container = new HBox(false, 6);
		}
    }
}
