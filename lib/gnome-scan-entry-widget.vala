/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using Gtk;

namespace Gnome.Scan {
	public class EntryWidget : OptionWidget {
		private Entry entry;

		construct {
			var option = this.option as OptionString;

			entry = new Entry();
			entry.text = option.value;
			this.pack_start(entry, true, true, 0);

			entry.notify["text"] += this.on_entry_changed;
			option.notify["value"] += this.on_option_changed;
		}

		private void on_entry_changed()
		{
			option.freeze_notify();
			((OptionString)option).value = entry.text;
			option.thaw_notify();
		}

		private void on_option_changed()
		{
			entry.freeze_notify();
			entry.text = ((OptionString)option).value;
			entry.thaw_notify();
		}
	}
}