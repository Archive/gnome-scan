/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;
using Gegl;

namespace Gnome.Scan {
	public abstract class Node : Object {
		public Status status	{set; get;}
		public string? message	{set; get;}

		private HashTable<string,Option> options_table;
		private SList<Option> _options;
		public SList<Option>	options {
			get {
				return this._options;
			}
		}

		// Subgraph owned by this node. Subclass must create a Gegl
		// pipeline in this graph.
		private SList<Gegl.Node> _nodes;
		public SList<Gegl.Node> nodes {
			get {
				return this._nodes;
			}
		}


		construct {
			options_table = new HashTable<string,Option>(GLib.str_hash, GLib.str_equal);
			this.update_status(Status.INITIALIZING, null);
		}

		public virtual void start_scan()
		{
		}

		// Allows nodes to prepare next image (e.g. next filename,
		// next page in pdf, etc.) for multi-image
		// acquisition. Scanner returns true when new image is to
		// acquire.  For other node type, returned value is ignored.
		public virtual bool start_image()
		{
			return false;
		}

		public virtual void end_image()
		{
		}

		public virtual void end_scan()
		{
		}

		// Let message to null and the message will be determined from
		// status.
		public void update_status(Status status, string? message)
		{
			if (message == null) {
				switch(status) {
				case Status.UNKNOWN:
					this.message = _("Unknown");
					break;
				case Status.FAILED:
					this.message = _("Failed");
					break;
				case Status.INITIALIZING:
					this.message = _("Initializing");
					break;
				case Status.UNCONFIGURED:
					this.message = _("Unconfigured");
					break;
				case Status.READY:
					this.message = _("Ready");
					break;
				case Status.PROCESSING:
					this.message = _("Processing");
					break;
				case Status.DONE:
					this.message = _("Done");
					break;
				}
			}
			else {
				this.message = message;
			}
			this.status = status;
		}

		public void install_option(Option option)
		{
			this._options.append(option);
			this.options_table.insert(option.name, option);
		}

		public Option lookup_option(string name)
		{
			return this.options_table.lookup(name);
		}

		public void append_node(Gegl.Node node)
		{
			this._nodes.append(node);
		}

		public void remove_node(Gegl.Node node)
		{
			this._nodes.remove(node);
		}
	}
}