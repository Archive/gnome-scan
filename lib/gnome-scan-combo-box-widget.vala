/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using Gtk;

namespace Gnome.Scan {
	public class ComboBoxWidget : OptionWidget {
		private ListStore store;
		private ComboBox combo;
		private HashTable<string,string> strings;

		enum Column {
			LABEL,
			LENGTH
		}

		construct {
			TreeIter iter;
			CellRenderer renderer;

			strings = new HashTable<string,string>(GLib.str_hash, GLib.str_equal);

			this.store = new ListStore(Column.LENGTH, typeof(string));
			combo = new ComboBox.with_model(store);
			this.pack_start(combo, false, true, 0);
			renderer = new CellRendererText();
			combo.pack_start(renderer,true);
			combo.set_attributes(renderer, "text", Column.LABEL);

			var option = this.option as OptionEnum;
			weak Gnome.Scan.EnumValue curval = option.value;
			foreach(Gnome.Scan.EnumValue value in option.values) {
				store.append(out iter);
				store.set(iter, Column.LABEL, value.label);
				
				strings.insert(value.label, store.get_string_from_iter(iter));

				if (value.label == curval.label)
						combo.set_active_iter(iter);
			}

			combo.notify["active"] += this.on_combo_changed;
			option.notify["value"] += this.on_option_changed;

			// don't show one option selector. Thanks Philipp for
			// pointing that.
			if (option.values.length() <= 1)
				this.no_show_all = true;
		}

		private void on_combo_changed()
		{
			TreeIter iter;
			weak string label;

			combo.get_active_iter(out iter);
			store.get(iter,
					  Column.LABEL, out label);

			option.freeze_notify();
			var option = this.option as OptionEnum;
			foreach(Gnome.Scan.EnumValue value in option.values) {
				if (value.label == label)
					option.value = value;
			}
			option.thaw_notify();
		}

		private void on_option_changed()
		{
			TreeIter iter;
			var option = this.option as OptionEnum;
			string str = strings.lookup(option.value.label);
			if (str == null) {
				debug("No path for '%s' value", option.value.label);
				return;
			}

			store.get_iter_from_string(out iter, str);
			
			combo.freeze_notify();
			combo.set_active_iter(iter);
			combo.thaw_notify();
		}
	}
} 