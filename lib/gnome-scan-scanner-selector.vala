/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using Gtk;

namespace Gnome.Scan {
    enum Column {
		ICON,
		NAME,
		STATUS,
		OBJECT,
		LAST,
    }

	public class ScannerSelector : ScrolledWindow {
		ListStore	scanners;
		TreeView	view;
		TreeSelection selection;
		SList<Backend> backends;
		int backends_probing_count;

		public signal void probe_done();

		public Job job {get; set construct; }

		construct {
			CellRenderer renderer;
			TreeViewColumn col;

			this.backends_probing_count = 0;
			this.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);

			// list
			this.load_backends();
			this.scanners = new ListStore(Column.LAST,
										  typeof(string), typeof(string), typeof(string), typeof(GLib.Object));
			
			// view
			this.view = new TreeView.with_model(this.scanners);
			this.view.headers_visible = true;

			selection = this.view.get_selection();
			selection.set_mode(SelectionMode.SINGLE);
			selection.changed.connect(this.on_selection_changed);

			// columns;
			renderer = new CellRendererPixbuf();
			col = new TreeViewColumn.with_attributes(null, renderer, "icon-name", Column.ICON);
			this.view.append_column(col);

			renderer = new CellRendererText();
			col = new TreeViewColumn.with_attributes(_("Device"), renderer, "text", Column.NAME);
			this.view.append_column(col);

			renderer = new CellRendererText();
			col = new TreeViewColumn.with_attributes(_("Status"), renderer, "text", Column.STATUS);
			this.view.append_column(col);

			this.add_with_viewport(this.view);
		}

		public ScannerSelector(Job job)
		{
			this.job = job;
		}

		public void probe_scanners()
		{
			foreach(Backend backend in this.backends) {
				try {
					this.backends_probing_count++;
					GLib.Thread.create(backend.probe_scanners, false);
				}
				catch(GLib.ThreadError error) {
					warning("Unable to create new thread : %s", error.message);
					this.backends_probing_count--;
				}
			}
			GLib.Timeout.add_seconds(1, this.on_check_probe_done);
		}

		private void load_backends()
		{
			GLib.Type[] backend_types;
			Backend backend;

			backend_types = typeof(Backend).children();
			foreach(GLib.Type type in backend_types) {
				backend = (Backend)GLib.Object.new(type);
				this.backends.append(backend);
				backend.scanner_added.connect(this.on_scanner_added);
				backend.probe_done.connect(this.on_probe_done);
			}
		}

		private void on_scanner_added(Backend backend, Scanner new_scanner)
		{
			TreeIter iter;
			this.scanners.append(out iter);
			this.scanners.set(iter,
							  Column.ICON,	new_scanner.icon_name,
							  Column.NAME,	new_scanner.name,
							  Column.STATUS,_(new_scanner.message),
							  Column.OBJECT,new_scanner);
			new_scanner.set_data("path", this.scanners.get_path(iter).to_string());
			new_scanner.notify["status"] += on_scanner_status_changed;
		}

		private void on_probe_done(Backend backend)
		{
			this.backends_probing_count--;
		}

		private bool on_check_probe_done()
		{
			TreeIter iter;

			// auto select first scanners
			// TODO: remember selection in GConf
			this.scanners.get_iter_first(out iter);
			if (this.scanners.iter_is_valid(iter))
				this.view.get_selection().select_iter(iter);

			// stop polling once all backends ended probing.
			if (this.backends_probing_count == 0) {
				this.probe_done();
				return false;
			}

			return true;
		}

		private bool select_scanner_if_usable()
		{
			TreeModel model;
			TreeIter iter;
			Scanner scanner;

			if (selection.get_selected(out model, out iter))
				model.get(iter, Column.OBJECT, out scanner);
			else
				scanner = null;

			if (scanner == null) {
				this.job.scanner = null;
				return false;
			}

			if (scanner.status == Status.FAILED) {
				selection.unselect_all();
				return false;
			}

			if ((int)scanner.status >= (int)Status.UNCONFIGURED) {
				this.job.scanner = scanner;
				return false;
			}

			return true;
		}

		private void on_selection_changed(TreeSelection selection)
		{
			GLib.Idle.add(this.select_scanner_if_usable);
		}

		private void on_scanner_status_changed(Scanner scanner, ParamSpec pspec)
		{
			TreeIter iter;
			this.scanners.get_iter_from_string(out iter,
											   ((GLib.Object)scanner).get_data("path"));
			if (!this.scanners.iter_is_valid(iter))
				warning("Unable to update '%s' status in scanner selector!", scanner.name);

			this.scanners.set(iter, Column.STATUS, _(scanner.message));
		}
	}
}
