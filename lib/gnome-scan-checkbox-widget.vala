/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using GLib;
using Gtk;

namespace Gnome.Scan {
	public class CheckboxWidget : OptionWidget {
		private CheckButton check;

		construct {
			this.no_label = true;
			this.expand = false;
			this.check = new CheckButton.with_label(this.option.title);
			this.pack_start(this.check, true, true, 0);

			this.check.active = ((OptionBool)this.option).value;
			this.check.notify["active"] += this.update_option;
			this.option.notify["value"] += this.update_widget;
		}

		private void update_option()
		{
			var option = this.option as OptionBool;
			option.freeze_notify();
			option.value = this.check.active;
			option.thaw_notify();
		}

		private void update_widget()
		{
			var option = this.option as OptionBool;
			option.freeze_notify();
			this.check.active = option.value;
			option.thaw_notify();
		}
	}
}