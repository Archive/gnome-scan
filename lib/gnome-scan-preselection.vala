/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2009  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using GLib;
 
namespace Gnome.Scan {
	public abstract class Preselection : Object {
		public string option_name { set construct; get; }

		public virtual void apply(Option option)
		{
			message("%s does not implement apply, asked for option %s %s",
					this.get_type().name(), option.get_type().name(), option.name);
		}
	}

	public class PreselValue : Preselection {
		public Value value { set construct; get; }

		public PreselValue(string option_name, Value value)
		{
			this.option_name	= option_name;
			this.value			= value;
		}

		public override void apply(Option option)
		{
			option.set_g_value(this.value);
		}
	}

	public class PreselEnumValues : Preselection {
		public weak SList<Gnome.Scan.EnumValue?> values {set construct; get;}

		public PreselEnumValues(string option_name, SList<Gnome.Scan.EnumValue?> values)
		{
			this.option_name = option_name;
			this.values = values;
		}

		public override void apply(Option option)
		{
			if (!(option is OptionEnum))
				return;

			var opt = option as OptionEnum;

			foreach(weak Gnome.Scan.EnumValue value in values)
				opt.append(value);
		}
	}
}
