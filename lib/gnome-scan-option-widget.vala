/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using GLib;
using Gtk;

namespace Gnome.Scan {
	/** GnomeScanOptionWidget:
	 *
	 * A #GnomeScanOptionWidget expose a #GnomeScanOption to user
	 * and pass user defined value to #GnomeScanSettings.
	 */
	// TODO: with Gtk+ 2.16, herits directly from Box and let children
	// decide orientation using GtkOrientable.
	public abstract class OptionWidget : HBox {
		public Option	option		{get; set construct;}
		public bool		no_label	{get; set; default = false;}
		public bool		expand		{get; set;}

		construct {
			this.spacing = 4;
			this.border_width = 2;
			this.tooltip_text = option.desc;
			this.option.notify["active"] += this.on_option_active_changed;
			this.auto_hide(option);

			if (option.unit != Gnome.Scan.Unit.NONE) {
				var unit = new Label(unit_to_string(option.unit));
				this.pack_end(unit, false, false, 0);
			}
		}

		public OptionWidget(Option option)
		{
			this.option = option;
		}

		private void on_option_active_changed(Option option, ParamSpec pspec)
		{
			this.auto_hide(option);
		}

		private void auto_hide(Option option)
		{
			this.no_show_all = !option.active;
			if (option.active)
				this.show_all();
			else
				this.hide();
		}
	}
}
