/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;

public errordomain ModuleError {
	NONAME,
	INVALID,
	LOADED,
	FAIL
}

namespace Gnome.Scan {
	/**
	 * SECTION: gnome-scan-module-manager
	 * @short_description: Finding installed modules
	 *
	 * The #GnomeScanModuleManager search in a list of path shared object,
	 * and instanciate a #GnomeScanModule for each shared object. Upon
	 * initialization, the #GnomeScanModule will register new GTypes. This
	 * implementation is meant to be portable, but never tested on other
	 * plateform that GNU/Linux.
	 **/
	public class ModuleManager : Object {
		/**
		 * GnomeScanModuleManager:path:
		 *
		 * The search path where are installed #GnomeScanModule shared
		 * object. A search path is a string formed by a list of absolute
		 * path to directory separated by #G_SEARCHPATH_SEPARATOR.
		 **/
		[Description(nick="Path", blurb="Module path")]
		public string path { get; construct set; }

		private SList<TypeModule> modules;
			
		/**
		 * gnome_scan_module_manager_new:
		 * @path: Search path.
		 * 
		 * Create a new #GnomeScanModuleManager which will handle @path.
		 * 
		 * Returns: a new #GnomeScanModuleManager
		 **/
		public ModuleManager (string path) {
			this.path = path;
		}

		public static bool is_valid_module_name (string name)
		{
			return name.has_prefix ("lib")
			&& (name.has_suffix (GLib.Module.SUFFIX)
				|| name.has_suffix (".la"));
		}

		public static string module_name_from_filename(string filename) throws ModuleError {
			try {
				string name;
				var regex = new Regex ("lib(.*)\\..*$");
				MatchInfo match;
				if (! regex.match(filename, 0, out match))
					throw new ModuleError.NONAME("No name matched");
				name = match.fetch(1);
				if (name == null)
					throw new ModuleError.NONAME("No name fetched");
				return name;
			} catch (Error e) {
				var message = "Unable to determine %s module name: %s".printf(filename, e.message);
				throw new ModuleError.NONAME(message);
			}
		}

		/**
		 * gnome_scan_module_manager_query_modules:
		 * @self: a #GnomeScanModuleManager
		 * 
		 * Search for shared objects in path, instanciate and initialize a
		 * #GnomeScanModule for each shared object. Note that it won't search
		 * in subdirectories.
		 **/
		public void query_modules () {
			Module module;
			Dir dir = null;
			string[] paths = path.split (GLib.Path.SEARCHPATH_SEPARATOR_S);
			var loaded = new SList<string>();
			string name;
			string module_name;
			string filename;
	
			foreach (string path in paths) {
				try {
					dir = Dir.open (path, 0);
				}
				catch (GLib.Error error) {
					/* Show warning only for absolute path, else should be devel path… */
					if (path[0] == '/')
						warning("%s", error.message);
				}

				if (dir == null)
					continue;
				  
				/* Scan for lib*.{so,la} */
				while ((name = dir.read_name ()) != null) {
					try {
						if (!is_valid_module_name (name))
							throw new ModuleError.INVALID("%s: File not a module".printf(name));
						module_name = module_name_from_filename(name);

						foreach(string lname in loaded)
							if (module_name == lname)
								throw new ModuleError.LOADED("%s: Module already loaded".printf(name));

						filename = Path.build_filename (path, name);
						module = new Module (filename);
							
						if (!module.use ())
							throw new ModuleError.FAIL(GLib.Module.error());

						modules.append (module);
						loaded.append (module_name);
					}
					catch (ModuleError e) {
						if (e is ModuleError.FAIL)
							warning (e.message);
					}
				}
			}
		}
			
		/**
		 * gnome_scan_module_manager_unload_modules:
		 * @self: a #GnomeScanModuleManager
		 * 
		 * Search for shared objects in path, instanciate and initialize a
		 * #GnomeScanModule for each shared object. Note that it won't search
		 * in subdirectories.
		 **/
		public void unload_modules () {
			foreach (TypeModule module in modules)
			module.unuse ();
		}
	}
}
