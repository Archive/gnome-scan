/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;

namespace Gnome.Scan {
		
	/**
	 * SECTION: gnome-scan-module
	 * @short_description: Module loading and initialization
	 *
	 * A module represent the library opened with #GModule where are
	 * stored a list of #GObjects.
	 **/
	public class Module : TypeModule {

		[Description(nick="Filename", blurb="Library filename for use with GModule")]
		public string filename { get; construct set; }

		private GLib.Module library;
		private InitFunc init;
		private	new FinalizeFunc finalize;

		public delegate void InitFunc (Module module);
		public delegate void FinalizeFunc ();

		public override bool load () {
			library = GLib.Module.open (filename, ModuleFlags.BIND_MASK);
			if (library == null)
				return false;
				
			void * init_function = null;
			void * finalize_function = null;
				
			if (!library.symbol ("gnome_scan_module_init", out init_function) ||
				!library.symbol ("gnome_scan_module_finalize", out finalize_function))
			{
				warning ("%s does not contain a valid %s", filename, ( this.get_type().name() ));
				return false;
			}
				
			init = (InitFunc) init_function;
			finalize = (FinalizeFunc) finalize_function;
				
			init (this);	
				
			return true;
		}
			
		public override void unload () {
			finalize ();
		}
			

		/**
		 * gnome_scan_module_new:
		 * @filename: The path to shared object
		 * 
		 * Open and initialize a new module. This function should be used only
		 * by #GnomeScanModuleManager.
		 * 
		 * Returns: a new #GnomeScanModule
		 **/
		public Module (string filename) {
			this.filename = filename;
		}			
	}
}
