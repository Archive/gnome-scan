/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;
using Gegl;

namespace Gnome.Scan {
	public class Job : Object {
		public Status status {get; set; default = Status.INITIALIZING;}

		private Scanner _scanner;
		public Scanner scanner {
			get {
				return this._scanner;
			}
			set construct {
				Scanner old = this._scanner;
				this._scanner = value;
				this.register_node(value, old);
			}
		}

		private Sink _sink;
		public Sink sink {
			get {
				return this._sink;
			}
			set {
				Sink old = this._sink;
				this._sink = value;
				this.register_node(value, old);
			}
		}

		private double 	_progress = 0;
		public double progress {
			get {
				return this._progress;
			}
		}

		private SList<Preselection> preselections;
		private bool cancelled = false;
		private Gegl.Node load_buffer;
		private SList<Gegl.Node> graph;
		private Gegl.Node gegl_sink;
		private Gegl.Processor processor = null;
		private SList<Gnome.Scan.Node> nodes = null;

		public Job(Sink sink)
		{
			this.sink = sink;
		}

		public void register_preselection(Preselection preselection)
		{
			preselections.append(preselection);
		}

		public bool is_running()
		{
			return this._status == Status.PROCESSING;
		}

		public void run()
		{
			this.cancelled = false;
			int count = 0;

			if (!this.is_running()) {
				foreach(Gnome.Scan.Node node in this.nodes) {
					node.start_scan();
				}
				this.status = Status.PROCESSING;
			}

			while(run_once())
				count++;

			debug("%d images acquired", count);
			this.status = Status.DONE;
		}

		public void end()
		{
			if (!this.is_running())
				return;

			foreach(Gnome.Scan.Node node in this.nodes) {
				node.end_scan();
			}

			this.status = Status.READY;
		}

		public void cancel()
		{
			this.cancelled = true;
		}

		public bool run_once()
		{
			foreach(weak Gnome.Scan.Node node in this.nodes) {
				if (!node.start_image() && node is Gnome.Scan.Scanner) {
					return false;
				}
			}

			double progress = 0.0;
			bool scanner_is_node = this._scanner.nodes.length() == 0;
			if (scanner_is_node) {
				while(!this.cancelled && this._scanner.work(out progress)) {
					this._progress = progress/3.0;
				}
				this.load_buffer.set("operation", "gegl:load-buffer",
									 "buffer", this._scanner.buffer);
			}

			processor = new Gegl.Processor(this.gegl_sink, null);
			while(!this.cancelled && processor.work(out progress)) {
				if (scanner_is_node)
					this._progress = 0.333 + progress*2/3;
				else
					this._progress = progress;
			}

			foreach(weak Gnome.Scan.Node node in this.nodes)
				node.end_image();

			return true;
		}


		private void register_node(Node? node, Node? old)
		{
			if (old != null) {
				old.notify["status"]	-= this.check_nodes_status;
				old.notify["graph"]		-= this.build_graph;
				this.nodes.remove(old);
			}

			if (node != null) {
				this.apply_preselections(node);

				node.notify["status"]	+= this.check_nodes_status;
				node.notify["graph"]	+= this.build_graph;

				if (node is Scanner)
					this.nodes.prepend(node);
				else if (node is Sink)
					this.nodes.append(node);
				else {
					weak SList<Node> last = this.nodes.last();
					if (last.data is Sink)
						this.nodes.insert_before(last, node);
					else
						this.nodes.append(node);
				}
			}

			this.check_nodes_status();
			this.build_graph();
		}

		private void apply_preselections(Node node)
		{
			foreach(Preselection presel in preselections) {
				var option = node.lookup_option(presel.option_name);
				if (option != null)
					presel.apply(option);
			}
		}

		private void build_graph()
		{
			if ((int)this._status < (int)Status.READY)
				return;

			weak Gegl.Node prev = null;

			// remove all children from current graph
			foreach(Gegl.Node node in this.graph) {
				if (prev != null)
					node.disconnect("input");
				prev = node;
			}
			this.graph = null;

			// list all nodes
			prev = null;
			if (this._scanner.nodes.length() == 0) {
				this.load_buffer = new Gegl.Node();
				this.load_buffer.set("operation", "gegl:load-buffer");
				prev = this.load_buffer;
			}
			else {
				this.load_buffer = null;
			}

			foreach(weak Gnome.Scan.Node gsnode in this.nodes) {
				foreach(weak Gegl.Node gnode in gsnode.nodes) {
					this.graph.append(gnode);
					if (prev != null) {
						prev.link(gnode);
					}
					prev = gnode;
				}
			}
			this.gegl_sink = prev;
		}

		private void check_nodes_status()
		{
			Status status;
			if (this._scanner == null
				|| this._sink == null || this._sink.nodes.length() == 0)
				this.status = Status.UNCONFIGURED;
			else {
				status = Status.READY;
				if (this._scanner != null && ((int)this._scanner.status) < ((int)status))
					status = this._scanner.status;

				if (this._sink != null && ((int)this._sink.status) < ((int)status))
					status = this._sink.status;

				var redo_graph = (int)status > (int)this._status && status == Status.READY;

				this.status = status;

				// auto cancel job is status is not ready (failure,
				// etc.)
				if ((int)status < (int)Status.READY)
					this.cancelled = true;

				if (redo_graph)
					this.build_graph();
			}
		}
	}
}
