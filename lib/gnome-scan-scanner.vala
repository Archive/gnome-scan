/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using GLib;

namespace Gnome.Scan {
    public abstract class Scanner : Node {
		public string name		{set construct; get;}
		public string blurb		{set construct; get;}
		public string icon_name {set construct; get;}
		public Gegl.Buffer buffer {set; get;}
		public Extent extent { set; get; }

		construct {
			this.extent = Extent(0.0, 0.0);
		}

		// return true while there is more work to do.
		public virtual bool work(out double progress)
		{
			progress = 1.0;
			warning("Fake processor for %s", this.get_type().name());
			return false;
		}
    }
}
