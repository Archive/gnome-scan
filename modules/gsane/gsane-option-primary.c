/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include "gsane-option-primary.h"

G_DEFINE_TYPE(GSaneOptionPrimary, gsane_option_primary, GSANE_TYPE_OPTION_HANDLER_GENERIC)

static void
gsane_option_primary_init(GSaneOptionPrimary* self)
{
}

static void
gsane_option_primary_class_init(GSaneOptionPrimaryClass *klass)
{
	GSaneOptionHandlerGenericClass* hg_class = GSANE_OPTION_HANDLER_GENERIC_CLASS(klass);
	hg_class->option_hint = GNOME_SCAN_OPTION_HINT_PRIMARY;
}
