/* Gnome Scan - Scan as easy as you print
 * Copyright © 2007  Étienne Bersac <bersace@gnome.org>
 *
 * Gnome Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#if	HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>
#include <gmodule.h>
#include <sane/sane.h>
#include <gnome-scan.h>
#include "gsane-common.h"
#include "gsane-option-manager.h"
#include "gsane-option-handler-generic.h"
#include "gsane-option-source.h"
#include "gsane-option-primary.h"
#include "gsane-option-area.h"
#include "gsane-backend.h"
#include "gsane-scanner.h"

static GSaneOptionManager *gsane_option_manager;

G_MODULE_EXPORT void
gnome_scan_module_init (GnomeScanModule *module)
{
	SANE_Status status;
	SANE_Int version;
	/* TODO: version checking */
	status = sane_init(&version, NULL);
	bind_textdomain_codeset("sane-backends","UTF-8");

	g_message (G_STRLOC ": SANE version is %i.%i.%i for GSANE %s",
		   SANE_VERSION_MAJOR(version),
		   SANE_VERSION_MINOR(version),
		   SANE_VERSION_BUILD(version),
		   PACKAGE_VERSION);
	
	if (SANE_VERSION_MAJOR(version) != SANE_CURRENT_MAJOR) {
		g_warning (G_STRLOC ": SANE major version must be %i.",
			   SANE_CURRENT_MAJOR);
		return;
	}
	
	gsane_backend_register (G_TYPE_MODULE (module));
	gsane_scanner_register (G_TYPE_MODULE (module));

	/* GSane option handling */
	gsane_option_manager = gsane_option_manager_new();
	gsane_option_manager_add_rule_by_type(gsane_option_manager, SANE_TYPE_BOOL, GSANE_TYPE_OPTION_HANDLER_GENERIC);
	gsane_option_manager_add_rule_by_type(gsane_option_manager, SANE_TYPE_INT, GSANE_TYPE_OPTION_HANDLER_GENERIC);
	gsane_option_manager_add_rule_by_type(gsane_option_manager, SANE_TYPE_FIXED, GSANE_TYPE_OPTION_HANDLER_GENERIC);
	gsane_option_manager_add_rule_by_type(gsane_option_manager, SANE_TYPE_STRING, GSANE_TYPE_OPTION_HANDLER_GENERIC);
	gsane_option_manager_add_rules_by_name(gsane_option_manager, GSANE_TYPE_OPTION_SOURCE, "source", "doc-source", NULL);
	gsane_option_manager_add_rules_by_name(gsane_option_manager, GSANE_TYPE_OPTION_PRIMARY, "resolution", "mode", NULL);
	gsane_option_manager_add_rules_by_name(gsane_option_manager, GSANE_TYPE_OPTION_AREA, "tl-x", "tl-y", "br-x", "br-y", NULL);
}

G_MODULE_EXPORT void
gnome_scan_module_finalize (GnomeScanModule *module)
{
	/* TODO: backend and scanners */
	gsane_option_manager_destroy(gsane_option_manager);
	sane_exit();
}

