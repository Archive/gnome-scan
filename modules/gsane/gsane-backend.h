/* Gnome Scan - Scan as easy as you print
 * Copyright © 2007  Étienne Bersac <bersace@gnome.org>
 *
 * Gnome Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */


#ifndef _GSANE_BACKEND_H_
#define _GSANE_BACKEND_H_

#include <glib-object.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define GSANE_TYPE_BACKEND             (gsane_backend_get_type ())
#define GSANE_BACKEND(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_TYPE_SCAN_BACKEND_SANE, GSaneBackend))
#define GSANE_BACKEND_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_TYPE_SCAN_BACKEND_SANE, GSaneBackendClass))
#define GNOME_IS_SCAN_BACKEND_SANE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_TYPE_SCAN_BACKEND_SANE))
#define GNOME_IS_SCAN_BACKEND_SANE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_SCAN_BACKEND_SANE))
#define GSANE_BACKEND_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_TYPE_SCAN_BACKEND_SANE, GSaneBackendClass))

typedef struct _GSaneBackendClass GSaneBackendClass;
typedef struct _GSaneBackend GSaneBackend;

struct _GSaneBackendClass
{
	GnomeScanBackendClass parent_class;
};

struct _GSaneBackend
{
	GnomeScanBackend parent_instance;
};

GType gsane_backend_get_type (void) G_GNUC_CONST;
void gsane_backend_register(GTypeModule *module);

G_END_DECLS

#endif /* _GSANE_BACKEND_H_ */
