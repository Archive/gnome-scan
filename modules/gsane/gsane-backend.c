/* Gnome Scan - Scan as easy as you print
 * Copyright © 2007  Étienne Bersac <bersace@gnome.org>
 *
 * Gnome Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include <sane/sane.h>
#include "gsane-backend.h"
#include "gsane-scanner.h"

static GnomeScanBackendClass* parent_class = NULL;

G_DEFINE_DYNAMIC_TYPE (GSaneBackend, gsane_backend, GNOME_SCAN_TYPE_BACKEND);


static void*
gsane_backend_probe_scanners(GnomeScanBackend *backend)
{
	const SANE_Device **devices;
	SANE_Status status;
	GnomeScanScanner *scanner;
	gint i;
	
	status = sane_get_devices(&devices, SANE_FALSE);
	
	for (i = 0; devices[i]; i++) {
		scanner = gsane_scanner_new(devices[i]);
		if (scanner) {
			g_signal_emit_by_name(backend, "scanner-added", scanner);
			g_object_unref (scanner);
		}
		else {
			g_debug("SANE device %s failed or ignored",
				devices[i]->name);
		}
	}

	g_signal_emit_by_name(backend, "probe-done");
	return NULL;
}

static void
gsane_backend_init (GSaneBackend *object)
{
}

static void
gsane_backend_finalize (GObject *object)
{
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gsane_backend_class_init (GSaneBackendClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	GnomeScanBackendClass *backend_class = GNOME_SCAN_BACKEND_CLASS (klass);
	
	object_class->finalize = gsane_backend_finalize;
	backend_class->probe_scanners = gsane_backend_probe_scanners;
}

static void
gsane_backend_class_finalize(GSaneBackendClass *klass)
{
}

void
gsane_backend_register(GTypeModule *module)
{
	gsane_backend_register_type(module);
}
