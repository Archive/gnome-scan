/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include "gsane-option-manager.h"

#define	GSANE_OPTION_MANAGER_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE((o), GSANE_TYPE_OPTION_MANAGER, GSaneOptionManagerPrivate))

struct _GSaneOptionManagerPrivate
{
	GHashTable*	name_rules;
	GHashTable*	type_rules;
};

/* singleton instance pointer */
static GSaneOptionManager* gsane_option_manager = NULL;

GSaneOptionManager*
gsane_option_manager_new()
{
	if (G_UNLIKELY(gsane_option_manager == NULL)) {
		gsane_option_manager = GSANE_OPTION_MANAGER(g_type_create_instance(GSANE_TYPE_OPTION_MANAGER));
	}

	return gsane_option_manager;
}

void
gsane_option_manager_destroy(GSaneOptionManager *self)
{
	g_type_free_instance((GTypeInstance*)self);
}

void
gsane_option_manager_add_rule_by_name(GSaneOptionManager* self, const gchar* name, GType handler_type)
{
	g_hash_table_insert(self->priv->name_rules, g_strdup(name), (gpointer)handler_type);
}

void
gsane_option_manager_add_rules_by_name(GSaneOptionManager* self, GType handler_type, ...)
{
	va_list names;
	gchar* name;
	va_start(names, handler_type);
	while ((name = (gchar*) va_arg(names, gchar*)) != NULL)
		gsane_option_manager_add_rule_by_name(self, name, handler_type);
	va_end(names);
}

void
gsane_option_manager_add_rule_by_type(GSaneOptionManager* self, SANE_Value_Type value_type, GType handler_type)
{
	g_hash_table_insert(self->priv->type_rules, (gpointer)value_type, (gpointer) handler_type);
}

GType
gsane_option_manager_get_handler_type(GSaneOptionManager* self, const SANE_Option_Descriptor* desc)
{
	GType type = G_TYPE_INVALID;

	type = (GType) g_hash_table_lookup(self->priv->name_rules, desc->name);
	if (type != G_TYPE_INVALID)
		return type;

	type = (GType) g_hash_table_lookup(self->priv->type_rules, (gpointer)desc->type);
	if (type != G_TYPE_INVALID)
		return type;

	return G_TYPE_INVALID;
}

/* GLib type boiler plate code */
void
gsane_option_manager_class_init(gpointer g_class, gpointer class_data)
{
	g_type_class_add_private(g_class, sizeof(GSaneOptionManagerPrivate));
}

void
gsane_option_manager_instance_init(GTypeInstance* instance, gpointer g_class)
{
	GSaneOptionManager *self = GSANE_OPTION_MANAGER(instance);
	self->priv = GSANE_OPTION_MANAGER_GET_PRIVATE(instance);
	self->priv->name_rules = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
	self->priv->type_rules = g_hash_table_new(g_direct_hash, g_direct_equal);
}

GType
gsane_option_manager_get_type()
{
	static GType type;
	static const GTypeInfo tinfo = {
		.class_size	= sizeof(GSaneOptionManagerClass),
		.base_init	= NULL,
		.base_finalize	= NULL,
		.class_init	= gsane_option_manager_class_init,
		.class_finalize	= NULL,
		.class_data	= NULL,
		.instance_size	= sizeof(GSaneOptionManager),
		.n_preallocs	= 0,
		.instance_init	= gsane_option_manager_instance_init,
		.value_table	= NULL,
	};
	static const GTypeFundamentalInfo finfo = {
		.type_flags	= G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE,
	};

	if (G_UNLIKELY(type == 0)) {
		type = g_type_fundamental_next();
		g_type_register_fundamental(type, "GSaneOptionManager", &tinfo, &finfo, 0);
	}

	return type;
}
