/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-scan
 * Copyright (C) Étienne Bersac 2007 <bersace03@laposte.net>
 * 
 * gnome-scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#if	HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>
#include "gsane-common.h"

GQuark
gsane_sane_error_quark()
{
	return g_quark_from_static_string("gsane-sane-error");
}

guint
gsane_status_to_error_code(SANE_Status status)
{

	switch(status) {
	case SANE_STATUS_UNSUPPORTED:
		return GSANE_ERROR_UNSUPPORTED;
		break;
	case SANE_STATUS_CANCELLED:
		return GSANE_ERROR_CANCELLED;
		break;
	case SANE_STATUS_DEVICE_BUSY:
		return GSANE_ERROR_BUSY;
		break;
	case SANE_STATUS_INVAL:
		return GSANE_ERROR_INVALID_VALUE;
		break;
	case SANE_STATUS_JAMMED:
		return GSANE_ERROR_JAMMED;
		break;
	case SANE_STATUS_NO_DOCS:
		return GSANE_ERROR_NO_DOCS;
		break;
	case SANE_STATUS_COVER_OPEN:
		return GSANE_ERROR_COVER_OPEN;
		break;
	case SANE_STATUS_IO_ERROR:
		return GSANE_ERROR_IO_ERROR;
		break;
	case SANE_STATUS_NO_MEM:
		return GSANE_ERROR_NO_MEM;
		break;
	case SANE_STATUS_ACCESS_DENIED:
		return GSANE_ERROR_DENIED;
		break;
	default:
		return GNOME_SCAN_STATUS_UNKNOWN;
		break;
	}
}

GError*
gsane_error_new_from_status(SANE_Status status)
{
	guint code;

	if (status == SANE_STATUS_GOOD)
		return NULL;

	code = gsane_status_to_error_code(status);

	return g_error_new_literal(GSANE_ERROR, code, S_(sane_strstatus(status)));
}

void
gsane_propagate_status(GError **dest, SANE_Status status)
{
	if (dest == NULL) {
		g_warning("Unhandled SANE error : %s", S_(sane_strstatus(status)));
		return;
	}

	g_propagate_error(dest, gsane_error_new_from_status(status));
}

void
gsane_propagate_prefixed_status(GError **dest, SANE_Status status, const gchar* format, ...)
{
	gchar* prefix;

	va_list ap;
	va_start (ap, format);
	prefix = g_strdup_vprintf(format, ap);
	va_end (ap);

	if (dest == NULL) {
		g_warning("%s: Unhandled SANE error : %s", prefix, S_(sane_strstatus(status)));
		return;
	}

	g_propagate_error(dest, gsane_error_new_from_status(status));
	g_prefix_error(dest, "%s", prefix);
}


gboolean
gsane_string_in_array(const gchar* str, const gchar** array)
{
	for(; *array; array++)
		if (g_str_equal(str, *array))
			return TRUE;
	return FALSE;
}
