/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include "gsane-processor.h"
#include <string.h>
#include <math.h>
#include <config.h>

typedef void (*GSaneProcessorFunc) (GSaneProcessor *self, guchar *buf, guint buf_len);

struct _GSaneProcessorPrivate {
	const SANE_Parameters *params;
	/* output format of processor */
	Babl* format;
	/* target buffer */
	GeglBuffer *buffer;
	/* rect representing the current buffer being processed. Might
	   make sense to add a param instead ? */
	volatile GeglRectangle rect;
	/* The amount of pixel represented in the current buffer
	   returned by SANE */
	volatile guint pixels_in_buf;
	/* total bytes read*/
	volatile guint bytes_processed;
	/* total number of frame to acquire to get the entire image */
	guint frame_count;
	/* number of sample in frame */
	guint sample_count;
	/* offset of a sample in bytes for three-pass acquisition */
	guint sample_offset;
	/* maximum value for a sample, needed for non regular depth */
	guint32 max_sample_value;
	/* maximum value for a sample for Babl */
	guint32 max_target_sample_value;
	/* exact portion of bytes used to représent one pixel in the frame */
	gdouble bytes_per_pixel;
	/* minimal amount of bytes needed to contain one pixel, this
	   is usualy the final stride since we don't convert but just
	   normalize to 8bit-multiple depth. */
	guint pixel_stride;
	/* minimal amount of bytes needed to contain one sample in the frame */
	guint sample_stride;
	/* number of bits needed for a pixel, useful for arbitrary depth */
	guint bits_per_pixel;
	/* pattern to match one pixel value in word */
	guint32 pixel_pattern;
	/* pattern to match on sample in a word */
	guint32 sample_pattern;
	/* pointer to the processor function */
	GSaneProcessorFunc process;
	/* target sample value for SANE 1bit sample value, it changes
	   whether it's grey or RGB :/ . (see SANE 1.X standard). So
	   we swap min and max value upon frame configuration to
	   transparently handle this special case. */
	guchar minval;
	guchar maxval;
};


void
gsane_processor_init(GSaneProcessor *self)
{
	self->priv = g_new0(GSaneProcessorPrivate, 1);
	return;
}


static void
gsane_processor_process_void(GSaneProcessor *self, guchar *buf, guint buf_len)
{
}

static void
gsane_processor_process_1bit(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	GeglRectangle roi = self->priv->rect;
	guchar*buf8 = g_new0(guchar, buf_len*8);
	guint i, j;
	for (i = 0; i < buf_len; i++)
		for (j = 0; j < 8; j++)
			buf8[i * 8 + j] = (buf[i] & (1 << (7 - j))) ? self->priv->maxval : self->priv->minval;
	gegl_buffer_set(self->priv->buffer, &roi, self->priv->format, buf8, GEGL_AUTO_ROWSTRIDE);
	g_free(buf8);
}

/* process Y or RGB 8/16 bit into … 8/16 bit. */
static void
gsane_processor_process_8bit(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	GeglRectangle roi = self->priv->rect;
	gegl_buffer_set (self->priv->buffer, &roi, self->priv->format, buf, GEGL_AUTO_ROWSTRIDE);
}

static void
gsane_processor_process_nbit(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	GeglRectangle roi = self->priv->rect;
	guint i;
	guint32 value = 0;	/* support up to 32 bit sample, we
				   don't use an array due to bit
				   operation */
	guint src_pos;
	guint offset;
#if !ENABLE_GEGL_010	
	guchar *src, *dest, *buf8 = g_new0(guchar, self->priv->pixels_in_buf * self->priv->format->format.bytes_per_pixel);
#else
	guchar *src, *dest, *buf8 = g_new0(guchar, self->priv->pixels_in_buf * babl_format_get_bytes_per_pixel(self->priv->format));
#endif
	guint samples_in_buf = self->priv->pixels_in_buf * self->priv->sample_count;
	for (i = 0 ; i < samples_in_buf ; i++) {
		/* compute the address of the first byte container sample value */
		src_pos = i * self->priv->bytes_per_pixel / self->priv->sample_count;
		/* retrieve bytes containing sample value */
		memcpy(&value, buf+src_pos, self->priv->sample_stride);
		/* At which bit from right of value start the sample. */
		offset = (i * self->priv->params->depth) - (src_pos * 8);
		/* align sample bits to the right */
		value = value >> offset;
		/* apply sample patter to get sample value */
		value = value & self->priv->sample_pattern;
		/* compute target value */
		value = (gdouble)self->priv->max_target_sample_value * ((gdouble)value / (gdouble)self->priv->max_sample_value);

		/* get the address of the first byte set. */
		src = (guchar*)(&value);
#if G_BYTE_ORDER == G_BIG_ENDIAN
		src = src + (sizeof(value) - self->priv->sample_stride);
#endif
		/* compute the first byte target address */
		dest = buf8 + i * self->priv->sample_stride;
		/* save */
		memcpy(dest, src, self->priv->sample_stride);
	}
	/* send */
	gegl_buffer_set(self->priv->buffer, &roi, self->priv->format, buf8, GEGL_AUTO_ROWSTRIDE);
	g_free(buf8);
}
static void
gsane_processor_process_three_pass_1bit(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	GeglRectangle roi = self->priv->rect;
	guchar *buf3 = g_new0(guchar, self->priv->frame_count * buf_len * 8);
	guint i, j;
	guint dest;
	gegl_buffer_get(self->priv->buffer, 1.0, &roi, self->priv->format, buf3, GEGL_AUTO_ROWSTRIDE);
	for (i = 0; i < buf_len; i++) {
		for (j = 0; j < 8; j++) {
			dest = (i * 8 + j) * self->priv->frame_count + self->priv->sample_offset;
			buf3[dest] = (buf[i] & (1 << (7 - j))) ? 0xFF : 0x00;
		}
	}
	/* send */
	gegl_buffer_set(self->priv->buffer, &roi, self->priv->format, buf3, GEGL_AUTO_ROWSTRIDE);
	g_free(buf3);
}

static void
gsane_processor_process_three_pass_8bit(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	GeglRectangle roi = self->priv->rect;
	guchar *buf3 = g_new0(guchar, self->priv->frame_count * buf_len);
	guint i;
	guint src_pos, dest_pos;
	gegl_buffer_get(self->priv->buffer, 1.0, &roi, self->priv->format, buf3, GEGL_AUTO_ROWSTRIDE);
	/* copy pixel per pixel from buf to buf3 */
	for (i = 0; i < self->priv->pixels_in_buf; i++) {
		/* pos of pixel i in buf */
		src_pos = i * self->priv->bytes_per_pixel;
		/* pos of pixel i in buf3 */
#if !ENABLE_GEGL_010
		dest_pos = i * self->priv->format->format.bytes_per_pixel + self->priv->sample_offset;
#else
		dest_pos = i * babl_format_get_bytes_per_pixel(self->priv->format) + self->priv->sample_offset;
#endif
		/* save */
		memcpy(buf3+dest_pos, buf+src_pos, self->priv->bytes_per_pixel);
	}
	/* send */
	gegl_buffer_set(self->priv->buffer, &roi, self->priv->format, buf3, GEGL_AUTO_ROWSTRIDE);
	g_free(buf3);
}


static void
gsane_processor_process_three_pass_nbit(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	GeglRectangle roi = self->priv->rect;
	guint32 value = 0;
	guchar *src, *dest, *buf8 = g_new0(guchar, self->priv->pixels_in_buf * self->priv->sample_stride * self->priv->frame_count);
	guint i, offset, src_pos;
	gegl_buffer_get(self->priv->buffer, 1.0, &roi, self->priv->format, buf8, GEGL_AUTO_ROWSTRIDE);
	for (i = 0; i < self->priv->pixels_in_buf; i++) {
		/* compute the address of the first byte container sample value */
		src_pos = i* self->priv->bytes_per_pixel;
		/* retrieve bytes containing sample value */
		memcpy(&value, buf + src_pos, self->priv->sample_stride);
		/* compute the offset of the samples bits inside the sample stride */
		offset = (8 * self->priv->sample_stride) - ((i * self->priv->params->depth) - (src_pos * 8)) - 1;
		/* align sample bits to the right */
		value = value >> offset;
		/* apply sample patter to get sample value */
		value = value & self->priv->sample_pattern;
		/* compute target value */
		value = self->priv->max_target_sample_value * (value / self->priv->max_sample_value);
		/* get the address of the first byte set. */
		src = (guchar*)(&value);
#if G_BYTE_ORDER == G_BIG_ENDIAN
		src = src + (sizeof(value) - self->priv->sample_stride);
#endif
		/* compute the first byte target address */
		dest = buf8 + i * self->priv->frame_count * self->priv->sample_stride  + self->priv->sample_offset;
		/* save */
		memcpy(dest, src, self->priv->sample_stride);
	}
	/* send */
	gegl_buffer_set(self->priv->buffer, &roi, self->priv->format, buf8, GEGL_AUTO_ROWSTRIDE);
	g_free(buf8);
}

static GSaneProcessorFunc
gsane_processor_get_func(GSaneProcessor *self)
{
	GSaneProcessorFunc func = gsane_processor_process_void;
	switch(self->priv->params->format) {
	case SANE_FRAME_RGB:
	case SANE_FRAME_GRAY:
		if (self->priv->params->depth % 8 == 0)
			func = gsane_processor_process_8bit;
		else if (self->priv->params->depth == 1)
			func = gsane_processor_process_1bit;
		else
			func = gsane_processor_process_nbit;
		break;
	case SANE_FRAME_RED:
	case SANE_FRAME_GREEN:
	case SANE_FRAME_BLUE:
		if (self->priv->params->depth % 8 == 0)
			func = gsane_processor_process_three_pass_8bit;
		else if (self->priv->params->depth == 1)
			func = gsane_processor_process_three_pass_1bit;
		else
			func = gsane_processor_process_three_pass_nbit;
		break;
	default:
		g_warning("Unsupported SANE frame format.");
		break;
	}
	return func;
}

static const gchar*
gsane_processor_get_babl_color_model(GSaneProcessor *self)
{
	const gchar *model = NULL;

	switch(self->priv->params->format) {
	case SANE_FRAME_GRAY:
		model = "Y";
		break;
	case SANE_FRAME_RGB:
	case SANE_FRAME_RED:
	case SANE_FRAME_GREEN:
	case SANE_FRAME_BLUE:
		model = "RGB";
		break;
	default:
		g_warning("Unsupported SANE frame format.");
		break;
	}

	return model;
}

static inline guint
gsane_processor_get_color_depth(GSaneProcessor *self)
{
	return MAX(8, (((self->priv->params->depth + 7)/8)*8));
}

/* Compute the pixel format in which data from scanner will be
   translated to. */
static Babl*
gsane_processor_get_babl_format(GSaneProcessor *self)
{
	const gchar* model = gsane_processor_get_babl_color_model(self);
	if (!model)
		return NULL;
	guint depth = gsane_processor_get_color_depth(self);
	gchar* name = g_strdup_printf("%s u%d", model, depth);
	g_debug("Format is %s", name);
	return babl_format(name);
}

/* Return the offset in pixel stride of the samples acquired in
   current frame. */
static guint
gsane_processor_get_sample_offset(GSaneProcessor *self)
{
	switch(self->priv->params->format) {
	case SANE_FRAME_RED:
		return 0;
	case SANE_FRAME_GREEN:
		return self->priv->sample_stride;
	case SANE_FRAME_BLUE:
		return 2 * self->priv->sample_stride;
	default:
		return 0;
	}
}

static guint
gsane_processor_get_sample_count(GSaneProcessor *self)
{
	switch(self->priv->params->format) {
	case SANE_FRAME_RGB:
		return 3;
	case SANE_FRAME_GRAY:
	case SANE_FRAME_RED:
	case SANE_FRAME_GREEN:
	case SANE_FRAME_BLUE:
		return 1;
		break;
	default:
		g_warning("Unsupported SANE frame format.");
		break;
	}

	return 0;
}

/* Initialize acquisition of one image. Returns the buffer initialized
   with right format and extent. */
GeglBuffer*
gsane_processor_prepare_image(GSaneProcessor *self, SANE_Parameters* params, guint frame_count)
{
	GeglRectangle extent = {
		.x = 0, .y = 0,
		.width = params->pixels_per_line,
		/* we set an arbirary huge height for unknown height,
		   GSaneScanner takes care of resizing at end of frame
		   acquisition */
		.height = (params->lines == -1 ? 65535 : params->lines),
	};

	self->priv->params = params;
	/* computes values used by various processor func */
	self->priv->frame_count		= frame_count;
	self->priv->sample_count	= gsane_processor_get_sample_count(self);
	self->priv->bits_per_pixel	= params->depth * self->priv->sample_count;
	self->priv->bytes_per_pixel	= (gdouble) self->priv->bits_per_pixel / 8.;
	self->priv->pixel_stride	= floor(self->priv->bytes_per_pixel) + (self->priv->bytes_per_pixel > (gdouble)((guint) self->priv->bytes_per_pixel) ? 1 : 0);
	self->priv->max_sample_value	= (0xFFFFFFFF) >> (32 - self->priv->params->depth);
	self->priv->pixel_pattern	= (0xFFFFFFFF) >> (32 - self->priv->bits_per_pixel);
	self->priv->sample_pattern	= (0xFFFFFFFF) >> (32 - self->priv->params->depth);

	self->priv->process = gsane_processor_get_func(self);
	g_return_val_if_fail(self->priv->process, NULL);

	self->priv->format = gsane_processor_get_babl_format(self);
	g_return_val_if_fail(self->priv->format, NULL);

#if !ENABLE_GEGL_010
	self->priv->sample_stride	= self->priv->format->format.bytes_per_pixel / MAX(self->priv->sample_count, self->priv->frame_count);
#else
	self->priv->sample_stride       = babl_format_get_bytes_per_pixel(self->priv->format) / MAX(self->priv->sample_count, self->priv->frame_count);
#endif
	self->priv->max_target_sample_value= (0xFFFFFFFF) >> (32 - self->priv->sample_stride * 8);

	self->priv->buffer = gegl_buffer_new(&extent, self->priv->format);
	return self->priv->buffer;
}

void
gsane_processor_prepare_frame(GSaneProcessor *self, SANE_Parameters* params)
{
	self->priv->params = params;
	self->priv->bytes_processed = 0;
	self->priv->sample_offset = gsane_processor_get_sample_offset(self);
	switch(params->format) {
	case SANE_FRAME_GRAY:
		self->priv->minval = 0xFF; /* 0 = white */
		self->priv->maxval = 0x00; /* 1 = black */
		break;
	default:
		self->priv->minval = 0x00; /* 0 = min (R, G or B) */
		self->priv->maxval = 0xFF; /* 1 = max */
		break;
	}
}

void
gsane_processor_process(GSaneProcessor *self, guchar *buf, guint buf_len)
{
	g_return_if_fail(self->priv->process);
	guchar* next_buf = NULL;
	guint next_buf_len = 0;
	guint bytes_processed = buf_len;

	/* define the rect corresponding buf */
	self->priv->rect.y = self->priv->bytes_processed / self->priv->params->bytes_per_line;
	self->priv->rect.x = self->priv->bytes_processed % self->priv->params->bytes_per_line;
	guint pixel_to_end_of_line = self->priv->params->pixels_per_line - self->priv->rect.x;
	self->priv->pixels_in_buf = (gdouble)buf_len / self->priv->params->bytes_per_line * self->priv->params->pixels_per_line;
	self->priv->rect.width = MIN (self->priv->pixels_in_buf - self->priv->rect.x, pixel_to_end_of_line);

	/* compute the height of the rect and determine whether buf is
	   in several rect */
	if (self->priv->rect.x > 0) {
		/* first rect is one line */
		self->priv->rect.height = 1;
		if (self->priv->pixels_in_buf > self->priv->rect.width) {
			/* recursion for the rest (will start at x=0) */
			bytes_processed = self->priv->rect.width * self->priv->bytes_per_pixel;
			next_buf_len = buf_len - bytes_processed;
			next_buf = buf + bytes_processed;
		}
	}
	else {
		/* multiline */
		self->priv->rect.height = self->priv->pixels_in_buf / self->priv->rect.width;
		guint last_line_length = self->priv->pixels_in_buf % self->priv->rect.width;
		if (last_line_length > 0) {
			/* the last line needs recursion */
			next_buf_len = last_line_length * self->priv->bytes_per_pixel;
			bytes_processed = buf_len - next_buf_len;
			next_buf = buf + bytes_processed;
		}
	}

	/* process */
	/* g_debug("Processing %dx%d+%d+%d", self->priv->rect.width, self->priv->rect.height, self->priv->rect.x, self->priv->rect.y); */
	self->priv->process(self, buf, buf_len);
	self->priv->bytes_processed+= bytes_processed;

	/* recurse for next roi */
	if (next_buf && next_buf_len)
		gsane_processor_process(self, next_buf, next_buf_len);
}

void
gsane_processor_destroy(GSaneProcessor *self)
{
	g_free(self->priv);
	return;
}
