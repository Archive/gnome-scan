/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-scan
 * Copyright (C) Étienne Bersac 2007 <bersace03@laposte.net>
 *
 * gnome-scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#ifndef _GSANE_COMMON_H_
#define _GSANE_COMMON_H_

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>
#include <sane/sane.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define	SANE_GETTEXT_PACKAGE	"sane-backends"
#define S_(s)					dgettext(SANE_GETTEXT_PACKAGE, s)

#if	DEBUG
#define	gs_debug(...)	g_debug (__VA_ARGS__)
#else
#define gs_debug(...)
#endif

#define	GSANE_BOOLEAN_TO_STRING(b)	(b ? "TRUE" : "FALSE")
#define GSANE_ACTION_TO_STRING(a)	(a == SANE_ACTION_GET_VALUE ? "get" : (a == SANE_ACTION_SET_VALUE ? "set" : (a == SANE_ACTION_SET_AUTO ? "auto-set" : "<unknown>")))

#define GSANE_ERROR	gsane_sane_error_quark()

typedef enum {
	GSANE_ERROR_UNSUPPORTED		= GNOME_SCAN_STATUS_FAILED + 1,
    GSANE_ERROR_INVALID_VALUE	= GNOME_SCAN_STATUS_FAILED + 2,
    GSANE_ERROR_JAMMED			= GNOME_SCAN_STATUS_FAILED + 3,
    GSANE_ERROR_NO_DOCS			= GNOME_SCAN_STATUS_FAILED + 4,
    GSANE_ERROR_COVER_OPEN		= GNOME_SCAN_STATUS_FAILED + 5,
    GSANE_ERROR_IO_ERROR		= GNOME_SCAN_STATUS_FAILED + 6,
    GSANE_ERROR_NO_MEM			= GNOME_SCAN_STATUS_FAILED + 7,
    GSANE_ERROR_DENIED			= GNOME_SCAN_STATUS_FAILED + 8,
	GSANE_ERROR_CANCELLED		= GNOME_SCAN_STATUS_DONE + 1,
    GSANE_ERROR_BUSY			= GNOME_SCAN_STATUS_BUSY + 1,
	GSANE_ERROR_OPTION_INACTIVE,
	GSANE_ERROR_OPTION_READONLY,
} GSaneError;

GQuark gsane_sane_error_quark(void) G_GNUC_CONST;
guint gsane_status_to_error_code(SANE_Status status);
GError* gsane_error_new_from_status(SANE_Status status);
void gsane_propagate_status(GError **dest, SANE_Status status);
void gsane_propagate_prefixed_status(GError **dest, SANE_Status status, const gchar* format, ...);
gboolean gsane_string_in_array(const gchar* str, const gchar** array);

G_END_DECLS

#endif /* _GSANE_COMMON_H_ */
