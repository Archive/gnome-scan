/* GSane - SANE GNOME Scan backend
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include "gsane-option-handler.h"
#include "gsane-scanner.h"
#include "gsane-common.h"

#define	GSANE_OPTION_HANDLER_GET_PRIVATE(obj)	(G_TYPE_INSTANCE_GET_PRIVATE((obj), GSANE_TYPE_OPTION_HANDLER, GSaneOptionHandlerPrivate))

struct _GSaneOptionHandlerPrivate
{
	/* pointer to the SANE handle owned by the GSaneScanner */
	SANE_Handle handle;
};

GnomeScanOptionHint
gsane_option_hint(const SANE_Option_Descriptor*desc)
{
	if (desc->cap & SANE_CAP_ADVANCED) {
		return GNOME_SCAN_OPTION_HINT_SECONDARY;
	}
	else {
		return GNOME_SCAN_OPTION_HINT_PRIMARY;
	}
}

GnomeScanUnit
gsane_option_unit(const SANE_Option_Descriptor* desc)
{
	switch(desc->unit) {
	case SANE_UNIT_PIXEL:
		return GNOME_SCAN_UNIT_PIXEL;
	case SANE_UNIT_BIT:
		return GNOME_SCAN_UNIT_BIT;
	case SANE_UNIT_MM:
		return GNOME_SCAN_UNIT_MM;
	case SANE_UNIT_DPI:
		return GNOME_SCAN_UNIT_DPI;
	case SANE_UNIT_PERCENT:
		return GNOME_SCAN_UNIT_PERCENT;
	case SANE_UNIT_MICROSECOND:
		return GNOME_SCAN_UNIT_MICROSECOND;
	default:
		return GNOME_SCAN_UNIT_NONE;
	}
}

GSaneOptionHandler*
gsane_option_handler_new(GType type, GnomeScanScanner* scanner, SANE_Handle handle)
{
	GSaneOptionHandler *self = GSANE_OPTION_HANDLER(g_type_create_instance(type));
	self->priv->handle = handle;
	self->scanner = scanner;
	g_signal_connect_swapped(scanner, "reload-options", G_CALLBACK(gsane_option_handler_reload_options), self);
	return self;
}

void
gsane_option_handler_destroy(GSaneOptionHandler *self)
{
	g_type_free_instance((GTypeInstance*) self);
}

/* handle-options abstract method */
static void
gsane_option_handler_default_handle_option(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar* group)
{
	g_warning("gsane_option_handler_handle_option not implemented by %s", g_type_name(G_TYPE_FROM_INSTANCE(self)));
}

void
gsane_option_handler_handle_option(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group)
{
	GSANE_OPTION_HANDLER_GET_CLASS(self)->handle_option(self, desc, n, group);
}

/* reload-options abstract method */
static void
gsane_option_handler_default_reload_options(GSaneOptionHandler *self)
{
	g_warning("gsane_option_handler_reload_option not implemented by %s", g_type_name(G_TYPE_FROM_INSTANCE(self)));
}

void
gsane_option_handler_reload_options(GSaneOptionHandler *self)
{
	GSANE_OPTION_HANDLER_GET_CLASS(self)->reload_options(self);
}

const SANE_Option_Descriptor*
gsane_option_handler_get_sane_option_descriptor(GSaneOptionHandler *self, SANE_Int n)
{
	return sane_get_option_descriptor(self->priv->handle, n);
}

GSList*
gsane_option_handler_enum_list_int_values(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n)
{
	GSList *list = NULL;
	gint count = desc->constraint.word_list[0];
	gint i, intval;
	GValue* values = g_new0(GValue, count);
	GValue* value;
	GnomeScanEnumValue* evalues = g_new0(GnomeScanEnumValue, count);
	GnomeScanEnumValue* evalue;

	/* list values */
	for(i = 0; i < count; i++) {
		value = values+i;
		evalue = evalues+i;
		intval = desc->constraint.word_list[i+1];

		g_value_init(value, G_TYPE_INT);
		g_value_set_int(value, intval);

		gnome_scan_enum_value_init(evalue, value, g_strdup_printf("%i", intval), NULL);
		list = g_slist_append(list, evalue);
	}
	g_free(values);
	return list;
}

GSList*
gsane_option_handler_enum_list_double_values(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n)
{
	GSList *list = NULL;
	gint count = desc->constraint.word_list[0];
	gint i;
	gdouble doubleval;
	GValue *value, *values = g_new0(GValue, count);
	GnomeScanEnumValue *evalue, *evalues = g_new0(GnomeScanEnumValue, count);

	for(i = 0; i < count; i++) {
		value = values + i;
		evalue = evalues + i;
		doubleval = SANE_UNFIX(desc->constraint.word_list[i+1]);
		g_value_init(value, G_TYPE_DOUBLE);
		g_value_set_double(value, doubleval);
		gnome_scan_enum_value_init(evalue, value, g_strdup_printf("%.2f", doubleval), NULL);
		list = g_slist_append(list, evalue);
	}
	g_free(values);
	return list;
}

GSList*
gsane_option_handler_enum_list_string_values(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n)
{
	GSList *list = NULL;
	gint i;
	const gchar*strval;
	GValue *value;
	GnomeScanEnumValue *evalue;
	const SANE_String_Const *strlist = desc->constraint.string_list;

	for(i = 0; strlist[i]; i++) {
		strval = strlist[i];
		value = g_new0(GValue, 1);
		evalue = g_new0(GnomeScanEnumValue, 1);
		g_value_init(value, G_TYPE_STRING);
		g_value_set_string(value, strval);
		gnome_scan_enum_value_init(evalue, value, S_(strval), SANE_GETTEXT_PACKAGE);
		list = g_slist_append(list, evalue);
		g_free(value);
	}
	return list;
}


/* wrapper aroung sane_control_option handling errors and propagating
   reload signals */
static gboolean
gsane_option_handler_control_option(GSaneOptionHandler*self, const SANE_Option_Descriptor* desc, SANE_Int index, SANE_Action action, gpointer data, GError**error)
{
	SANE_Status status;
	SANE_Int flags = 0;

	if (!SANE_OPTION_IS_ACTIVE(desc->cap)) {
		g_propagate_error(error, g_error_new(GSANE_ERROR, GSANE_ERROR_OPTION_INACTIVE,
						     "Option %s is inactive", desc->name));
		return FALSE;
	}

	if (action == SANE_ACTION_SET_VALUE && !SANE_OPTION_IS_SETTABLE(desc->cap)) {
		g_propagate_error(error, g_error_new(GSANE_ERROR, GSANE_ERROR_OPTION_READONLY,
						     "Unable to set readonly option %s", desc->name));
		return FALSE;
	}

	status = sane_control_option(self->priv->handle, index, action, data, &flags);

	if (status != SANE_STATUS_GOOD) {
		gsane_propagate_prefixed_status(error, status,
						"%s %s(%d)", GSANE_ACTION_TO_STRING(action), desc->name, index);
		return FALSE;
	}

	gboolean inexact = FALSE;
	if (flags & SANE_INFO_INEXACT) {
		g_debug("Setting %s : value inexact", desc->name);
		inexact = TRUE;
	}
	if (flags & SANE_INFO_RELOAD_OPTIONS) {
		g_debug("Reload options");
		g_signal_emit_by_name(self->scanner, "reload-options");
		g_debug("Options reloaded");
	}
	if (flags & SANE_INFO_RELOAD_PARAMS)
		gsane_scanner_reload_parameters(GSANE_SCANNER(self->scanner));

	return inexact;
}


gboolean
gsane_option_handler_get_bool(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError**error)
{
	gboolean value = FALSE;
	gsane_option_handler_control_option(self, desc, index, SANE_ACTION_GET_VALUE, &value, error);
	g_debug("get %s(%d) = %x", desc->name, index, value);
	return value;
}

gboolean
gsane_option_handler_set_bool(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gboolean value, GError**error)
{
	g_debug("set %s(%d) = %x", desc->name, index, value);
	return gsane_option_handler_control_option(self, desc, index, SANE_ACTION_SET_VALUE, &value, error);
}


gint
gsane_option_handler_get_int(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error)
{
	gint value = 0;
	gsane_option_handler_control_option(self, desc, index, SANE_ACTION_GET_VALUE, &value, error);
	g_debug("get %s(%d) = %d", desc->name, index, value);
	return value;
}

gboolean
gsane_option_handler_set_int(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gint value, GError **error)
{
	g_debug("set %s(%d) = %d", desc->name, index, value);
	return gsane_option_handler_control_option(self, desc, index, SANE_ACTION_SET_VALUE, &value, error);
}


gdouble
gsane_option_handler_get_double(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error)
{
	SANE_Word data;
	gdouble value = 0.0;

	switch(desc->type) {
	case SANE_TYPE_INT:
		value = (gdouble) gsane_option_handler_get_int(self, desc, index, error);
		break;
	case SANE_TYPE_FIXED:
		gsane_option_handler_control_option(self, desc, index, SANE_ACTION_GET_VALUE, &data, error);
		value = SANE_UNFIX(data);
		g_debug("get %s(%d) = %f", desc->name, index, value);
		break;
	default:
		g_warning("Trying to get double value on a non number option.");
		break;
	}
	return value;
}

gboolean
gsane_option_handler_set_double(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gdouble value, GError **error)
{
	SANE_Fixed fixed;
	switch(desc->type) {
	case SANE_TYPE_INT:
		return gsane_option_handler_set_int(self, desc, index, (gint) value, error);
		break;
	case SANE_TYPE_FIXED:
		g_debug("set %s(%d) = %f", desc->name, index, value);
		fixed = SANE_FIX(value);
		return gsane_option_handler_control_option(self, desc, index, SANE_ACTION_SET_VALUE, &fixed, error);
		break;
	default:
		g_warning("Trying to get double value from non number option.");
		return FALSE;
	}
}


gchar*
gsane_option_handler_get_string(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error)
{
	gchar* data = g_new0(gchar, desc->size);
	gchar* value;
	gsane_option_handler_control_option(self, desc, index, SANE_ACTION_GET_VALUE, data, error);
	value = g_strdup(data);
	g_free(data);
	g_debug("get %s(%d) = '%s'", desc->name, index, value);
	return value;
}

gboolean
gsane_option_handler_set_string(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gchar* value, GError **error)
{
	g_debug("set %s(%d) = '%s'", desc->name, index, value);
	return gsane_option_handler_control_option(self, desc, index, SANE_ACTION_SET_VALUE, value, error);
}






GnomeScanEnumValue*
gsane_option_handler_get_enum(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GSList* values, GError **error)
{
	gint intval;
	gdouble doubleval;
	gchar* strval;
	GSList* iter;
	GnomeScanEnumValue* evalue;
	/* default to first */
	const GnomeScanEnumValue* retvalue = values->data;

	switch(desc->type) {
	case SANE_TYPE_INT:
		intval = gsane_option_handler_get_int(self, desc, index, error);
		for (iter = values; iter; iter = iter->next) {
			evalue = iter->data;
			if (g_value_get_int(&evalue->value) == intval) {
				retvalue = evalue;
				break;
			}
		}
		break;
	case SANE_TYPE_FIXED:
		doubleval = gsane_option_handler_get_double(self, desc, index, error);
		for (iter = values; iter; iter = iter->next) {
			evalue = iter->data;
			if (g_value_get_double(&evalue->value) == doubleval) {
				retvalue = evalue;
				break;
			}
		}
		break;
	case SANE_TYPE_STRING:
		strval = gsane_option_handler_get_string(self, desc, index, error);
		for (iter = values; iter; iter = iter->next) {
			evalue = iter->data;
			if (g_str_equal(g_value_get_string(&evalue->value), strval)) {
				retvalue = evalue;
				break;
			}
		}
		break;
	default:
		break;
	}

	if (retvalue)
		return gnome_scan_enum_value_dup(retvalue);
	else
		return NULL;
}

gboolean
gsane_option_handler_set_enum(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GnomeScanEnumValue* evalue, GError **error)
{
	switch(desc->type) {
	case SANE_TYPE_INT:
		return gsane_option_handler_set_int(self, desc, index, g_value_get_int(&evalue->value), error);
		break;
	case SANE_TYPE_FIXED:
		return gsane_option_handler_set_double(self, desc, index, g_value_get_double(&evalue->value), error);
		break;
	case SANE_TYPE_STRING:
		return gsane_option_handler_set_string(self, desc, index, g_value_dup_string(&evalue->value), error);
	default:
		return FALSE;
		break;
	}
}


/* GType instance boiler plate code */
void
gsane_option_handler_instance_init(GTypeInstance *instance, gpointer g_class)
{
	GSaneOptionHandler *self = GSANE_OPTION_HANDLER(instance);
	self->priv = GSANE_OPTION_HANDLER_GET_PRIVATE(instance);
}

void
gsane_option_handler_class_init(gpointer g_class, gpointer class_data)
{
	GSaneOptionHandlerClass *oh_class = GSANE_OPTION_HANDLER_CLASS(g_class);
	g_type_class_add_private(g_class, sizeof(GSaneOptionHandlerPrivate));
	oh_class->unique = FALSE;
	oh_class->handle_option		= gsane_option_handler_default_handle_option;
	oh_class->reload_options	= gsane_option_handler_default_reload_options;
}

GType
gsane_option_handler_get_type()
{
	static GType type = 0;
	static GTypeInfo tinfo = {
		.class_size	= sizeof(GSaneOptionHandlerClass),
		.base_init	= NULL,
		.base_finalize	= NULL,
		.class_init	= gsane_option_handler_class_init,
		.class_finalize	= NULL,
		.class_data	= NULL,
		.instance_size	= sizeof(GSaneOptionHandler),
		.instance_init	= gsane_option_handler_instance_init,
		.value_table	= NULL,
	};
	static GTypeFundamentalInfo finfo = {
		.type_flags	= G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE,
	};

	if (G_UNLIKELY(type == 0)) {
		type = g_type_fundamental_next();
		g_type_register_fundamental(type, "GSaneOptionHandler", &tinfo, &finfo, G_TYPE_FLAG_ABSTRACT);
	}

	return type;
}
