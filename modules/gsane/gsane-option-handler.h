/* GSane - SANE GNOME Scan backend
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifndef _GSANE_OPTION_HANDLER_H_
#define _GSANE_OPTION_HANDLER_H_

#include <glib-object.h>
#include <gnome-scan.h>
#include "gsane-common.h"
#include <sane/sane.h>

G_BEGIN_DECLS

#define	GSANE_TYPE_OPTION_HANDLER		(gsane_option_handler_get_type())
#define GSANE_OPTION_HANDLER(o)			(G_TYPE_CHECK_INSTANCE_CAST((o), GSANE_TYPE_OPTION_HANDLER, GSaneOptionHandler))
#define GSANE_OPTION_HANDLER_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GSANE_TYPE_OPTION_HANDLER, GSaneOptionHandlerClass))
#define	GSANE_OPTION_HANDLER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS((o), GSANE_TYPE_OPTION_HANDLER, GSaneOptionHandlerClass))

typedef struct _GSaneOptionHandlerClass GSaneOptionHandlerClass;
typedef struct _GSaneOptionHandler GSaneOptionHandler;
typedef struct _GSaneOptionHandlerPrivate GSaneOptionHandlerPrivate;

typedef void (*GSaneOptionHandlerFunc)  (GSaneOptionHandler*self, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group);

struct _GSaneOptionHandlerClass
{
	GTypeClass		parent_class;
	GSaneOptionHandlerFunc	handle_option;
	void			(*reload_options) (GSaneOptionHandler*self);
	/* Whether the scanner must have a unique instance of the
	   option handler */
	gboolean		unique;
};

struct _GSaneOptionHandler
{
	GTypeInstance	parent_instance;
	GnomeScanScanner* scanner;

	/*< private >*/
	GSaneOptionHandlerPrivate* priv;
};

/* OPTION UTILS */
GnomeScanOptionHint gsane_option_hint(const SANE_Option_Descriptor*desc);
GnomeScanUnit gsane_option_unit(const SANE_Option_Descriptor* desc);


/* FACILITY FOR NAMED OPTION HANDLING */

/* Define private structs fields to handle a named SANE option  */
#define GSANE_OPTION_HANDLER_DEFINE_OPTION(name)			\
	const SANE_Option_Descriptor* name##_desc;			\
	SANE_Int name##_index;

/* Define class struct field to handle a named SANE option. */
#define	GSANE_OPTION_HANDLER_CLASS_DEFINE_HANDLER(name)			\
	GSaneOptionHandlerFunc handle_##name;				\
	gchar** name##_names;

/* Install a named SANE option handler in the class. Requires
 * gsane_option_handler_handler_<oname> to be declared. See
 * GSANE_OPTION_HANDLER_DEFINE_OPTION and
 * GSANE_OPTION_HANDLER_CLASS_DEFINE_HANDLER.
 *
 * oname is a string containing all other SANE option name that should
 * be matched as oname.
 */
#define GSANE_OPTION_HANDLER_CLASS_INSTALL_OPTION(klass, oname, onames)		\
	klass->handle_##oname = gsane_option_handler_handle_##oname;	\
	klass->oname##_names = g_strsplit(#oname "," onames, ",", 8);

/* If SANE option desc match oname, desc and n are saved in self->priv
   and named option handler is called. */
#define GSANE_OPTION_HANDLER_MATCH_OPTION(self, klass, oname, desc, n, group)		\
	if (gsane_string_in_array(desc->name,				\
				  (const gchar**)klass->oname##_names)) { \
		self->priv->oname##_desc = desc;			\
		self->priv->oname##_index = n;				\
		klass->handle_##oname(GSANE_OPTION_HANDLER(self), desc, n, group); \
	}


/* OPTION HANDLER METHODS */
GSaneOptionHandler* gsane_option_handler_new(GType type, GnomeScanScanner *scanner, SANE_Handle handle);
void gsane_option_handler_destroy(GSaneOptionHandler *self);
void gsane_option_handler_handle_option(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group);
void gsane_option_handler_reload_options(GSaneOptionHandler *self);

/* helper for option initialazation. */
GSList* gsane_option_handler_enum_list_int_values(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n);
GSList* gsane_option_handler_enum_list_double_values(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n);
GSList* gsane_option_handler_enum_list_string_values(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n);
const SANE_Option_Descriptor* gsane_option_handler_get_sane_option_descriptor(GSaneOptionHandler *self, SANE_Int n);

gboolean gsane_option_handler_get_bool(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error);
gboolean gsane_option_handler_set_bool(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gboolean value, GError **error);

gint gsane_option_handler_get_int(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error);
gboolean gsane_option_handler_set_int(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gint value, GError **error);

gdouble gsane_option_handler_get_double(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error);
gboolean gsane_option_handler_set_double(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gdouble value, GError **error);

gchar* gsane_option_handler_get_string(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GError **error);
gboolean gsane_option_handler_set_string(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, gchar* value, GError **error);

GnomeScanEnumValue* gsane_option_handler_get_enum(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GSList* values, GError **error);
gboolean gsane_option_handler_set_enum(GSaneOptionHandler *self, const SANE_Option_Descriptor* desc, SANE_Int index, GnomeScanEnumValue* value, GError **error);

GType gsane_option_handler_get_type(void) G_GNUC_CONST;

G_END_DECLS

#endif
