/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifndef _GSANE_OPTION_HANDLER_GENERIC_H_
#define _GSANE_OPTION_HANDLER_GENERIC_H_

#include "gsane-option-handler.h"

G_BEGIN_DECLS

#define	GSANE_TYPE_OPTION_HANDLER_GENERIC		(gsane_option_handler_generic_get_type())
#define	GSANE_OPTION_HANDLER_GENERIC(o)			(G_TYPE_CHECK_INSTANCE_CAST((o), GSANE_TYPE_OPTION_HANDLER_GENERIC, GSaneOptionHandlerGeneric))
#define GSANE_OPTION_HANDLER_GENERIC_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GSANE_TYPE_OPTION_HANDLER_GENERIC, GSaneOptionHandlerGenericClass))
#define	GSANE_OPTION_HANDLER_GENERIC_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS((o), GSANE_TYPE_OPTION_HANDLER_GENERIC, GSaneOptionHandlerGenericClass))

typedef struct _GSaneOptionHandlerGenericClass GSaneOptionHandlerGenericClass;
typedef struct _GSaneOptionHandlerGeneric GSaneOptionHandlerGeneric;
typedef struct _GSaneOptionHandlerGenericPrivate GSaneOptionHandlerGenericPrivate;

struct _GSaneOptionHandlerGeneric
{
	GSaneOptionHandler parent_instance;
	GSaneOptionHandlerGenericPrivate* priv;
};

struct _GSaneOptionHandlerGenericClass
{
	GSaneOptionHandlerClass parent_class;
	GnomeScanOptionHint option_hint;
};

GType gsane_option_handler_generic_get_type(void) G_GNUC_CONST;

G_END_DECLS

#endif
