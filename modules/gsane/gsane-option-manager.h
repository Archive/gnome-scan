/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifndef _GSANE_OPTION_MANAGER_H_
#define	_GSANE_OPTION_MANAGER_H_

#include <glib-object.h>
#include <sane/sane.h>

G_BEGIN_DECLS

#define GSANE_TYPE_OPTION_MANAGER	(gsane_option_manager_get_type())
#define	GSANE_OPTION_MANAGER(o)		(G_TYPE_CHECK_INSTANCE_CAST((o), GSANE_TYPE_OPTION_MANAGER, GSaneOptionManager))
#define	GSANE_OPTION_MANAGER_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GSANE_TYPE_OPTION_MANAGER, GSaneOptionManagerClass))

typedef struct _GSaneOptionManagerClass GSaneOptionManagerClass;
typedef struct _GSaneOptionManager GSaneOptionManager;
typedef struct _GSaneOptionManagerPrivate GSaneOptionManagerPrivate;

struct _GSaneOptionManagerClass
{
	GTypeClass	parent_class;
};

struct _GSaneOptionManager
{
	GTypeInstance	parent_instance;
	GSaneOptionManagerPrivate*	priv;
};

GSaneOptionManager* gsane_option_manager_new(void);
void gsane_option_manager_destroy(GSaneOptionManager *self);
void gsane_option_manager_add_rule_by_type(GSaneOptionManager* self, SANE_Value_Type value_type, GType handler_type);
void gsane_option_manager_add_rule_by_name(GSaneOptionManager* self, const gchar* name, GType handler_type);
void gsane_option_manager_add_rules_by_name(GSaneOptionManager* self, GType handler_type, ...);
GType gsane_option_manager_get_handler_type(GSaneOptionManager* self, const SANE_Option_Descriptor* desc);
GType gsane_option_manager_get_type(void) G_GNUC_CONST;

G_END_DECLS

#endif
