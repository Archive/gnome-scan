/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include "gsane-option-area.h"

#define GSANE_OPTION_AREA_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE((o), GSANE_TYPE_OPTION_AREA, GSaneOptionAreaPrivate))

struct _GSaneOptionAreaPrivate {
	GSANE_OPTION_HANDLER_DEFINE_OPTION(tl_x);
	GSANE_OPTION_HANDLER_DEFINE_OPTION(tl_y);
	GSANE_OPTION_HANDLER_DEFINE_OPTION(br_x);
	GSANE_OPTION_HANDLER_DEFINE_OPTION(br_y);
	guint count;
	GnomeScanOption* origin;
	GnomeScanOption* paper_size;
	GnomeScanOption* page_orientation;
	GnomeScanUnit unit;
	gdouble	fake_resolution;
};

G_DEFINE_TYPE(GSaneOptionArea, gsane_option_area, GSANE_TYPE_OPTION_HANDLER)

static void
gsane_option_area_install_origin(GSaneOptionArea *self)
{
	GSaneOptionHandler* handler = (GSaneOptionHandler*) self;
	gdouble x, y;

	x = gsane_option_handler_get_double(handler, self->priv->tl_x_desc, self->priv->tl_x_index, NULL);
	y = gsane_option_handler_get_double(handler, self->priv->tl_y_desc, self->priv->tl_y_index, NULL);

	if (self->priv->fake_resolution) {
		x = gnome_scan_convert_to_mm(x, self->priv->unit, self->priv->fake_resolution);
		y = gnome_scan_convert_to_mm(y, self->priv->unit, self->priv->fake_resolution);
	}

	GnomeScanPoint point = {
		.x = x,
		.y = y,
	};

	self->priv->origin = GNOME_SCAN_OPTION(gnome_scan_option_pointer_new("origin", _("Origin"), _("Coordinate of the top left corner of the paper."), GNOME_SCAN_OPTION_GROUP_FORMAT, GETTEXT_PACKAGE,
									     gnome_scan_point_dup(&point), GNOME_SCAN_OPTION_HINT_HIDDEN));
	gnome_scan_node_install_option(GNOME_SCAN_NODE(handler->scanner), self->priv->origin);
}

static void
gsane_option_area_update(GSaneOptionArea* self)
{
	GSaneOptionHandler* handler = GSANE_OPTION_HANDLER(self);
	GtkPaperSize* ps;
	GnomeScanEnumValue evalue = {0};
	gdouble tl_x, tl_y, br_x, br_y;
	GnomeScanPoint *origin;
	GtkPageOrientation orientation;

	gnome_scan_option_enum_get_value(GNOME_SCAN_OPTION_ENUM(self->priv->paper_size), &evalue);

	ps = g_value_get_boxed(&evalue.value);
	gnome_scan_option_enum_get_value(GNOME_SCAN_OPTION_ENUM(self->priv->page_orientation), &evalue);
	orientation = g_value_get_enum(&evalue.value);
	origin = gnome_scan_option_pointer_get_value(GNOME_SCAN_OPTION_POINTER(self->priv->origin));
	
	tl_x = origin->x;
	tl_y = origin->y;
	if (orientation == GTK_PAGE_ORIENTATION_PORTRAIT
	    || g_str_equal(gtk_paper_size_get_name(ps), "max")) {
		br_x = origin->x + gtk_paper_size_get_width(ps, GTK_UNIT_MM);
		br_y = origin->y + gtk_paper_size_get_height(ps, GTK_UNIT_MM);
	}
	else {
		br_x = origin->x + gtk_paper_size_get_height(ps, GTK_UNIT_MM);
		br_y = origin->y + gtk_paper_size_get_width(ps, GTK_UNIT_MM);
	}

	g_debug("Scan (%.2f ; %.2f) (%.2f ; %.2f) mm", tl_x, tl_y, br_x, br_y);
	if (self->priv->fake_resolution) {
		tl_x = gnome_scan_convert_from_mm(tl_x, self->priv->unit, self->priv->fake_resolution);
		tl_y = gnome_scan_convert_from_mm(tl_y, self->priv->unit, self->priv->fake_resolution);
		br_x = gnome_scan_convert_from_mm(br_x, self->priv->unit, self->priv->fake_resolution);
		br_y = gnome_scan_convert_from_mm(br_y, self->priv->unit, self->priv->fake_resolution);
		g_debug("=> actual scan (%.2f ; %.2f) (%.2f ; %.2f) %s",
			tl_x, tl_y, br_x, br_y,
			gnome_scan_unit_to_string(self->priv->unit));
	}

	gsane_option_handler_set_double(handler, self->priv->tl_x_desc, self->priv->tl_x_index, tl_x, NULL);
	gsane_option_handler_set_double(handler, self->priv->tl_y_desc, self->priv->tl_y_index, tl_y, NULL);
	gsane_option_handler_set_double(handler, self->priv->br_x_desc, self->priv->br_x_index, br_x, NULL);
	gsane_option_handler_set_double(handler, self->priv->br_y_desc, self->priv->br_y_index, br_y, NULL);
}

static void
gsane_option_area_option_changed(GSaneOptionArea* self, GParamSpec* pspec, GObject* option)
{
	gsane_option_area_update(self);
}

static GSList*
gsane_option_area_add_paper_size(GSaneOptionArea* self, GSList* values, const gchar* paper_name, GnomeScanExtent *extent)
{
	GtkPaperSize* ps;

	ps = gtk_paper_size_new(paper_name);

	if (gtk_paper_size_get_width(ps, GTK_UNIT_MM) > extent->width
	    || gtk_paper_size_get_height(ps, GTK_UNIT_MM) > extent->height) {
		g_message("Can't scan %s document, too wide for hardware extent.",
			  gtk_paper_size_get_display_name(ps));
		gtk_paper_size_free(ps);
		return values;
	}

	return g_slist_append(values, ps);
}

/* List common paper sizes. This will be extendable through
   Preselection */
static GSList*
gsane_option_area_list_paper_sizes(GSaneOptionArea *self, GnomeScanExtent *extent)
{
	GSList *values = NULL;

	gnome_scan_scanner_get_extent(GSANE_OPTION_HANDLER(self)->scanner, extent);

	/* ISO format */
	values = gsane_option_area_add_paper_size(self, values, GTK_PAPER_NAME_A4, extent);
	values = gsane_option_area_add_paper_size(self, values, GTK_PAPER_NAME_A5, extent);

	/* US bullshit non standard paper sizes */
	values = gsane_option_area_add_paper_size(self, values, GTK_PAPER_NAME_LETTER, extent);
	values = gsane_option_area_add_paper_size(self, values, GTK_PAPER_NAME_LEGAL, extent);
	values = gsane_option_area_add_paper_size(self, values, GTK_PAPER_NAME_EXECUTIVE, extent);

	return values;
}

static void
gsane_option_area_install_paper_size(GSaneOptionArea* self)
{
	GSaneOptionHandler* handler = (GSaneOptionHandler*)self;
	GSList *ps_list = NULL, *it;
	gdouble tlx, tly, brx, bry, width, height;
	GnomeScanExtent extent;

	gnome_scan_scanner_get_extent(GSANE_OPTION_HANDLER(self)->scanner, &extent);

	/* Read hardware saved custom paper size */
	tlx = gsane_option_handler_get_double(handler, self->priv->tl_x_desc, self->priv->tl_x_index, NULL);
	tly = gsane_option_handler_get_double(handler, self->priv->tl_y_desc, self->priv->tl_y_index, NULL);
	brx = gsane_option_handler_get_double(handler, self->priv->br_x_desc, self->priv->br_x_index, NULL);
	bry = gsane_option_handler_get_double(handler, self->priv->br_y_desc, self->priv->br_y_index, NULL);
	width = brx - tlx;
	height = bry - tly;

	/* We don't list paper size when using fake resolution because
	   we can't reliabily check whether fit the scan area. Also,
	   this will lead the ROI not to fit the actual paper on the
	   preview. Last but not least, pixel device are often webcam
	   and such device that really don't care paper sizes.
	*/
	if (self->priv->fake_resolution) {
		width = gnome_scan_convert_to_mm(width, self->priv->unit, self->priv->fake_resolution);	
		height = gnome_scan_convert_to_mm(height, self->priv->unit, self->priv->fake_resolution);
	}
	else
		ps_list = gsane_option_area_list_paper_sizes(self, &extent);

	/* define special paper sizes */

	
	ps_list = g_slist_prepend(ps_list,
				  /* Translator: manual selection display label */
				  gtk_paper_size_new_custom("manual", _("Manual"),
							    width, height,
							    GTK_UNIT_MM));

	ps_list = g_slist_prepend(ps_list,
				  /* Translator: maximum paper size display label */
				  gtk_paper_size_new_custom("max", _("Maximum"),
							    extent.width, extent.height,
							    GTK_UNIT_MM));

	GSList *enum_values = NULL;
	GnomeScanEnumValue evalue;
	GValue *value = g_new0(GValue,1);
	gchar *papersizes = NULL, *old;
	g_value_init(value, GTK_TYPE_PAPER_SIZE);
	for(it = ps_list; it; it = it->next) {
		g_value_set_boxed(value, it->data);
		gnome_scan_enum_value_init(&evalue, value, gtk_paper_size_get_display_name(it->data), NULL);
		enum_values = g_slist_append(enum_values, gnome_scan_enum_value_dup(&evalue));

		/* Assemble debugging list  */
		if (papersizes) {
			old = papersizes;
			papersizes = g_strdup_printf("%s, \"%s\"", papersizes, evalue.label);
			g_free(old);
		}
		else
			papersizes = g_strdup_printf("\"%s\"", evalue.label);

		gnome_scan_enum_value_destroy(&evalue);
	}
	g_value_reset(value);
	g_free(value);


	self->priv->paper_size = GNOME_SCAN_OPTION(gnome_scan_option_paper_size_new("paper-size", _("Paper-Size"), _("Paper-Size"), GNOME_SCAN_OPTION_GROUP_FORMAT, GETTEXT_PACKAGE,
										    &extent, enum_values->data, enum_values, GNOME_SCAN_OPTION_HINT_PRIMARY));
	gnome_scan_node_install_option(GNOME_SCAN_NODE(handler->scanner), self->priv->paper_size);
	g_signal_connect_swapped(self->priv->paper_size, "notify::value", G_CALLBACK(gsane_option_area_option_changed), self);

	g_debug("\toption    : paper-size = \"%s\", enum = {%s}", ((GnomeScanEnumValue*)(enum_values->data))->label, papersizes);
	g_slist_foreach(ps_list, (GFunc) gtk_paper_size_free, NULL);
	g_slist_free(ps_list);
	g_free(papersizes);
}


static GSList*
gsane_option_area_add_orientation(GSaneOptionArea* self, GSList* values, GtkPageOrientation orientation, const gchar* stock_id)
{
	GnomeScanEnumValue evalue;
	GValue* value;

	value = g_new0(GValue, 1);
	g_value_init(value, GTK_TYPE_PAGE_ORIENTATION);
	g_value_set_enum(value, orientation);
	gnome_scan_enum_value_init(&evalue, value, stock_id, NULL);
	values = g_slist_append(values, gnome_scan_enum_value_dup(&evalue));
	gnome_scan_enum_value_destroy(&evalue);
	g_value_reset(value);
	g_free(value);

	return values;
}

static void
gsane_option_area_install_page_orientation(GSaneOptionArea *self)
{
	GSaneOptionHandler* handler = (GSaneOptionHandler*)self;
	GSList* values = NULL;
	GnomeScanEnumValue value0;
	GnomeScanEnumValue* value1;
	GtkPaperSize *ps;
	gdouble width, height;

	gnome_scan_option_enum_get_value(GNOME_SCAN_OPTION_ENUM(self->priv->paper_size), &value0);
	ps = g_value_get_boxed(&(value0.value));
	width = gtk_paper_size_get_width(ps, GTK_UNIT_MM);
	height= gtk_paper_size_get_height(ps, GTK_UNIT_MM);

	values = gsane_option_area_add_orientation(self, values, GTK_PAGE_ORIENTATION_PORTRAIT, GTK_STOCK_ORIENTATION_PORTRAIT);
	values = gsane_option_area_add_orientation(self, values, GTK_PAGE_ORIENTATION_LANDSCAPE, GTK_STOCK_ORIENTATION_LANDSCAPE);
	value1 = width <= height ? values->data : values->next->data;
	self->priv->page_orientation = GNOME_SCAN_OPTION(gnome_scan_option_enum_new("page-orientation", _("Page Orientation"), _("Page orientation"), GNOME_SCAN_OPTION_GROUP_FORMAT, GETTEXT_PACKAGE,
										    value1, values, GNOME_SCAN_OPTION_HINT_PRIMARY));
	gnome_scan_node_install_option(GNOME_SCAN_NODE(handler->scanner), self->priv->page_orientation);
	g_signal_connect_swapped(self->priv->page_orientation, "notify::value", G_CALLBACK(gsane_option_area_option_changed), self);
}

static gboolean
gsane_option_area_get_extent(GSaneOptionArea *self, GnomeScanExtent *extent)
{
	gdouble min, max;

	/* TODO: handle enum ? Didn't found a backend using enum
	   constraint */
	switch (self->priv->tl_x_desc->type) {
	case SANE_TYPE_INT:
		min = self->priv->br_x_desc->constraint.range->min;
		max = self->priv->br_x_desc->constraint.range->max;
		extent->width = max - min;
		min = self->priv->br_y_desc->constraint.range->min;
		max = self->priv->br_y_desc->constraint.range->max;
		extent->height = max - min;
		break;
	case SANE_TYPE_FIXED:
		min = SANE_UNFIX(self->priv->br_x_desc->constraint.range->min);
		max = SANE_UNFIX(self->priv->br_x_desc->constraint.range->max);
		extent->width = max - min;
		min = SANE_UNFIX(self->priv->br_y_desc->constraint.range->min);
		max = SANE_UNFIX(self->priv->br_y_desc->constraint.range->max);
		extent->height = max - min;
		break;
	default:
		g_warning("Unsupported coordinate unit.");
		return FALSE;
		break;
	}

	return TRUE;
}

static void
gsane_option_handler_handle_matched(GSaneOptionHandler *handler, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group)
{
	GSaneOptionArea* self = GSANE_OPTION_AREA (handler);
	GnomeScanExtent extent;

	self->priv->count++;

	if (self->priv->count < 4)
		return;

	/* Here we have matched all SANE option we wanted */

	if (!gsane_option_area_get_extent(self, &extent))
		return;

	self->priv->unit = gsane_option_unit(self->priv->tl_x_desc);
	if (self->priv->unit == GNOME_SCAN_UNIT_PIXEL) {
		/* Pixel defined area prevent us to get the actual
		   scanner extent. We assume the scanner can scan a US
		   Legal-width paper. We compute a fake resolution
		   based on this assumption. */
		self->priv->fake_resolution = extent.width / (216. / 25.4);

		extent.width = gnome_scan_convert_to_mm(extent.width,
							self->priv->unit,
							self->priv->fake_resolution);
		extent.height = gnome_scan_convert_to_mm(extent.height,
							 self->priv->unit,
							 self->priv->fake_resolution);
	}

	gnome_scan_scanner_set_extent(handler->scanner, gnome_scan_extent_dup(&extent));
	g_debug("%s extent is %.2fx%.2f %s",
		gnome_scan_scanner_get_name(handler->scanner),
		extent.width, extent.height, gnome_scan_unit_to_string(self->priv->unit));

	/* Actually install high level options */
	gsane_option_area_install_origin(self);
	gsane_option_area_install_paper_size(self);
	gsane_option_area_install_page_orientation(self);
}

static void
gsane_option_area_handle_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group)
{
	GSaneOptionArea* self = GSANE_OPTION_AREA (handler);
	GSaneOptionAreaClass* klass = GSANE_OPTION_AREA_GET_CLASS(self);

	GSANE_OPTION_HANDLER_MATCH_OPTION(self, klass, tl_x, desc, n, group); 
	GSANE_OPTION_HANDLER_MATCH_OPTION(self, klass, tl_y, desc, n, group); 
	GSANE_OPTION_HANDLER_MATCH_OPTION(self, klass, br_x, desc, n, group); 
	GSANE_OPTION_HANDLER_MATCH_OPTION(self, klass, br_y, desc, n, group);
}

static void
gsane_option_area_reload_options(GSaneOptionHandler *handler)
{
	GSaneOptionArea* self = GSANE_OPTION_AREA (handler);

	self->priv->tl_x_desc = gsane_option_handler_get_sane_option_descriptor(handler, self->priv->tl_x_index);
	self->priv->tl_y_desc = gsane_option_handler_get_sane_option_descriptor(handler, self->priv->tl_y_index);
	self->priv->br_x_desc = gsane_option_handler_get_sane_option_descriptor(handler, self->priv->br_x_index);
	self->priv->br_y_desc = gsane_option_handler_get_sane_option_descriptor(handler, self->priv->br_y_index);
}

static void
gsane_option_area_class_init(GSaneOptionAreaClass *klass)
{
	g_type_class_add_private(klass, sizeof(GSaneOptionAreaPrivate));
	GSaneOptionHandlerClass *oh_class = GSANE_OPTION_HANDLER_CLASS(klass);
	oh_class->unique	= TRUE;
	oh_class->handle_option = gsane_option_area_handle_option;
	oh_class->reload_options= gsane_option_area_reload_options;

#define	gsane_option_handler_handle_tl_x	gsane_option_handler_handle_matched
#define	gsane_option_handler_handle_tl_y	gsane_option_handler_handle_matched
#define	gsane_option_handler_handle_br_x	gsane_option_handler_handle_matched
#define	gsane_option_handler_handle_br_y	gsane_option_handler_handle_matched

	GSANE_OPTION_HANDLER_CLASS_INSTALL_OPTION(klass, tl_x, "tl-x");
	GSANE_OPTION_HANDLER_CLASS_INSTALL_OPTION(klass, tl_y, "tl-y");
	GSANE_OPTION_HANDLER_CLASS_INSTALL_OPTION(klass, br_x, "br-x");
	GSANE_OPTION_HANDLER_CLASS_INSTALL_OPTION(klass, br_y, "br-y");

#undef gsane_option_handler_handle_tl_x
#undef gsane_option_handler_handle_tl_y
#undef gsane_option_handler_handle_br_x
#undef gsane_option_handler_handle_br_y

}

static void
gsane_option_area_init(GSaneOptionArea* self)
{
	self->priv = GSANE_OPTION_AREA_GET_PRIVATE(self);
	self->priv->count = 0;
	self->priv->fake_resolution = 0.;
}
