/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include <config.h>
#include <glib/gi18n-lib.h>
#include "gsane-scanner.h"
#include "gsane-option-source.h"
#include "gsane-common.h"

#define GSANE_OPTION_SOURCE_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE((o), GSANE_TYPE_OPTION_SOURCE, GSaneOptionSourcePrivate))

struct _GSaneOptionSourcePrivate {
	GSANE_OPTION_HANDLER_DEFINE_OPTION(source);
	GnomeScanOption *option;
};

G_DEFINE_TYPE(GSaneOptionSource, gsane_option_source, GSANE_TYPE_OPTION_HANDLER);

/*
 * GSaneOptionSource has many jobs to fill.
 *
 * - It expose source option as a primary option, making some changes
 *   to void backend inconsistency.
 *
 * - It knows whether the ADF has been selected in order to enable
 *   mass acquisition.
 *
 * TODO:
 *
 * - Support ADF boolean option.
 *
 * - Support duplex scan (boolean and special source).
 */

/* some unlisted sources :
 *
 * - Slide(s)
 * - TMA Slides
 * - ADF Back
 * - ADF Duplex (should be mapped to duplex boolean)
 */

/* List of known source used for flatbed  */
#define	GSANE_SOURCE_FLATBED	_("Flatbed")
static const gchar* flatbed_src[] = {
	"FB",
	"Normal",
	"Manual Feed Tray",
	"Opaque/Normal",
	NULL
};

/* List of known source used for Automatic Document Feeder */
#define	GSANE_SOURCE_ADF	_("Automatic Document Feeder")
static const gchar* adf_src[] = {
	"ADF",
	"ADF Front",
	"Automatic Slide Feeder",
	"Document Feeder",
	"AutoFeeder",
	NULL
};

/* List of known source used for Transparency Adapter */
#define	GSANE_SOURCE_TMA	_("Transparency Adapter")
static const gchar* tma_src[] = {
	"Transparency",
	"Transparency Adapter",
	"Transparency Unit",
	"TMA",			/* Transparent Media Adapter */
	NULL
};

/* List of known source used for Negative Adapter */
#define GSANE_SOURCE_NEGATIVE	_("Negative Adapter")
static const gchar* negative_src[] = {
	"Negative",
	"Negative Unit",
	"TMA Negative",
	"Filmstrip",
	"Film",
	"XPA",
	NULL
};

static void
gsane_option_source_option_changed(GSaneOptionSource* self, GParamSpec *pspec, GObject* option)
{
	GnomeScanEnumValue evalue;
	gnome_scan_option_enum_get_value(GNOME_SCAN_OPTION_ENUM(option), &evalue);
	gsane_scanner_set_mass_acquisition(GSANE_SCANNER(GSANE_OPTION_HANDLER(self)->scanner),
					   g_str_equal(evalue.label, GSANE_SOURCE_ADF));
	gsane_option_handler_set_enum(GSANE_OPTION_HANDLER(self), self->priv->source_desc, self->priv->source_index, &evalue, NULL);
}

static void
gsane_option_handler_handle_source(GSaneOptionHandler* handler, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group)
{
	GSaneOptionSource* self = GSANE_OPTION_SOURCE(handler);
	GSList *iter, *values = gsane_option_handler_enum_list_string_values(handler, desc, n);
	GnomeScanEnumValue *value, *default_value;
	const gchar* src;
	gchar *sources, *old;

	/* override label to make source consistent accross backends,
	   this also allow to know whether ADF is selected. */
	sources = NULL;
	for(iter = values; iter; iter = iter->next) {
		value = iter->data;
		src = g_value_get_string(&value->value);
		if (gsane_string_in_array(src, flatbed_src))
			value->label = GSANE_SOURCE_FLATBED;
		else if (gsane_string_in_array(src, adf_src))
			value->label = GSANE_SOURCE_ADF;
		else if (gsane_string_in_array(src, tma_src))
			value->label = GSANE_SOURCE_TMA;
		else if (gsane_string_in_array(src, negative_src))
			value->label = GSANE_SOURCE_NEGATIVE;

		if (sources) {
			old = sources;
			sources = g_strdup_printf("%s, \"%s\"", sources, value->label);
			g_free(old);
		}
		else
			sources = g_strdup_printf("\"%s\"", value->label);

	}

	default_value = gsane_option_handler_get_enum(handler, desc, n, values, NULL);
	self->priv->option = GNOME_SCAN_OPTION(gnome_scan_option_enum_new(desc->name, _("Source"), S_(desc->desc), GNOME_SCAN_OPTION_GROUP_SCANNER, SANE_GETTEXT_PACKAGE,
									  default_value, values, GNOME_SCAN_OPTION_HINT_PRIMARY));
	g_signal_connect_swapped(self->priv->option, "notify::value", G_CALLBACK(gsane_option_source_option_changed), self);
	gnome_scan_node_install_option(GNOME_SCAN_NODE(handler->scanner), self->priv->option);

	g_debug("\toption %02d : source = \"%s\", enum = {%s}", n, default_value->label, sources);
	g_free(sources);
}

static void
gsane_option_source_handle_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group)
{
	GSaneOptionSource *self = GSANE_OPTION_SOURCE(handler);
	GSaneOptionSourceClass *klass = GSANE_OPTION_SOURCE_GET_CLASS(self);

	GSANE_OPTION_HANDLER_MATCH_OPTION(self, klass, source, desc, n, group);
}

static void
gsane_option_source_reload_option(GSaneOptionHandler *handler)
{
	GSaneOptionSource *self = GSANE_OPTION_SOURCE(handler);

	self->priv->source_desc = gsane_option_handler_get_sane_option_descriptor(handler, self->priv->source_index);
}

static void
gsane_option_source_class_init(GSaneOptionSourceClass *klass)
{
	GSaneOptionHandlerClass *oh_class = GSANE_OPTION_HANDLER_CLASS(klass);

	g_type_class_add_private(klass, sizeof(GSaneOptionSourcePrivate));

	oh_class->unique = TRUE;
	oh_class->handle_option = gsane_option_source_handle_option;
	oh_class->reload_options = gsane_option_source_reload_option;
	GSANE_OPTION_HANDLER_CLASS_INSTALL_OPTION(klass, source, "doc-source");
}

static void
gsane_option_source_init(GSaneOptionSource *self)
{
	self->priv = GSANE_OPTION_SOURCE_GET_PRIVATE(self);
}
