/* Gnome Scan - Scan as easy as you print
 * Copyright © 2007  Étienne Bersac <bersace@gnome.org>
 *
 * Gnome Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */


#ifndef _GSANE_SCANNER_H_
#define _GSANE_SCANNER_H_

#include <glib-object.h>
#include <sane/sane.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define GSANE_TYPE_SCANNER             (gsane_scanner_get_type ())
#define GSANE_SCANNER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSANE_TYPE_SCANNER, GSaneScanner))
#define GSANE_SCANNER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GSANE_TYPE_SCANNER, GSaneScannerClass))
#define GNOME_IS_SCANNER_SANE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSANE_TYPE_SCANNER))
#define GNOME_IS_SCANNER_SANE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GSANE_TYPE_SCANNER))
#define GSANE_SCANNER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GSANE_TYPE_SCANNER, GSaneScannerClass))

typedef struct _GSaneScanner GSaneScanner;
typedef struct _GSaneScannerClass GSaneScannerClass;
typedef struct _GSaneScannerPrivate GSaneScannerPrivate;

struct _GSaneScannerClass
{
	GnomeScanScannerClass	parent_class;
};

struct _GSaneScanner
{
	GnomeScanScanner	parent_instance;
	
	/*< private >*/
	GSaneScannerPrivate*	priv;
};

GType gsane_scanner_get_type (void) G_GNUC_CONST;
void gsane_scanner_register (GTypeModule *module);
GnomeScanScanner* gsane_scanner_new (const SANE_Device *device);
void gsane_scanner_reload_parameters(GSaneScanner* self);
void gsane_scanner_set_mass_acquisition(GSaneScanner* self, gboolean mass_acquisition);

G_END_DECLS

#endif /* _GSANE_SCANNER_H_ */
