/* GSane - SANE GNOME Scan backend
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include <gnome-scan.h>

#include "gsane-common.h"
#include "gsane-option-handler-generic.h"

/* list all options to group in GROUP_SCANNER  */
static const gchar* group_scanner_options[] = {
	"resolution",
	"mode",
	"source",
	NULL
};

#define	GSANE_OPTION_HANDLER_GENERIC_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE((o), GSANE_TYPE_OPTION_HANDLER_GENERIC, GSaneOptionHandlerGenericPrivate))

struct _GSaneOptionHandlerGenericPrivate
{
	SANE_Int index;
	const SANE_Option_Descriptor* desc;
	GnomeScanOption* option;
	void (*get_value) (GSaneOptionHandlerGeneric *self);
};

static GSaneOptionHandlerClass*gsane_option_handler_generic_parent_class = NULL;


static void
gsane_option_handler_generic_bool_get_value(GSaneOptionHandlerGeneric *self)
{
	gboolean value = gsane_option_handler_get_bool(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, NULL);
	gnome_scan_option_bool_set_value(GNOME_SCAN_OPTION_BOOL(self->priv->option), value);
}

static void
gsane_option_handler_generic_number_get_value(GSaneOptionHandlerGeneric *self)
{
	gint value = gsane_option_handler_get_double(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, NULL);
	gnome_scan_option_number_set_value(GNOME_SCAN_OPTION_NUMBER(self->priv->option), value);
}

static void
gsane_option_handler_generic_string_get_value(GSaneOptionHandlerGeneric *self)
{
	gchar* value = gsane_option_handler_get_string(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, NULL);
	gnome_scan_option_string_set_value(GNOME_SCAN_OPTION_STRING(self->priv->option), value);
}

static void
gsane_option_handler_generic_enum_get_value(GSaneOptionHandlerGeneric *self)
{
	GnomeScanEnumValue* value = gsane_option_handler_get_enum(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index,
								  gnome_scan_option_enum_get_values(GNOME_SCAN_OPTION_ENUM(self->priv->option)),
								  NULL);
	gnome_scan_option_enum_set_value(GNOME_SCAN_OPTION_ENUM(self->priv->option), value);
}



static void
gsane_option_handler_generic_bool_option_value_changed(GSaneOptionHandlerGeneric* self, GParamSpec* pspec, GObject* option)
{
	gboolean value;
	g_object_get(option, "value", &value, NULL);
	gsane_option_handler_set_bool(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, value, NULL);
}

static void
gsane_option_handler_generic_number_option_value_changed(GSaneOptionHandlerGeneric* self, GParamSpec* pspec, GObject* option)
{
	gdouble value;
	g_object_get(option, "value", &value, NULL);
	if (gsane_option_handler_set_double(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, value, NULL))
		gnome_scan_option_number_set_value(GNOME_SCAN_OPTION_NUMBER(option),
						   gsane_option_handler_get_double(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, NULL));
}

static void
gsane_option_handler_generic_string_option_value_changed(GSaneOptionHandlerGeneric* self, GParamSpec* pspec, GObject* option)
{
	gchar* value;
	g_object_get(option, "value", &value, NULL);
	gsane_option_handler_set_string(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, value, NULL);
	g_free(value);
}

static void
gsane_option_handler_generic_enum_option_changed(GSaneOptionHandlerGeneric* self, GParamSpec* pspec, GObject* option)
{
	GnomeScanEnumValue evalue;
	gnome_scan_option_enum_get_value(GNOME_SCAN_OPTION_ENUM(option), &evalue);
	gsane_option_handler_set_enum(GSANE_OPTION_HANDLER(self), self->priv->desc, self->priv->index, &evalue, NULL);
}



static inline const gchar*
gsane_option_handler_generic_get_group(GSaneOptionHandlerGeneric* self, const SANE_Option_Descriptor* desc, SANE_Int n, const gchar*group)
{
	return gsane_string_in_array(desc->name, group_scanner_options) ? GNOME_SCAN_OPTION_GROUP_SCANNER : group;
}


static void
gsane_option_handler_generic_handle_bool_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group)
{
	GSaneOptionHandlerGeneric* self = GSANE_OPTION_HANDLER_GENERIC(handler);
	gboolean value = gsane_option_handler_get_bool(handler, desc, n, NULL);
	self->priv->option = GNOME_SCAN_OPTION(gnome_scan_option_bool_new(desc->name, S_(desc->title), S_(desc->desc),
									  gsane_option_handler_generic_get_group(self, desc, n, group),
									  SANE_GETTEXT_PACKAGE, value,
									  GSANE_OPTION_HANDLER_GENERIC_GET_CLASS(handler)->option_hint));
	self->priv->get_value = gsane_option_handler_generic_bool_get_value;
	g_signal_connect_swapped(self->priv->option, "notify::value", G_CALLBACK(gsane_option_handler_generic_bool_option_value_changed), self);
	g_debug("\toption %02d : boolean %s = %s", n, desc->name, GSANE_BOOLEAN_TO_STRING(value));
}

static gchar*
gsane_option_handler_generic_int_get_range(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, gdouble value, GnomeScanRange *range)
{
	gchar *smin, *smax, *debug;

	if (desc->constraint_type == SANE_CONSTRAINT_RANGE) {
		range->min = desc->constraint.range->min;
		smin = g_strdup_printf("%.0f", range->min);
		range->step = MAX(1, desc->constraint.range->quant);
		range->max = desc->constraint.range->max;
		smax = g_strdup_printf("%.0f", range->max);
	}
	else {
		range->min = G_MININT;
		smin = g_strdup("G_MININT");
		range->step = 1.;
		range->max = G_MAXINT;
		smax = g_strdup("G_MAXINT");
	}

	debug = g_strdup_printf("\toption %02d : int %s = %.0f, range = [%s;%.0f;%s]", n, desc->name, value, smin, range->step, smax);
	g_free(smin);
	g_free(smax);
	return debug;
}


static gchar*
gsane_option_handler_generic_double_get_range(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, gdouble value, GnomeScanRange *range)
{
	gchar *smin, *smax, *debug;

	if (desc->constraint_type == SANE_CONSTRAINT_RANGE) {
		range->min = SANE_UNFIX(desc->constraint.range->min);
		smin = g_strdup_printf("%.2f", range->min);
		range->step = MAX(1., SANE_UNFIX(desc->constraint.range->quant));
		range->max = SANE_UNFIX(desc->constraint.range->max);
		smax = g_strdup_printf("%.2f", range->max);
	}
	else {
		range->min = G_MINDOUBLE;
		smin = g_strdup("G_MINDOUBLE");
		range->step = .1;
		range->max = G_MAXDOUBLE;
		smax = g_strdup("G_MAXDOUBLE");
	}

	debug = g_strdup_printf("\toption %02d : int %s = %.2f, range = [%s;%.2f;%s]", n, desc->name, value, smin, range->step, smax);
	g_free(smin);
	g_free(smax);
	return debug;
}

static void
gsane_option_handler_generic_handle_number_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group)
{
	GSaneOptionHandlerGeneric* self = GSANE_OPTION_HANDLER_GENERIC(handler);
	GnomeScanRange range = {0};
	gdouble value;
	gchar* debug;

	value = gsane_option_handler_get_double(handler, desc, n, NULL);
	if (desc->type == SANE_TYPE_INT)
		debug = gsane_option_handler_generic_int_get_range(handler, desc, n, value, &range);
	else
		debug = gsane_option_handler_generic_double_get_range(handler, desc, n, value, &range);

	self->priv->option = GNOME_SCAN_OPTION(gnome_scan_option_number_new(desc->name, S_(desc->title), S_(desc->desc),
									    gsane_option_handler_generic_get_group(self, desc, n, group),
									    SANE_GETTEXT_PACKAGE,
									    value, gnome_scan_range_dup(&range),
									    gsane_option_unit(desc),
									    GSANE_OPTION_HANDLER_GENERIC_GET_CLASS(handler)->option_hint));
	self->priv->get_value = gsane_option_handler_generic_number_get_value;
	g_signal_connect_swapped(self->priv->option, "notify::value", G_CALLBACK(gsane_option_handler_generic_number_option_value_changed), self);
	g_debug("%s", debug);
	g_free(debug);
}

static void
gsane_option_handler_generic_handle_string_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group)
{
	GSaneOptionHandlerGeneric* self = GSANE_OPTION_HANDLER_GENERIC(handler);
	gchar* value = gsane_option_handler_get_string(handler, desc, n, NULL);
	self->priv->option = GNOME_SCAN_OPTION(gnome_scan_option_string_new(desc->name, S_(desc->title), S_(desc->desc),
									    gsane_option_handler_generic_get_group(self, desc, n, group),
									    SANE_GETTEXT_PACKAGE,
									    value,
									    GSANE_OPTION_HANDLER_GENERIC_GET_CLASS(handler)->option_hint));
	self->priv->get_value = gsane_option_handler_generic_string_get_value;
	g_signal_connect_swapped(self->priv->option, "notify::value", G_CALLBACK(gsane_option_handler_generic_string_option_value_changed), self);
	g_debug("\toption %02d : string %s = \"%s\"", n, desc->name, value);
	g_free(value);
}

static void
gsane_option_handler_generic_handle_non_list_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group)
{
	gint array_length;

	/* instanciate option with default value depending on SANE_Value_Type */
	switch(desc->type) {
	case SANE_TYPE_BOOL:
		gsane_option_handler_generic_handle_bool_option(handler, desc, n, group);
		break;
	case SANE_TYPE_INT:
	case SANE_TYPE_FIXED:
		array_length = desc->size / sizeof(SANE_Word);
		if (array_length > 1)
			g_debug("\toption %02d : Ignoring number array option %s", n, desc->name);
		else
			gsane_option_handler_generic_handle_number_option(handler, desc, n, group);
		break;
	case SANE_TYPE_STRING:
		gsane_option_handler_generic_handle_string_option(handler, desc, n, group);
		break;
	default:
		g_debug("\toption %02d : <unhandled> %s", n, desc->name);
		break;
	}
}


static void
gsane_option_handler_generic_handle_list_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group)
{
	GSaneOptionHandlerGeneric* self = GSANE_OPTION_HANDLER_GENERIC(handler);
	GSList *values = NULL, *iter;
	GnomeScanEnumValue *default_value, *evalue;
	gint array_length;
	gchar *enums = NULL, *old, *enum_content;

	switch(desc->type) {
	case SANE_TYPE_INT:
		array_length = desc->size/sizeof(SANE_Int);
		if (array_length > 1)
			g_debug("\toption %02d : Ignoring int enum array option %s", n, desc->name);
		else
			values = gsane_option_handler_enum_list_int_values(handler, desc, n);
		break;
	case SANE_TYPE_FIXED:
		array_length = desc->size/sizeof(SANE_Fixed);
		if (array_length > 1)
			g_debug("\toption %02d : Ignoring double enum array option %s", n, desc->name);
		else
			values = gsane_option_handler_enum_list_double_values(handler, desc, n);
		break;
	case SANE_TYPE_STRING:
		values = gsane_option_handler_enum_list_string_values(handler, desc, n);
		break;
	default:
		g_debug("\toption %02d : <unhandled> %s", n, desc->name);
		break;
	}

	if (!values)
		return;

	for (iter = values ; iter; iter = iter->next) {
		evalue = iter->data;
		enum_content = g_strdup_value_contents(&evalue->value);
		if (enums) {
			old = enums;
			enums = g_strdup_printf("%s, %s", enums, enum_content);
			g_free(enum_content);
			g_free(old);
		}
		else
			enums = enum_content;
	}


	default_value = gsane_option_handler_get_enum(handler, desc, n, values, NULL);
	self->priv->option = GNOME_SCAN_OPTION(gnome_scan_option_enum_new(desc->name, S_(desc->title), S_(desc->desc),
									  gsane_option_handler_generic_get_group(self, desc, n, group),
									  SANE_GETTEXT_PACKAGE,
									  default_value, values,
									  GSANE_OPTION_HANDLER_GENERIC_GET_CLASS(self)->option_hint));
	self->priv->get_value = gsane_option_handler_generic_enum_get_value;
	g_signal_connect_swapped(self->priv->option, "notify::value", G_CALLBACK(gsane_option_handler_generic_enum_option_changed), self);
	g_debug("\toption %02d : %s = %s ; enum = {%s}", n, desc->name, g_strdup_value_contents(&default_value->value), enums);
	g_free(enums);
}

static void
gsane_option_handler_generic_handle_option(GSaneOptionHandler *handler, const SANE_Option_Descriptor*desc, SANE_Int n, const gchar* group)
{
	GSaneOptionHandlerGeneric* self = GSANE_OPTION_HANDLER_GENERIC(handler);
	self->priv->index = n;
	self->priv->desc = desc;

	switch(desc->constraint_type) {
	case SANE_CONSTRAINT_NONE:
	case SANE_CONSTRAINT_RANGE:
		gsane_option_handler_generic_handle_non_list_option(handler, desc, n, group);
		break;
	case SANE_CONSTRAINT_WORD_LIST:
	case SANE_CONSTRAINT_STRING_LIST:
		gsane_option_handler_generic_handle_list_option(handler, desc, n, group);
		break;
	}

	if (!self->priv->option)
		return;

	/* common */
	g_object_set(self->priv->option, "active", SANE_OPTION_IS_ACTIVE(desc->cap), NULL);

	/* install option */
	gnome_scan_node_install_option(GNOME_SCAN_NODE(handler->scanner), self->priv->option);
}

static void
gsane_option_handler_generic_reload_options(GSaneOptionHandler *handler)
{
	GSaneOptionHandlerGeneric*self = GSANE_OPTION_HANDLER_GENERIC(handler);
	if (!self->priv->option)
		return;

	self->priv->desc = gsane_option_handler_get_sane_option_descriptor(handler, self->priv->index);

	gboolean was_active = gnome_scan_option_get_active(self->priv->option);
	gboolean is_active = SANE_OPTION_IS_ACTIVE(self->priv->desc->cap);
	if (!was_active && is_active) {
		g_debug("Activate option %s(%d)", self->priv->desc->name, self->priv->index);
		self->priv->get_value(self);
	}

	if (was_active != is_active)
		gnome_scan_option_set_active(self->priv->option, is_active);
}

static void
gsane_option_handler_generic_class_init(gpointer g_class, gpointer class_data)
{
	gsane_option_handler_generic_parent_class = g_type_class_peek_parent(g_class);
	g_type_class_add_private(g_class, sizeof(GSaneOptionHandlerGenericPrivate));
	GSaneOptionHandlerGenericClass *klass = g_class;
	klass->option_hint		= GNOME_SCAN_OPTION_HINT_SECONDARY;
	GSaneOptionHandlerClass* oh_class = g_class;
	oh_class->handle_option 	= gsane_option_handler_generic_handle_option;
	oh_class->reload_options	= gsane_option_handler_generic_reload_options;
}

void
gsane_option_handler_generic_instance_init(GTypeInstance* instance, gpointer g_class)
{
	GSaneOptionHandlerGeneric *self = GSANE_OPTION_HANDLER_GENERIC(instance);
	self->priv = GSANE_OPTION_HANDLER_GENERIC_GET_PRIVATE(self);
}

GType
gsane_option_handler_generic_get_type(void)
{
	static GType type = 0;
	static const GTypeInfo info = {
		.class_size	= sizeof(GSaneOptionHandlerGenericClass),
		.base_init	= NULL,
		.base_finalize	= NULL,
		.class_init	= gsane_option_handler_generic_class_init,
		.class_finalize	= NULL,
		.class_data	= NULL,
		.instance_size	= sizeof(GSaneOptionHandlerGeneric),
		.instance_init	= gsane_option_handler_generic_instance_init,
		.value_table	= NULL,
	};

	if (G_UNLIKELY(type == 0)) {
		type = g_type_register_static(GSANE_TYPE_OPTION_HANDLER,
					      "GSaneOptionHandlerGeneric",
					      &info, 0);
	}

	return type;
}
