/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifndef _GSANE_PROCESSOR_H_
#define	_GSANE_PROCESSOR_H_

#include <gegl.h>
#include <sane/sane.h>

G_BEGIN_DECLS

typedef struct _GSaneProcessor GSaneProcessor;
typedef struct _GSaneProcessorPrivate GSaneProcessorPrivate;

struct _GSaneProcessor {
	GSaneProcessorPrivate *priv;
};

void gsane_processor_init(GSaneProcessor *self);
GeglBuffer* gsane_processor_prepare_image(GSaneProcessor *self, SANE_Parameters *params, guint frame_count);
void gsane_processor_prepare_frame(GSaneProcessor *self, SANE_Parameters *params);
void gsane_processor_process(GSaneProcessor *self, guchar* buf, guint buf_len);
void gsane_processor_destroy(GSaneProcessor*self);


G_END_DECLS

#endif
