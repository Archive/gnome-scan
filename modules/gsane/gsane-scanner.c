/* GSane - SANE GNOME Scan backend 
 * Copyright © 2007-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GSane is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 * 
 * GSane is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GSane.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#include <config.h>
#include <string.h>
#include <sane/sane.h>
#include "gsane-common.h"
#include "gsane-option-manager.h"
#include "gsane-option-handler.h"
#include "gsane-scanner.h"
#include "gsane-processor.h"

#define GSANE_SCANNER_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GSANE_TYPE_SCANNER, GSaneScannerPrivate))

struct _GSaneScannerPrivate
{
	/* properties */
	gchar* sane_id;
	gchar* sane_type;
	gboolean mass_acquisition;

	/* fields */
	GHashTable* option_handlers;

	GSaneProcessor processor;

	/* SANE handle */
	SANE_Handle handle;
	/* the number of pages scanned from the source (should not be > 1 for non ADF source) */
	guint image_count;
	/* SANE frame paramters */
	SANE_Parameters params;
	guint chunk_len;
	guint bytes_per_pixel;
	guint frame_count;
	/* total bytes read*/
	volatile guint bytes_read;
	/* total bytes to acquire to get the entire image (all frames) */
	guint total_bytes_count;

	/* Threads */
	GThread* opt_thread;
	GThread* params_thread;
};

enum {
	GSANE_SCANNER_DUMMY_PROPERTY,
	GSANE_SCANNER_SANE_ID,
	GSANE_SCANNER_SANE_TYPE,
};

G_DEFINE_DYNAMIC_TYPE (GSaneScanner, gsane_scanner, GNOME_SCAN_TYPE_SCANNER);

/* List of device type used for one icon name. */

/* default  */
#define	GSANE_SCANNER_ICON_NAME	"scanner"
static const gchar* scanner_types[] = {
	"scanner",
	"flatbed scanner",
	"frame grabber",
	"handheld scanner",
	"sheetfed scanner",
	"virtual device",
	NULL
};

#define	GSANE_PRINTER_ICON_NAME	"printer"
static const gchar* printer_types[] = {
	"multi-function peripheral",
	"all-in-one",
	NULL
};

#define	GSANE_WEBCAM_ICON_NAME	"camera-web"
static const gchar* webcam_types[] = {
	"v4l",			/* that's actually a backend name */
	NULL
};

#define	GSANE_CAMERA_ICON_NAME	"camera-photo"
static const gchar* camera_types[] = {
	"still camera",
	NULL
};

#define	GSANE_VIDEO_ICON_NAME	"camera-video"
static const gchar* video_types[] = {
	"video camera",
	NULL
};

static const gchar*
gsane_scanner_icon_name(const SANE_Device *device)
{
	gchar **parts = g_strsplit(device->name, ":", 2);
	const gchar *backend_name = parts[0];
	const gchar* icon_name;

	icon_name = GSANE_SCANNER_ICON_NAME;

	if (gsane_string_in_array(device->type, printer_types))
		icon_name = GSANE_PRINTER_ICON_NAME;
	else if (gsane_string_in_array(backend_name, webcam_types))
		icon_name = GSANE_WEBCAM_ICON_NAME;
	else if (gsane_string_in_array(device->type, camera_types))
		icon_name = GSANE_CAMERA_ICON_NAME;
	else if (gsane_string_in_array(device->type, video_types))
		icon_name = GSANE_VIDEO_ICON_NAME;
	else if (gsane_string_in_array(device->type, scanner_types))
		icon_name = GSANE_SCANNER_ICON_NAME;
	else
		/* Just output unknown to maintain type list. */
		g_debug("%s %s (%s) device type is %s",
			device->vendor, device->model, device->name,
			device->type);

	g_strfreev(parts);

	return icon_name;
}

GnomeScanScanner*
gsane_scanner_new(const SANE_Device *device)
{
	gchar* name;
	if (g_str_equal(device->vendor, "Noname"))
		name = g_strdup(device->model);
	else
		name = g_strconcat(device->vendor, " ", device->model, NULL);

	const gchar* icon_name = gsane_scanner_icon_name(device);
	GObject* object= g_object_new(GSANE_TYPE_SCANNER,
				      "name", name,
				      "icon-name", icon_name,
				      "sane-id", device->name,
				      "sane-type", device->type,
				      NULL);
	g_free(name);

	return GNOME_SCAN_SCANNER(object);
}


void
gsane_scanner_set_mass_acquisition(GSaneScanner* self, gboolean mass_acquisition)
{
	self->priv->mass_acquisition = mass_acquisition;
}

/* Returns whether the status is good. Update node status if
   failing. */
static gboolean
gsane_scanner_check_sane_status(GSaneScanner *self, const gchar* operation, SANE_Status status)
{
	if (status != SANE_STATUS_GOOD) {
		const SANE_String_Const message = sane_strstatus(status);
		g_warning("SANE operation : %s failed with %s", operation, message);
		gnome_scan_node_update_status(GNOME_SCAN_NODE(self), gsane_status_to_error_code(status), message);
	}
	return status == SANE_STATUS_GOOD;
}

/* Reload parameters. This is useful if we want to predetermine image
   size and such. However, it is not supposed to be accurate before
   sane_start() (see SANE standard). */
static void
gsane_scanner_reload_parameters_thread(GSaneScanner* self)
{
	SANE_Status status;
	SANE_Parameters params;

	status = sane_get_parameters(self->priv->handle, &params);
	if (gsane_scanner_check_sane_status(self, "sane_get_parameters", status))
		gnome_scan_node_update_status(GNOME_SCAN_NODE(self), GNOME_SCAN_STATUS_READY, NULL);

	self->priv->params_thread = NULL;
}

void
gsane_scanner_reload_parameters(GSaneScanner* self)
{
	if (self->priv->params_thread)
		return;

	self->priv->params_thread = g_thread_create((GThreadFunc)gsane_scanner_reload_parameters_thread, self, FALSE, NULL);
}

/* Take care of one option, instanciating the right handler and
   passing option to it. */
static void
gsane_scanner_handle_sane_option(GSaneScanner*self, SANE_Int n, const SANE_Option_Descriptor* desc, const gchar* group)
{
	/* retrieve handler type */
	GSaneOptionManager* mngr = gsane_option_manager_new();
	GType handler_type = gsane_option_manager_get_handler_type(mngr, desc);
	if (handler_type == G_TYPE_INVALID) {
		g_warning("No handler for %s", desc->name);
		return;
	}

	GSaneOptionHandlerClass* handler_class = g_type_class_ref(handler_type);
	GSaneOptionHandler* handler = NULL;
	const gchar* key;

	if (handler_class->unique) {
		key = g_type_name(handler_type);
		handler = g_hash_table_lookup(self->priv->option_handlers, key);
	}
	else
		key = desc->name;

	if (handler == NULL) {
		handler = gsane_option_handler_new(handler_type, GNOME_SCAN_SCANNER(self), self->priv->handle);
		g_hash_table_insert(self->priv->option_handlers, g_strdup(key), handler);
	}

	/* pass SANE option to handler */
	gsane_option_handler_handle_option(handler, desc, n, group);

	g_type_class_unref(handler_class);
}

/* Loops all SANE option and extract groups, sensors, buttons and
   regular option. Let OptionHandler manager *all* options. */
static void
gsane_scanner_probe_options(GSaneScanner *self)
{
	g_return_if_fail(self != NULL);

	SANE_Status status;
	const SANE_Option_Descriptor* desc;
	SANE_Int count;
	const gchar* group = NULL;
	gint n;

	status = sane_open(self->priv->sane_id, &self->priv->handle);
	gchar *operation = g_strdup_printf("sane_open(%s)", self->priv->sane_id);
	if (!gsane_scanner_check_sane_status(self, operation, status)) {
		g_free(operation);
		goto end;
	}

	sane_control_option(self->priv->handle, 0, SANE_ACTION_GET_VALUE, &count, NULL);
	g_debug("Device %s (%s) : %d options", gnome_scan_scanner_get_name(self), self->priv->sane_id, count);

	/* loop all SANE options */
	for (n = 1; n < count; n++) {
		desc = sane_get_option_descriptor(self->priv->handle, n);
		switch(desc->type) {
		case SANE_TYPE_GROUP:
			g_debug("Group \"%s\"", dgettext("sane-backends", desc->title));
			group = desc->title;
			break;
		case SANE_TYPE_BUTTON:
			g_debug("\toption %02d : Ignoring button %s.", n, desc->name);
			break;
		default:
			if (SANE_OPTION_IS_SETTABLE(desc->cap))
				gsane_scanner_handle_sane_option(self, n, desc, group);
			else
				g_debug("\toption %02d : Ignoring sensor %s", n, desc->name);
			break;
		}
	}

	/* initialisation is done */
	gnome_scan_node_update_status(GNOME_SCAN_NODE(self), GNOME_SCAN_STATUS_UNCONFIGURED, NULL);
	/* this is where we know if we're actually ready. (well
	   actually not until sane_start()…) */
	gsane_scanner_reload_parameters(self);

 end:
	self->priv->opt_thread = NULL;
}

/* Start counting images acquired during an atomic scan for mass
   acquisition. */
static void
gsane_scanner_start_scan(GnomeScanNode *node)
{
	GSaneScanner *self = GSANE_SCANNER(node);
	self->priv->image_count = 0;
}

/* Ask SANE to start a frame, update parameters according. We may need
   to call sane_cancel() before each sane_start() in order to avoid
   nasty SANE_STATUS_BUSY.  */
static gboolean
gsane_scanner_start_frame(GSaneScanner *self)
{
	SANE_Status status;

	status = sane_start(self->priv->handle);
	if (!gsane_scanner_check_sane_status(self, "sane_start", status))
		return FALSE;

	status = sane_get_parameters(self->priv->handle, &self->priv->params);
	if (!gsane_scanner_check_sane_status(self, "sane_get_parameters", status))
		return FALSE;

	/* Scan line by line. We may compute larger chunk considering
	   height. */
	self->priv->chunk_len = self->priv->params.bytes_per_line;
	gsane_processor_prepare_frame(&self->priv->processor, &self->priv->params);

	return TRUE;
}

/* returns the amount of frame to acquire to get all the image
   data. Actually SANE tells use whether a frame is the last or
   not. However we need frame count to handle proper sample handling,
   progress monitoring, etc. */
static guint
gsane_scanner_get_frame_count(GSaneScanner *self)
{
	switch(self->priv->params.format) {
	case SANE_FRAME_RGB:
	case SANE_FRAME_GRAY:
		return 1;
	case SANE_FRAME_RED:
	case SANE_FRAME_GREEN:
	case SANE_FRAME_BLUE:
		return 3;
	default:
		return 0;
	}
}

/* Starts the acquisition of one image of a scan. */
static gboolean
gsane_scanner_start_image(GnomeScanNode *node)
{
	GSaneScanner *self = GSANE_SCANNER(node);
	GeglBuffer *buffer;

	/* No mass acquisition from flatbed. We might use timeout scan, at this level? */
	if (self->priv->image_count > 0 && !self->priv->mass_acquisition)
		return FALSE;

	/* start the frame */
	if (!gsane_scanner_start_frame(self))
		return FALSE;

	self->priv->frame_count = gsane_scanner_get_frame_count(self);
	self->priv->bytes_read = 0;
	self->priv->bytes_per_pixel = self->priv->params.bytes_per_line/self->priv->params.pixels_per_line;
	self->priv->total_bytes_count = self->priv->params.pixels_per_line * self->priv->params.lines * self->priv->frame_count;

	/* get the image buffer from the processor and let processor
	   initialize data processing */
	buffer = gsane_processor_prepare_image(&self->priv->processor, &self->priv->params, self->priv->frame_count);
	gnome_scan_scanner_set_buffer((GnomeScanScanner*)self, buffer);

	return TRUE;
}

/* Resize buffer to actual size. This is needed for hand scanner where
   we put an arbitrary huge height and then shrink back to the actual
   height. */
static void
gsane_scanner_resize_buffer(GSaneScanner *self)
{
	GeglBuffer *buffer;
	const GeglRectangle *extent;
	GeglRectangle *actual_extent;
	guint actual_height;

	buffer = gnome_scan_scanner_get_buffer(GNOME_SCAN_SCANNER(self));
	extent = gegl_buffer_get_extent(buffer);
	actual_height = (self->priv->bytes_read/self->priv->frame_count) / self->priv->params.bytes_per_line;
	actual_extent = g_boxed_copy(GEGL_TYPE_RECTANGLE, extent);
	actual_extent->height = actual_height;
	gegl_buffer_set_extent(buffer, actual_extent);
	g_boxed_free(GEGL_TYPE_RECTANGLE, actual_extent);
}

/* Called once SANE stop sending data */
static gboolean
gsane_scanner_end_frame(GSaneScanner *self, SANE_Status status)
{
	g_debug("%s", sane_strstatus(status));
	/* The frame is acquired */
	if (status == SANE_STATUS_EOF) {
		if (!self->priv->params.last_frame)
			/* start next frame */
			return gsane_scanner_start_frame(self);
		else {
			/* terminate hand scanner acquisition */
			if (self->priv->params.lines == -1)
				gsane_scanner_resize_buffer(self);
			return FALSE; /* job done :). */
		}
	}
	else
		return FALSE; /* an error occur, just stop the work */
}

/* Actually ask SANE data and work on it. */
static gboolean
gsane_scanner_work(GnomeScanScanner*scanner, gdouble *progress)
{
	GSaneScanner *self = (GSaneScanner*) scanner;
	SANE_Status status;
	SANE_Byte *buf;
	SANE_Int read_len;

	buf = g_new0(SANE_Byte, self->priv->chunk_len);
	status = sane_read (self->priv->handle, buf, self->priv->chunk_len, &read_len);

	if (status != SANE_STATUS_GOOD)
		/* close current frame and start next if this is not
		   the last frame of the image */
		return gsane_scanner_end_frame(self, status);

	self->priv->bytes_read += read_len;
	/* process the bytes received */
	gsane_processor_process(&self->priv->processor, buf, read_len);
	g_free(buf);

	/* compute progress */
	*progress = (gdouble)self->priv->bytes_read/(gdouble)self->priv->total_bytes_count;
	return TRUE;
}

static void
gsane_scanner_end_image(GnomeScanNode*scanner)
{
	GSaneScanner *self = GSANE_SCANNER(scanner);
	/* Some backends needs to explicitely stop scan even after
	   EOF. do we need this for each frame ? */
	sane_cancel(self->priv->handle);
	self->priv->image_count++;
}

static GObject*
gsane_scanner_constructor(GType type, guint n_construct_properties, GObjectConstructParam* construct_params)
{
	GObject* object = G_OBJECT_CLASS(gsane_scanner_parent_class)->constructor(type, n_construct_properties, construct_params);
	GSaneScanner *self = GSANE_SCANNER(object);

	self->priv->opt_thread = g_thread_create((GThreadFunc) gsane_scanner_probe_options, self, FALSE, NULL);

	return object;
}

static void
gsane_scanner_init(GSaneScanner *self)
{
	self->priv = GSANE_SCANNER_GET_PRIVATE(self);
	self->priv->option_handlers	= g_hash_table_new_full(g_str_hash, g_str_equal, g_free, (GDestroyNotify)gsane_option_handler_destroy);
	self->priv->handle		= NULL;
	self->priv->opt_thread		= NULL;
	self->priv->params_thread	= NULL;
	self->priv->mass_acquisition	= FALSE;
	gsane_processor_init(&self->priv->processor);
}

static void
gsane_scanner_finalize(GObject *object)
{
	GSaneScanner *self = GSANE_SCANNER(object);
	g_debug("%s(%s)", __FUNCTION__, self->priv->sane_id);

	gsane_processor_destroy(&self->priv->processor);

	self->priv->sane_id = (self->priv->sane_id == NULL ? NULL : g_free(self->priv->sane_id), NULL);
	self->priv->sane_type = (self->priv->sane_type == NULL ? NULL : g_free(self->priv->sane_type), NULL);
	g_hash_table_destroy(self->priv->option_handlers);

	if (self->priv->opt_thread)
		g_thread_join(self->priv->opt_thread);

	if (self->priv->params_thread)
		g_thread_join(self->priv->params_thread);
}

static void
gsane_scanner_set_property(GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
	GSaneScanner *self;
	self = GSANE_SCANNER(object);
	switch(property_id) {
	case GSANE_SCANNER_SANE_ID:
		self->priv->sane_id = g_value_dup_string(value);
		break;
	case GSANE_SCANNER_SANE_TYPE:
		self->priv->sane_type = g_value_dup_string(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
		break;
	}
}

static void
gsane_scanner_class_init(GSaneScannerClass *klass)
{
	GObjectClass *o_class = G_OBJECT_CLASS(klass);

	g_type_class_add_private(o_class, sizeof(GSaneScannerPrivate));

	o_class->constructor	= gsane_scanner_constructor;
	o_class->finalize	= gsane_scanner_finalize;
	/* there is no readable properties */
	o_class->set_property	= gsane_scanner_set_property;
	g_object_class_install_property(o_class, GSANE_SCANNER_SANE_ID, g_param_spec_string("sane-id", "SANE ID",  "SANE device id", NULL,G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property(o_class, GSANE_SCANNER_SANE_TYPE, g_param_spec_string("sane-type", "SANE Type", "SANE device type", NULL, G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));
	g_signal_new("reload-options", GSANE_TYPE_SCANNER, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	GnomeScanNodeClass *n_class = GNOME_SCAN_NODE_CLASS(klass);
	n_class->start_scan	= gsane_scanner_start_scan;
	n_class->start_image	= gsane_scanner_start_image;
	n_class->end_image	= gsane_scanner_end_image;

	GnomeScanScannerClass *s_class = GNOME_SCAN_SCANNER_CLASS(klass);
	s_class->work = gsane_scanner_work;
}

static void
gsane_scanner_class_finalize(GSaneScannerClass *klass)
{
}

void
gsane_scanner_register(GTypeModule *module)
{
	gsane_scanner_register_type(module);
}
