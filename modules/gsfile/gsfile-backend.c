/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-scan
 * Copyright (C) Étienne Bersac 2007 <bersace03@laposte.net>
 * 
 * gnome-scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#include "gsfile-backend.h"
#include "gsfile-scanner.h"

G_DEFINE_DYNAMIC_TYPE (GSFileBackend, gsfile_backend, GNOME_SCAN_TYPE_BACKEND);

void*
gsfile_backend_probe_scanners (GnomeScanBackend *self)
{
	GnomeScanScanner *scanner;
	scanner = gsfile_scanner_new ();
	g_signal_emit_by_name(self, "scanner-added", scanner);
	g_object_unref (scanner);
	g_signal_emit_by_name(self, "probe-done");
	return NULL;
}

static void
gsfile_backend_init (GSFileBackend *object)
{
	/* TODO: Add initialization code here */
}

static void
gsfile_backend_finalize (GObject *self)
{
	/* TODO: Add deinitalization code here */

	G_OBJECT_CLASS (gsfile_backend_parent_class)->finalize (self);
}

static void
gsfile_backend_class_init (GSFileBackendClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	GnomeScanBackendClass *backend_class = GNOME_SCAN_BACKEND_CLASS (klass);
	
	object_class->finalize = gsfile_backend_finalize;
	backend_class->probe_scanners = gsfile_backend_probe_scanners;
}

static void
gsfile_backend_class_finalize(GSFileBackendClass *klass)
{
}

void
gsfile_backend_register(GTypeModule *module)
{
	gsfile_backend_register_type(module);
}
