/* Gnome Scan - Scan as easy as you print
 * Copyright © 2007  Étienne Bersac <bersace@gnome.org>
 *
 * Gnome Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */


#ifndef _GSFILE_SCANNER_H_
#define _GSFILE_SCANNER_H_

#include <glib-object.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define GSFILE_TYPE_SCANNER             (gsfile_scanner_get_type ())
#define GSFILE_SCANNER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSFILE_TYPE_SCANNER, GSFileScanner))
#define GSFILE_SCANNER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GSFILE_TYPE_SCANNER, GSFileScannerClass))
#define GSFILE_IS_SCANNER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSFILE_TYPE_SCANNER))
#define GSFILE_IS_SCANNER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GSFILE_TYPE_SCANNER))
#define GSFILE_SCANNER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GSFILE_TYPE_SCANNER, GSFileScannerClass))

typedef struct _GSFileScanner GSFileScanner;
typedef struct _GSFileScannerClass GSFileScannerClass;
typedef struct _GSFileScannerPrivate GSFileScannerPrivate;

struct _GSFileScannerClass
{
	GnomeScanScannerClass parent_class;
};

struct _GSFileScanner
{
	GnomeScanScanner parent_instance;
	GSFileScannerPrivate* priv;
};

GType gsfile_scanner_get_type ();
void gsfile_scanner_register (GTypeModule *module);
GnomeScanScanner *gsfile_scanner_new ();

G_END_DECLS

#endif /* _GSFILE_SCANNER_H_ */
