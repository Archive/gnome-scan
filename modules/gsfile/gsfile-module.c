/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */


#include <gnome-scan.h>
#include "gsfile-backend.h"
#include "gsfile-scanner.h"
#include "gsfile-options.h"
#include "gsfile-filenames-widget.h"

G_MODULE_EXPORT void
gnome_scan_module_init (GnomeScanModule *module)
{
	gsfile_backend_register (G_TYPE_MODULE (module));
	gsfile_scanner_register (G_TYPE_MODULE (module));
	gsfile_option_filenames_register(G_TYPE_MODULE(module));
	gsfile_filenames_widget_register(G_TYPE_MODULE(module));

	gnome_scan_option_manager_register_rule_by_type(gnome_scan_option_manager, GSFILE_TYPE_OPTION_FILENAMES, GSFILE_TYPE_FILENAMES_WIDGET);
}

G_MODULE_EXPORT void
gnome_scan_module_finalize (GnomeScanModule *module)
{
}
