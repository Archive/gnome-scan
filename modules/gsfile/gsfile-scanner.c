/* GNOME Scan - Scan as easy as you print
* Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
*
* GNOME Scan is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* GNOME Scan is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with GNOME Scan. If not, write to:
*
*	the Free Software Foundation, Inc.
*	51 Franklin Street, Fifth Floor
*	Boston, MA 02110-1301, USA
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>
#include <gnome-scan.h>
#include "gsfile-options.h"
#include "gsfile-scanner.h"

#define	GSFILE_SCANNER_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GSFILE_TYPE_SCANNER, GSFileScannerPrivate))

struct _GSFileScannerPrivate
{
	GSList* filenames;
	GSList* current;
	GeglNode* load;
};

G_DEFINE_DYNAMIC_TYPE (GSFileScanner, gsfile_scanner, GNOME_SCAN_TYPE_SCANNER);

GnomeScanScanner*
gsfile_scanner_new ()
{
	GObject *object = g_object_new (GSFILE_TYPE_SCANNER,
					/* translator: this is the name of the file
					loader backend which allow user to scan 
					a list a images */
					"name", _("Files"),
					"blurb", _("Import from files."),
					"icon-name", "gnome-mime-image",
					NULL);
	return GNOME_SCAN_SCANNER (object);
}

static void
gsfile_scanner_start_scan(GnomeScanNode *node)
{
	GSFileScanner *self = GSFILE_SCANNER(node);
	self->priv->current = self->priv->filenames;
}

static gboolean
gsfile_scanner_start_image(GnomeScanNode *node)
{
	GSFileScanner *self = GSFILE_SCANNER(node);
	if (!self->priv->current)
		return FALSE;

	gegl_node_set(self->priv->load,
		      "path", self->priv->current->data,
		      NULL);
	g_debug(G_STRLOC ": Scan %s", (gchar*) self->priv->current->data);
	return TRUE;
}

static void
gsfile_scanner_end_image(GnomeScanNode *node)
{
	GSFileScanner *self = GSFILE_SCANNER(node);
	self->priv->current = g_slist_next(self->priv->current);
}

static void
gsfile_scanner_on_filenames_modified(GSFileScanner* self, GParamSpec *pspec, GSFileOptionFilenames *option)
{
	GSList *list;
	GnomeScanStatus status;
	g_return_if_fail(GSFILE_IS_SCANNER(self));
	g_object_get(option, "value", &list, NULL);
	self->priv->filenames = list;
	self->priv->current = list;
	status = g_slist_length(list) > 0 ? GNOME_SCAN_STATUS_READY : GNOME_SCAN_STATUS_UNCONFIGURED;
	gnome_scan_node_update_status((GnomeScanNode*) self, status, NULL);
}





/* GOBJECT */
static GObject*
gsfile_scanner_constructor(GType type, guint n_construct_properties, GObjectConstructParam* construct_params)
{
	GObject* object = G_OBJECT_CLASS(gsfile_scanner_parent_class)->constructor(type, n_construct_properties, construct_params);
	GSFileScanner* self = GSFILE_SCANNER(object);
	GSList *formats = NULL;
	GSFileOptionFilenames *option;

	static gchar* png_mime[] = {"image/png",NULL};
	static gchar* png_exts[] = {"png",NULL};

	formats = g_slist_append (formats,
				  gnome_scan_format_new("png",
							N_("PNG picture"),
							GETTEXT_PACKAGE,
							"image",
							png_mime, G_N_ELEMENTS(png_mime),
							png_exts, G_N_ELEMENTS(png_exts)));


	static gchar *jpeg_mime[] = {"image/jpeg",NULL};
	static gchar *jpeg_exts[] = {"jpeg","jpe","jpg",NULL};
	formats = g_slist_append (formats,
				  gnome_scan_format_new("jpeg",
							N_("JPEG picture"),
							GETTEXT_PACKAGE,
							"image",
							jpeg_mime, G_N_ELEMENTS(jpeg_mime),
							jpeg_exts, G_N_ELEMENTS(jpeg_exts)));


	option = gsfile_option_filenames_new("filenames", "Filenames", "Files to scan", GETTEXT_PACKAGE,
					     GNOME_SCAN_OPTION_GROUP_SCANNER,
					     formats,
					     GNOME_SCAN_OPTION_HINT_PRIMARY);
	g_signal_connect_swapped(option, "notify::value", G_CALLBACK(gsfile_scanner_on_filenames_modified), self);
	gnome_scan_node_install_option((GnomeScanNode*) self, GNOME_SCAN_OPTION(option));

	gnome_scan_node_update_status((GnomeScanNode*) self, GNOME_SCAN_STATUS_UNCONFIGURED, NULL);

	/* construct gegl pipeline */
	self->priv->load = gegl_node_new();
	gegl_node_set(self->priv->load,
		      "operation", "gegl:load",
		      NULL);
	gnome_scan_node_append_node(GNOME_SCAN_NODE(self), self->priv->load);

	return object;
}

static void
gsfile_scanner_class_init (GSFileScannerClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	GnomeScanNodeClass* node_class = GNOME_SCAN_NODE_CLASS(klass);

	g_type_class_add_private (object_class, sizeof (GSFileScannerPrivate));

	object_class->constructor	= gsfile_scanner_constructor;

	node_class->start_scan	= gsfile_scanner_start_scan;
	node_class->start_image	= gsfile_scanner_start_image;
	node_class->end_image	= gsfile_scanner_end_image;
}

static void
gsfile_scanner_class_finalize(GSFileScannerClass *klass)
{
}

static void
gsfile_scanner_init (GSFileScanner *self)
{
	self->priv = GSFILE_SCANNER_GET_PRIVATE(self);
	self->priv->filenames = NULL;
	self->priv->current = NULL;
}

void
gsfile_scanner_register(GTypeModule *module)
{
	gsfile_scanner_register_type(module);
}
