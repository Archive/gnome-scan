/* GSFile - Scan from files
 * Copyright © 2007  Étienne Bersac <bersace03@laposte.net>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
#include <gnome-scan.h>

#ifndef _GSFILE_OPTION_H
#define _GSFILE_OPTION_H

G_BEGIN_DECLS

#define	GSFILE_TYPE_OPTION_FILENAMES	(gsfile_option_filenames_get_type())
#define	GSFILE_OPTION_FILENAMES(o)	(G_TYPE_CHECK_INSTANCE_CAST((o), GSFILE_TYPE_OPTION_FILENAMES, GSFileOptionFilenames))
#define GSFILE_IS_OPTION_FILENAMES(o)	(G_TYPE_CHECK_INSTANCE_TYPE((o), GSFILE_TYPE_OPTION_FILENAMES))
	
typedef struct _GSFileOptionFilenames GSFileOptionFilenames;
typedef struct _GSFileOptionFilenamesClass GSFileOptionFilenamesClass;
typedef struct _GSFileOptionFilenamesPrivate GSFileOptionFilenamesPrivate;

struct _GSFileOptionFilenames {
	GnomeScanOption	parent_instance;
	GSFileOptionFilenamesPrivate* priv;
};

struct _GSFileOptionFilenamesClass {
	GnomeScanOptionClass parent_class;
};

void gsfile_option_filenames_register(GTypeModule* module);
GType gsfile_option_filenames_get_type(void) G_GNUC_CONST;
GSFileOptionFilenames* gsfile_option_filenames_new(const gchar* name, const gchar* title, const gchar* desc, const gchar* domain, const gchar* group, GSList* formats, GnomeScanOptionHint hint);
GSList* gsfile_option_filenames_get_formats(GSFileOptionFilenames*self);
void gsfile_option_filenames_set_formats(GSFileOptionFilenames*self, GSList* formats);
GSList* gsfile_option_filenames_get_value(GSFileOptionFilenames* self);
void gsfile_option_filenames_set_value(GSFileOptionFilenames* self, GSList* filenames);


G_END_DECLS

#endif
