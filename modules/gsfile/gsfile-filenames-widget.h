/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifndef _GSFILE_FILENAMES_WIDGET_H_
#define _GSFILE_FILENAMES_WIDGET_H_

#include <glib-object.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define GSFILE_TYPE_FILENAMES_WIDGET             (gsfile_filenames_widget_get_type ())
#define GSFILE_FILENAMES_WIDGET(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSFILE_TYPE_FILENAMES_WIDGET, GSFileFilenamesWidget))
#define GSFILE_FILENAMES_WIDGET_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GSFILE_TYPE_FILENAMES_WIDGET, GSFileFilenamesWidgetClass))
#define GSFILE_IS_FILENAMES_WIDGET(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSFILE_TYPE_FILENAMES_WIDGET))
#define GSFILE_IS_FILENAMES_WIDGET_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GSFILE_TYPE_FILENAMES_WIDGET))
#define GSFILE_FILENAMES_WIDGET_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GSFILE_TYPE_FILENAMES_WIDGET, GSFileFilenamesWidgetClass))

typedef struct _GSFileFilenamesWidget GSFileFilenamesWidget;
typedef struct _GSFileFilenamesWidgetClass GSFileFilenamesWidgetClass;
typedef struct _GSFileFilenamesWidgetPrivate GSFileFilenamesWidgetPrivate;

struct _GSFileFilenamesWidget
{
	GnomeScanOptionWidget parent_instance;
	GSFileFilenamesWidgetPrivate *priv;
};

struct _GSFileFilenamesWidgetClass
{
	GnomeScanOptionWidgetClass parent_class;
};

void gsfile_filenames_widget_register(GTypeModule* module);
GType gsfile_filenames_widget_get_type(void) G_GNUC_CONST;


G_END_DECLS

#endif /* _GSFILE_FILENAMES_WIDGET_H_ */
