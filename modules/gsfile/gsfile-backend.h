/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-scan
 * Copyright (C) Étienne Bersac 2007 <bersace03@laposte.net>
 * 
 * gnome-scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with gnome-scan.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#ifndef _GSFILE_BACKEND_H_
#define _GSFILE_BACKEND_H_

#include <glib-object.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define GSFILE_TYPE_BACKEND             (gsfile_backend_get_type ())
#define GSFILE_BACKEND(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSFILE_TYPE_BACKEND, GSFileBackend))
#define GSFILE_BACKEND_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GSFILE_TYPE_BACKEND, GSFileBackendClass))
#define GNOME_IS_SCAN_BACKEND_FILE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSFILE_TYPE_BACKEND))
#define GNOME_IS_SCAN_BACKEND_FILE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GSFILE_TYPE_BACKEND))
#define GSFILE_BACKEND_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GSFILE_TYPE_BACKEND, GSFileBackendClass))

typedef struct _GSFileBackendClass GSFileBackendClass;
typedef struct _GSFileBackend GSFileBackend;

struct _GSFileBackendClass
{
	GnomeScanBackendClass parent_class;
};

struct _GSFileBackend
{
	GnomeScanBackend parent_instance;
};

GType gsfile_backend_get_type (void) G_GNUC_CONST;
void gsfile_backend_register (GTypeModule *module);

G_END_DECLS

#endif /* _GSFILE_BACKEND_H_ */
