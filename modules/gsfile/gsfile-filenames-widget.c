/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/* GNOME Scan - Scan as easy as you print
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

#ifdef  HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>
#include "gsfile-options.h"
#include "gsfile-filenames-widget.h"

#define	GSFILE_FILENAMES_WIDGET_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GSFILE_TYPE_FILENAMES_WIDGET, GSFileFilenamesWidgetPrivate))

#define	PREVIEW_SIZE	96

struct _GSFileFilenamesWidgetPrivate
{
	GtkWidget			*filechooser;
	GtkListStore		*liststore;
	GtkTreeSelection	*selection;
	GtkWidget			*clear;
	GtkWidget			*remove;
};

enum {
	FILENAMES_PREVIEW,
	FILENAMES_BASENAME,
	FILENAMES_PATHNAME,
	FILENAMES_N_COLUMNS
};

static void gsfile_filenames_widget_populate(GSFileFilenamesWidget *self,
						   GSList* filenames);
static void	gsfile_filenames_widget_filenames_add (GtkButton *button,
								 GSFileFilenamesWidget *self);
static void	gsfile_filenames_widget_filenames_remove (GtkButton *button,
									GSFileFilenamesWidget *self);
static void	gsfile_filenames_widget_filenames_clear (GtkButton *button,
								   GSFileFilenamesWidget *self);
static void	gsfile_filenames_widget_filenames_preview (GSFileFilenamesWidget *self);
static gboolean gsfile_filenames_widget_filenames_foreach_preview (GtkTreeModel *model,
												 GtkTreePath *path,
												 GtkTreeIter *iter,
												 GSFileFilenamesWidget *self);
static gboolean	gsfile_filenames_widget_filenames_foreach_fetch (GtkTreeModel *model,
											   GtkTreePath *path,
											   GtkTreeIter *iter,
											   GSList **list);

static void gsfile_filenames_widget_filenames_changed(GSFileFilenamesWidget *self);
static void gsfile_filenames_widget_selection_changed(GSFileFilenamesWidget *self);
static void gsfile_filenames_widget_filenames_update(GSFileFilenamesWidget *self);

G_DEFINE_DYNAMIC_TYPE (GSFileFilenamesWidget, gsfile_filenames_widget, GNOME_SCAN_TYPE_OPTION_WIDGET)

static GtkWidget*
gsfile_filenames_widget_build_filechooser(GSFileFilenamesWidget *self)
{
	GnomeScanOption *option;
	GtkWidget* filechooser;
	GtkFileFilter *filter, *filter1;
	GSList *node;
	GnomeScanFormat *format;
	gchar **mimes, **exts;
	gchar *pattern;
	gint i;

	g_object_get(self, "option", &option, NULL);
	filechooser = gtk_file_chooser_dialog_new (_("Select files"),
											   GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (self))),
											   GTK_FILE_CHOOSER_ACTION_OPEN,
											   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											   GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
											   NULL);
	gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (filechooser),
										 g_get_home_dir ());
	gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (filechooser),
										  TRUE);
		
	/* add a global filter and a per format filter */
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Supported formats"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooser), filter);
	g_object_get(option, "formats", &node, NULL);
	for (; node ; node = node->next) {
		format = node->data;
		filter1 = gtk_file_filter_new ();
			
		mimes = format->mime_types;
		for (i = 0; mimes[i]; i++) {
			gtk_file_filter_add_mime_type (filter, mimes[i]);
			gtk_file_filter_add_mime_type (filter1, mimes[i]);
		}
			
		exts = format->suffixes;
		for (i = 0; exts[i]; i++) {
			pattern = g_strdup_printf ("*.%s", exts[i]);
			gtk_file_filter_add_pattern (filter, pattern);
			gtk_file_filter_add_pattern (filter1, pattern);
		}
			
		gtk_file_filter_set_name (filter1,
								  g_strdup_printf ("%s (*.%s)",
												   dgettext(format->domain, format->desc),
												   g_strjoinv(", *.", format->suffixes)));
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooser),
									 filter1);
	}

	return filechooser;
}

static void
gsfile_filenames_widget_populate(GSFileFilenamesWidget *self, GSList *filenames)
{
	GError *error = NULL;
	GSList* node;
	GtkTreeIter iter;

	/* first populate list store */
	for (node = filenames; node ; node = node->next) {
		gtk_list_store_insert_with_values (self->priv->liststore, &iter, G_MAXINT,
										   FILENAMES_BASENAME, g_path_get_basename (node->data),
										   FILENAMES_PATHNAME, node->data,
										   -1);
	}
	/* delegate populate creation in a thread */
	g_thread_create ((GThreadFunc) gsfile_filenames_widget_filenames_preview,
					 self, FALSE, &error);
}

static void
gsfile_filenames_widget_filenames_add (GtkButton *button, GSFileFilenamesWidget *self)
{
	GSList *node;

	/* filechooser */
	if (!self->priv->filechooser) {
		self->priv->filechooser = gsfile_filenames_widget_build_filechooser(self);
	}
	
	/* run filechooser */
	if (gtk_dialog_run (GTK_DIALOG (self->priv->filechooser)) == GTK_RESPONSE_ACCEPT) {
		gtk_widget_hide (self->priv->filechooser);
		node = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (self->priv->filechooser));
		gsfile_filenames_widget_populate(self, node);
		gsfile_filenames_widget_filenames_update(self);
	}
	gtk_widget_hide (self->priv->filechooser);
}

static void
gsfile_filenames_widget_filenames_preview (GSFileFilenamesWidget *self)
{
	/* trigger preview update (this may take some time considering list length and image size) */
	gtk_tree_model_foreach (GTK_TREE_MODEL (self->priv->liststore),
							(GtkTreeModelForeachFunc) gsfile_filenames_widget_filenames_foreach_preview,
							self);
}

/* create a preview if not present */
gboolean
gsfile_filenames_widget_filenames_foreach_preview (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, GSFileFilenamesWidget *self)
{
	GdkPixbuf *preview;
	GError *error = NULL;
	gchar *pathname;
	
	gtk_tree_model_get (model, iter,
						FILENAMES_PREVIEW, &preview,
						FILENAMES_PATHNAME, &pathname,
						-1);
	if (!preview) {
		preview = gdk_pixbuf_new_from_file_at_scale (pathname,
													 PREVIEW_SIZE, PREVIEW_SIZE,
													 TRUE, &error);
		gtk_list_store_set (self->priv->liststore, iter,
							FILENAMES_PREVIEW, preview,
							-1);
	}
	return FALSE;
}

static void
gsfile_filenames_widget_filenames_remove (GtkButton *button, GSFileFilenamesWidget *self)
{
	GtkTreeModel *model = GTK_TREE_MODEL (self->priv->liststore);
	GList *node, *first = gtk_tree_selection_get_selected_rows (self->priv->selection,
																&model);
	GtkTreeIter iter;

	for (node = first; node ; node = node->next) {
		gtk_tree_model_get_iter (model, &iter, node->data);
		gtk_list_store_remove (self->priv->liststore, &iter);
		gtk_tree_path_free (node->data);
	}
	g_list_free (first);
	gsfile_filenames_widget_filenames_update(self);
}

static void
gsfile_filenames_widget_filenames_clear (GtkButton *button, GSFileFilenamesWidget *self)
{
	gtk_list_store_clear (self->priv->liststore);
	gsfile_filenames_widget_filenames_update(self);
}

static void
gsfile_filenames_widget_filenames_update (GSFileFilenamesWidget *self)
{
	GSList *list = NULL;
	GnomeScanOption *option;
	gtk_tree_model_foreach (GTK_TREE_MODEL (self->priv->liststore),
							(GtkTreeModelForeachFunc) gsfile_filenames_widget_filenames_foreach_fetch,
							&list);
	g_object_get(self, "option", &option, NULL);
	g_object_set(option, "value", list, NULL);
}

static gboolean
gsfile_filenames_widget_filenames_foreach_fetch (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, GSList **list)
{
	gchar*pathname;
	gtk_tree_model_get (model, iter,
						FILENAMES_PATHNAME, &pathname,
						-1);
	*list = g_slist_append (*list, g_strdup(pathname));
	
	return FALSE;
}

static void
gsfile_filenames_widget_filenames_changed(GSFileFilenamesWidget *self)
{
	GtkTreePath*path = gtk_tree_path_new_first();
	GtkTreeIter iter;
	gtk_tree_model_get_iter(GTK_TREE_MODEL(self->priv->liststore), &iter, path);
	gtk_widget_set_sensitive(self->priv->clear, (gtk_list_store_iter_is_valid(self->priv->liststore, &iter)));
}

static void
gsfile_filenames_widget_selection_changed(GSFileFilenamesWidget *self)
{
	gtk_widget_set_sensitive(self->priv->remove,
							 gtk_tree_selection_get_selected(self->priv->selection,
															 NULL, NULL));
}


static GObject*
gsfile_filenames_widget_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_properties)
{
	GObject* object = G_OBJECT_CLASS(gsfile_filenames_widget_parent_class)->constructor(type, n_construct_properties, construct_properties);
	GSFileFilenamesWidget* self = GSFILE_FILENAMES_WIDGET(object);
	GtkWidget *scrolled, *tree_view, *box, *button;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GnomeScanOption *option;
	GSList *filenames;
	
	gtk_box_set_spacing (GTK_BOX (self), 6);
	g_object_set(self, "no_label", TRUE, "expand", TRUE, NULL);
	
	self->priv->liststore = gtk_list_store_new (FILENAMES_N_COLUMNS,
												GDK_TYPE_PIXBUF,
												G_TYPE_STRING,
												G_TYPE_STRING);
	g_signal_connect_swapped(self->priv->liststore, "row-inserted", G_CALLBACK(gsfile_filenames_widget_filenames_changed), self);
	g_signal_connect_swapped(self->priv->liststore, "row-deleted", G_CALLBACK(gsfile_filenames_widget_filenames_changed), self);
	
	tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (self->priv->liststore));
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree_view), TRUE);
	gtk_tree_view_set_headers_clickable (GTK_TREE_VIEW (tree_view), TRUE);
	self->priv->selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect_swapped(self->priv->selection, "changed", G_CALLBACK(gsfile_filenames_widget_selection_changed), self);	
	
	/* preview */
	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Preview"), renderer,
													   "pixbuf", FILENAMES_PREVIEW,
													   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), column);
	
	/* basename */
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Filename"), renderer,
													   "markup", FILENAMES_BASENAME,
													   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), column);
	
	/* scrolled window */
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled),
										   tree_view);
	gtk_box_pack_start (GTK_BOX (self), scrolled, TRUE, TRUE, 0);
	
	/* buttons */
	box = gtk_vbutton_box_new ();
	gtk_box_set_spacing (GTK_BOX (box), 4);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (box), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (self), box, FALSE, FALSE, 0);
	
	/* add */
	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked",
					  (GCallback) gsfile_filenames_widget_filenames_add, self);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);
	
	/* remove */
	self->priv->remove = button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_widget_set_sensitive(button, FALSE);
	g_signal_connect (button, "clicked",
					  (GCallback) gsfile_filenames_widget_filenames_remove, self);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);
	
	/* clear */
	self->priv->clear = button = gtk_button_new_from_stock (GTK_STOCK_CLEAR);
	gtk_widget_set_sensitive(button, FALSE);
	g_signal_connect (button, "clicked",
					  (GCallback) gsfile_filenames_widget_filenames_clear, self);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);

	/* populate */
	g_object_get(self, "option", &option, NULL);
	g_object_get(option, "value", &filenames, NULL);
	gsfile_filenames_widget_populate(self, filenames);

	return object;
}

static void
gsfile_filenames_widget_class_init (GSFileFilenamesWidgetClass *klass)
{
	g_type_class_add_private(klass, sizeof(GSFileFilenamesWidgetPrivate));
	G_OBJECT_CLASS(klass)->constructor = gsfile_filenames_widget_construct;
}

static void
gsfile_filenames_widget_class_finalize(GSFileFilenamesWidgetClass *klass)
{
}

static void
gsfile_filenames_widget_init (GSFileFilenamesWidget *self)
{
	self->priv = GSFILE_FILENAMES_WIDGET_GET_PRIVATE(self);
}

void
gsfile_filenames_widget_register(GTypeModule *module)
{
	gsfile_filenames_widget_register_type(module);
}
