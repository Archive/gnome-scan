/* GSFile - Scan from files
 * Copyright © 2007  Étienne Bersac <bersace03@laposte.net>
 *
 * GNOME Scan is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * GNOME Scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with GNOME Scan. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
#include "gsfile-options.h"

#define	GSFILE_OPTION_FILENAMES_GET_PRIVATE(o)	(G_TYPE_INSTANCE_GET_PRIVATE((o), GSFILE_TYPE_OPTION_FILENAMES, GSFileOptionFilenamesPrivate))

struct _GSFileOptionFilenamesPrivate {
	GSList* formats;
	GSList* filenames;
};

enum {
	GSFILE_OPTION_FILENAMES_DUMMY_PROPERTY,
	GSFILE_OPTION_FILENAMES_FORMATS,
	GSFILE_OPTION_FILENAMES_VALUE,
};

G_DEFINE_DYNAMIC_TYPE(GSFileOptionFilenames, gsfile_option_filenames, GNOME_SCAN_TYPE_OPTION);

GSFileOptionFilenames*
gsfile_option_filenames_new(const gchar* name, const gchar* title, const gchar* desc, const gchar* domain, const gchar* group, GSList* formats, GnomeScanOptionHint hint)
{
	return g_object_new(GSFILE_TYPE_OPTION_FILENAMES, "name", name, "title", title, "desc",	desc, "domain",	domain, "group", group, "formats", formats, "hint", hint, NULL);
}

GSList*
gsfile_option_filenames_get_formats(GSFileOptionFilenames*self)
{
	return self->priv->formats;
}

void
gsfile_option_filenames_set_formats(GSFileOptionFilenames*self, GSList* formats)
{
	self->priv->formats = formats;
}

GSList*
gsfile_option_filenames_get_value(GSFileOptionFilenames* self)
{
	return self->priv->filenames;
}

void
gsfile_option_filenames_set_value(GSFileOptionFilenames* self, GSList* filenames)
{
	self->priv->filenames = filenames;
}

static void
gsfile_option_filenames_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	GSFileOptionFilenames* self = GSFILE_OPTION_FILENAMES(object);
	switch(property_id) {
	case GSFILE_OPTION_FILENAMES_FORMATS:
		g_value_set_pointer(value, gsfile_option_filenames_get_formats(self));
		break;
	case GSFILE_OPTION_FILENAMES_VALUE:
		g_value_set_pointer(value, gsfile_option_filenames_get_value(self));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
gsfile_option_filenames_set_property(GObject* object, guint property_id, const GValue* value, GParamSpec* pspec)
{
	GSFileOptionFilenames* self = GSFILE_OPTION_FILENAMES(object);
	switch(property_id) {
	case GSFILE_OPTION_FILENAMES_FORMATS:
		gsfile_option_filenames_set_formats(self, g_value_get_pointer(value));
		break;
	case GSFILE_OPTION_FILENAMES_VALUE:
		gsfile_option_filenames_set_value(self, g_value_get_pointer(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
gsfile_option_filenames_class_init(GSFileOptionFilenamesClass *klass)
{
	g_type_class_add_private(klass, sizeof(GSFileOptionFilenamesPrivate));
	GObjectClass* object_class = G_OBJECT_CLASS(klass);
	object_class->get_property	= gsfile_option_filenames_get_property;
	object_class->set_property	= gsfile_option_filenames_set_property;
	g_object_class_install_property(object_class,
					GSFILE_OPTION_FILENAMES_FORMATS,
					g_param_spec_pointer("formats", "Formats", "List for format available",
							     G_PARAM_STATIC_STRINGS|G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property(object_class,
					GSFILE_OPTION_FILENAMES_VALUE,
					g_param_spec_pointer("value", "Value", "List for files to scan",
							     G_PARAM_STATIC_STRINGS|G_PARAM_READWRITE));
}

static void
gsfile_option_filenames_class_finalize(GSFileOptionFilenamesClass *klass)
{
}

static void
gsfile_option_filenames_init(GSFileOptionFilenames* self)
{
	self->priv = GSFILE_OPTION_FILENAMES_GET_PRIVATE(self);
}

void
gsfile_option_filenames_register(GTypeModule *module)
{
	gsfile_option_filenames_register_type(module);
}
