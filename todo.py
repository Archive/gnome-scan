#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# this program transform an Anjuta TODO.tasks into a regular TODO
# plain text file.
#
# GPL stuff goes here
#

from xml.dom.minidom    import parse
from string             import split
import codecs

dom = parse('TODO.tasks')
gtodo = dom.childNodes[0]
f = codecs.open('TODO', 'w', 'utf-8')

# loop categories
for category in gtodo.childNodes:
    if category.nodeType != category.TEXT_NODE:
        title = category.getAttribute('title')
        f.write("* %s\n" % title)

        # loop items
        for item in category.childNodes:
            if item.nodeType != item.TEXT_NODE:
                done = item.childNodes[1].getAttribute('done')
                if done == "0":
                    summary = item.childNodes[3].childNodes[0]
                    f.write("  * %s\n" % summary.wholeText)

                    # print indented comment
                    comment = item.childNodes[5]
                    if (comment.childNodes.length == 1):
                        comment = comment.childNodes[0].wholeText
                        lines = split(comment,"\n")
                        for line in lines:
                            f.write("    %s\n" % line)
        f.write("\n")
f.close()
