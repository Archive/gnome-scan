/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-scan
 * Copyright (C) Étienne Bersac 2007 <bersace03@laposte.net>
 * 
 * gnome-scan is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gnome-scan.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

/**
 * SECTION: 
 * @short_description: #FlegitaGimpSink class
 * 
 **/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <glib/gi18n.h>
#include <gegl.h>
#include <libgimp/gimp.h>
#include "flegita-gimp-sink.h"


typedef struct _FlegitaGimpSinkPrivate FlegitaGimpSinkPrivate;
struct _FlegitaGimpSinkPrivate
{
	gint32  		layer;
	gchar 			*layer_name;
	GimpDrawable	*drawable;
	const GeglRectangle	*extent;
	GeglRectangle	actual_extent;
	BablFormat		*format;
	GimpPixelRgn	rgn;
	gpointer		iter;
	GeglBuffer		*buffer;
	GeglNode		*save;
};

#define GET_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), FLEGITA_TYPE_GIMP_SINK, FlegitaGimpSinkPrivate))

static void			fgs_configure		(GnomeScanPlugin *plugin,
									 GnomeScanSettings *settings);
static GList*		fgs_get_child_nodes	(GnomeScanPlugin *plugin,
										 GeglNode *root);
static gboolean		fgs_start_frame		(GnomeScanPlugin *plugin);
static gboolean		fgs_work			(GnomeScanPlugin *plugin,
 gdouble *progress);
static void			fgs_end_frame		(GnomeScanPlugin *plugin);

G_DEFINE_TYPE (FlegitaGimpSink, flegita_gimp_sink, GNOME_SCAN_TYPE_SINK);

#define	PARAM_LAYER_NAME	"layer-name"

static void
flegita_gimp_sink_init (FlegitaGimpSink *object)
{
	/* 	FlegitaGimpSinkPrivate *priv = GET_PRIVATE (object); */
	GParamSpec *pspec;
	
	pspec = gs_param_spec_string (PARAM_LAYER_NAME,
								  N_("Layer"),
								  N_("New layer name"),
								  GS_PARAM_GROUP_SINK_FRONT,
								  _("Scanned image"),
								  G_PARAM_WRITABLE);
	gs_param_spec_set_domain (pspec, GETTEXT_PACKAGE);
	gs_param_spec_set_unit (pspec, GNOME_SCAN_UNIT_NONE);
	gnome_scan_plugin_params_add (GNOME_SCAN_PLUGIN (object),
								  pspec);
}

static void
flegita_gimp_sink_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */

	G_OBJECT_CLASS (flegita_gimp_sink_parent_class)->finalize (object);
}

static void
flegita_gimp_sink_class_init (FlegitaGimpSinkClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	GnomeScanSinkClass* parent_class = GNOME_SCAN_SINK_CLASS (klass);
	GnomeScanPluginClass *plugin_class = GNOME_SCAN_PLUGIN_CLASS (klass);

	g_type_class_add_private (klass, sizeof (FlegitaGimpSinkPrivate));

	plugin_class->configure			= fgs_configure;
	plugin_class->get_child_nodes	= fgs_get_child_nodes;
	plugin_class->start_frame		= fgs_start_frame;
	plugin_class->work				= fgs_work;
	plugin_class->end_frame			= fgs_end_frame;

	object_class->finalize = flegita_gimp_sink_finalize;
}

GnomeScanSink*
flegita_gimp_sink_new (gint32 image_ID,
					   gboolean is_new)
{
	GObject *o = g_object_new (FLEGITA_TYPE_GIMP_SINK,
							   NULL);
	FlegitaGimpSink *s = FLEGITA_GIMP_SINK (o);

	s->image_ID = image_ID;
	s->is_new = is_new;

	return GNOME_SCAN_SINK (o);
}


/* INTERNALS */
static void
fgs_configure (GnomeScanPlugin *plugin, GnomeScanSettings *settings)
{
	FlegitaGimpSink *sink = FLEGITA_GIMP_SINK (plugin);
	FlegitaGimpSinkPrivate *priv = GET_PRIVATE (sink);

	priv->layer_name = gnome_scan_settings_get_string (settings,
													   PARAM_LAYER_NAME);
	if (sink->is_new) {
		gdouble res = gnome_scan_settings_get_double (settings, "resolution");
		gimp_image_set_resolution (sink->image_ID,
								   res, res);
	}
}

static GList*
fgs_get_child_nodes	(GnomeScanPlugin *plugin,
					 GeglNode *root)
{
	FlegitaGimpSinkPrivate *priv = GET_PRIVATE (plugin);
	GList *list = NULL;
	priv->save = gegl_node_new_child (root,
									  "operation", "gegl:save-buffer",
									  "buffer", &priv->buffer,
									  NULL);
	list = g_list_append (list, priv->save);
	return list;
}

static gboolean
fgs_start_frame (GnomeScanPlugin *plugin)
{
	FlegitaGimpSink *sink = FLEGITA_GIMP_SINK (plugin);
	FlegitaGimpSinkPrivate *priv = GET_PRIVATE (sink);
	GimpPixelRgn	rgn;
	BablFormat *format = NULL;
	gchar*format_name = NULL;
	gint i;

	if (!gegl_buffer_get_pixel_count (priv->buffer)) {
		g_warning (G_STRLOC ": No image to process !");
		return FALSE;
	}

	priv->extent = gegl_buffer_get_extent (priv->buffer);
	priv->actual_extent.width = priv->extent->width - priv->extent->x;
	priv->actual_extent.height = priv->extent->height - priv->extent->y;
	
	g_debug (G_STRLOC ": rect is %ix%i+%i+%i",
			 priv->extent->width, priv->extent->height,
			 priv->extent->x, priv->extent->y);
	g_object_get (priv->buffer, "format", &format, NULL);

	guint image_type = 0;
	switch (format->components) {
	case 1:
		image_type = GIMP_GRAY_IMAGE;
		format_name = "Y";
		break;
	case 3:
	default:
		image_type = GIMP_RGB_IMAGE;
		format_name = "RGB";
		break;
	}

	gint bps = format->type[0]->bits;
	for (i = 0; i < format->components; i++) {
		g_debug (G_STRLOC ": component %i:%s use %i bits",i,
				 format->type[i]->instance.name,
				 format->type[i]->bits);
	}
	format_name = g_strdup_printf ("%s u%i", format_name, bps > 8 ? 16 : bps);

	priv->format = (BablFormat*) babl_format (format_name);
	gdouble opacity = 100.;

	priv->layer = gimp_layer_new (sink->image_ID,
								  priv->layer_name,
								  priv->actual_extent.width, priv->actual_extent.height,
								  image_type,
								  opacity,
								  GIMP_NORMAL_MODE);

	priv->drawable = gimp_drawable_get (priv->layer);
	gimp_pixel_rgn_init (&priv->rgn, priv->drawable, 0, 0,
						 priv->actual_extent.width, priv->actual_extent.height,
						 TRUE, FALSE);

	if (sink->is_new) {
		gimp_image_resize (sink->image_ID,
						   MAX (priv->actual_extent.width, gimp_image_width (sink->image_ID)),
						   MAX (priv->actual_extent.height, gimp_image_height (sink->image_ID)),
						   0, 0);

	}

	priv->iter = gimp_pixel_rgns_register (1, &priv->rgn);
	return priv->iter != NULL;
}

static gboolean
fgs_work (GnomeScanPlugin *plugin, gdouble *progress)
{
	FlegitaGimpSinkPrivate *priv = GET_PRIVATE (plugin);
	GimpPixelRgn *rgn = &priv->rgn;

	gint chunk_size = rgn->w * rgn->h * priv->format->bytes_per_pixel;
	GeglRectangle rect = {
		.x		= rgn->x + priv->extent->x,
		.y		= rgn->y + priv->extent->y,
		.width	= rgn->w,
		.height	= rgn->h
	};
	guchar* chunk = g_new0 (guchar, chunk_size);

	gegl_buffer_get (priv->buffer,
					 1., &rect,
					 (Babl*) priv->format,
					 chunk,
					 GEGL_AUTO_ROWSTRIDE);

	guchar       *dest = rgn->data;
	memcpy (dest, chunk, chunk_size);
	g_free (chunk);

	priv->iter = gimp_pixel_rgns_process (priv->iter);
	return priv->iter != NULL;
}

static void
fgs_end_frame (GnomeScanPlugin *plugin)
{
	FlegitaGimpSink *sink = FLEGITA_GIMP_SINK (plugin);
	FlegitaGimpSinkPrivate *priv = GET_PRIVATE (plugin);

	gint32 image = sink->image_ID;
	gimp_image_add_layer (image,
						  priv->layer,
						  -1);

	gimp_image_undo_freeze (image);
	/* center */
	gimp_layer_translate (priv->layer,
						  (gimp_image_width (image) - priv->actual_extent.width) / 2,
						  (gimp_image_height (image) - priv->actual_extent.height) / 2);

	gimp_image_undo_thaw (image);

	gimp_drawable_detach (priv->drawable);

#define	destroy(o)	g_object_unref(o); o=NULL;
	destroy (priv->buffer);
	destroy (priv->save);
#undef	destroy
}
