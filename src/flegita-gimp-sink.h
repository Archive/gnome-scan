/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-scan
 * Copyright (C) Étienne Bersac 2007 <bersace03@laposte.net>
 * 
 * gnome-scan is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * gnome-scan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gnome-scan.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#ifndef _FLEGITA_GIMP_SINK_H_
#define _FLEGITA_GIMP_SINK_H_

#include <glib-object.h>
#include <gnome-scan.h>

G_BEGIN_DECLS

#define FLEGITA_TYPE_GIMP_SINK             (flegita_gimp_sink_get_type ())
#define FLEGITA_GIMP_SINK(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), FLEGITA_TYPE_GIMP_SINK, FlegitaGimpSink))
#define FLEGITA_GIMP_SINK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), FLEGITA_TYPE_GIMP_SINK, FlegitaGimpSinkClass))
#define FLEGITA_IS_GIMP_SINK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), FLEGITA_TYPE_GIMP_SINK))
#define FLEGITA_IS_GIMP_SINK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), FLEGITA_TYPE_GIMP_SINK))
#define FLEGITA_GIMP_SINK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), FLEGITA_TYPE_GIMP_SINK, FlegitaGimpSinkClass))

typedef struct _FlegitaGimpSinkClass FlegitaGimpSinkClass;
typedef struct _FlegitaGimpSink FlegitaGimpSink;

struct _FlegitaGimpSinkClass
{
	GnomeScanSinkClass parent_class;
};

/**
 * FlegitaGimpSink:
 *
 * 
 **/
struct _FlegitaGimpSink
{
	/*< private >*/
	GnomeScanSink parent_instance;

	/*< public >*/
	gint32	image_ID;			/* ID of the image where to add new layers */
	gboolean is_new;			/* Wether image image_ID is new (and thus, we should resize it */
};

GType flegita_gimp_sink_get_type (void) G_GNUC_CONST;
GnomeScanSink *flegita_gimp_sink_new (gint32 image_ID,
									  gboolean is_new);

G_END_DECLS

#endif /* _FLEGITA_GIMP_SINK_H_ */
