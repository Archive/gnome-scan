/* Flegita - Scanner utility
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * Flegita is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Flegita is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Flegita. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using Config;
using GLib;
using Gegl;
using Gnome.Scan;

namespace Flegita {
	public class Sink : Gnome.Scan.Sink {
		private Gegl.Node _save;
		private string directory;
		private string basename;
		private string suffix;
		private int count = 0;

		construct {
			Gnome.Scan.Format format;
			OptionOutputFilename option;
			weak string gettext_package = GETTEXT_PACKAGE;

			option = new OptionOutputFilename("output-filename", N_("Output Filename"), N_("Output filename"), GETTEXT_PACKAGE, OPTION_GROUP_SINK, OptionHint.PRIMARY);

			format = new Gnome.Scan.Format("png", N_("PNG Picture"), GETTEXT_PACKAGE, "image",
										   new string[] { "image/png" },
										   new string[] { "png" });
			option.install_format(format);
			this.install_option(option);

			option.value = "%s%c%s.%s".printf(GLib.Environment.get_user_special_dir(UserDirectory.PICTURES),
											  GLib.Path.DIR_SEPARATOR,
											  _("Scanned picture"),
											  format.suffixes[0]);
			option.notify["value"] += this.on_output_filename_changed;

			// gegl pipeline
			this._save = new Gegl.Node();
			this._save.set("operation", "gegl:png-save",
						   "compression", 9,
						   "bitdepth", 8);
			this.append_node(this._save);

			this.update_status(Gnome.Scan.Status.READY, null);
		}

		public override bool start_image()
		{
			string filename = "";
			string number;
			do {
				if (count == 0)
					number = "";
				else
					number = "-%d".printf(count);
				filename = "%s%c%s%s%s".printf(directory,
											   GLib.Path.DIR_SEPARATOR,
											   basename,
											   number,
											   suffix);
				count++;
			} while(GLib.FileUtils.test(filename, FileTest.EXISTS));

			debug("Saving to %s", filename);
			this._save.set("path", filename);

			return true;
		}

		private void on_output_filename_changed(OptionOutputFilename option, ParamSpec pspec)
		{
			// test directory existence?
			count = 0;
			directory = GLib.Path.get_dirname(option.value);
			basename = GLib.Path.get_basename(option.value);
			suffix = basename.rchr(-1, '.');
			if (suffix != null)
				basename = basename.replace(suffix, "");

			if (GLib.FileUtils.test(directory, FileTest.IS_DIR|FileTest.EXISTS) && basename.len() > 0 && suffix != null)
				this.update_status(Gnome.Scan.Status.READY, null);
			else
				this.update_status(Gnome.Scan.Status.UNCONFIGURED, null);
		}
	}
}
