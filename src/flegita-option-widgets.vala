/* Flegita - Scanner utility
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * Flegita is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Flegita is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Flegita. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */

using Config;
using GLib;
using Gtk;
using Gdk;
using Gnome.Scan;

namespace Flegita {
	enum FormatColumns {
		ICON,
		SUFFIX,
		N
	}

	public class OutputFilenameWidget : OptionWidget {
		private Entry entry;
		private FileChooser filechooser;
		private ListStore formats;
		private ComboBox combobox;
		private HashTable<string,string> format_paths;
		private bool inhibit = false;

		construct {
			CellRenderer renderer;
			OptionOutputFilename option = (OptionOutputFilename) this.option;
			TreeIter iter;

			this.no_label	= true;
			this.expand		= false;
			this.option.notify["value"] += this.parse_filename;
			Box box = new HBox(false, 6);
			this.pack_start(box, true, true, 0);

			// directory selector
			this.filechooser = new FileChooserButton(_("Select output directory"), FileChooserAction.SELECT_FOLDER);
			box.pack_start(this.filechooser, true, true, 0);

			// filename entry
			this.entry = new Entry();
			box.pack_start(this.entry, true, true, 0);

			// format store
			this.formats = new ListStore(FormatColumns.N, typeof(string), typeof(string));
			this.format_paths = new HashTable<string,string>(GLib.str_hash, GLib.str_equal);
			foreach(Gnome.Scan.Format format in option.list_formats()) {
				this.formats.append(out iter);
				this.formats.set(iter,
								 FormatColumns.ICON, format.icon_name,
								 FormatColumns.SUFFIX, format.suffixes[0]);
				var path = this.formats.get_path(iter);
				this.format_paths.insert(".%s".printf(format.suffixes[0]), path.to_string());
			}

			// format selector
			this.combobox = new ComboBox.with_model(this.formats);
			box.pack_start(this.combobox, false, false, 0);

			renderer = new CellRendererPixbuf();
			this.combobox.pack_start(renderer, false);
			this.combobox.set_attributes(renderer, "icon-name", FormatColumns.ICON);

			renderer = new CellRendererText();
			this.combobox.pack_start(renderer, true);
			this.combobox.set_attributes(renderer, "text", FormatColumns.SUFFIX);

			this.filechooser.selection_changed.connect(this.update_filename);
			this.entry.changed.connect(this.update_filename);
			this.combobox.changed.connect(this.update_filename);
			this.parse_filename();
		}

		private void update_filename()
		{
			if (this.inhibit)
				return;

			var option = this.option as OptionOutputFilename;
			string suffix;
			TreeIter iter;
			bool format_selected = this.combobox.get_active_iter(out iter);
			return_if_fail(format_selected);
			this.formats.get(iter, FormatColumns.SUFFIX, out suffix);

			this.inhibit = true;
			option.value = "%s%c%s.%s".printf(this.filechooser.get_filename(),
											  GLib.Path.DIR_SEPARATOR,
											  this.entry.get_text(),
											  suffix);
			this.inhibit = false;
		}

		private void parse_filename()
		{
			if (this.inhibit)
				return;

			this.inhibit = true;
			TreeIter iter;
			var option = this.option as OptionOutputFilename;
			string filename = option.value;
			if (filename == null || filename.len() == 0)
				return;

			this.filechooser.set_current_folder(GLib.Path.get_dirname(filename));

			string basename = GLib.Path.get_basename(filename);
			string suffix = basename.rchr(-1, '.');
			if (suffix == null) {
				warning("unable to parse suffix in %s", basename);
				return;
			}

			string path_string = this.format_paths.lookup(suffix);
			if (!this.formats.get_iter_from_string(out iter, path_string)) {
				warning("Invalid %s suffix", suffix);
				return;
			}
			this.combobox.set_active_iter(iter);

			var parts = basename.split(suffix, -1);
			this.entry.set_text(parts[0]);

			while(Gdk.events_pending())
				Gtk.main_iteration();

			this.inhibit = false;
		}
	}
}
