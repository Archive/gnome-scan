/* Flegita - Scanner utility
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * Flegita is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Flegita is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Flegita. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using Gnome.Scan;

namespace Flegita {
	public class OptionOutputFilename : Gnome.Scan.Option {
		public string? value	{get; set;}

		private string directory;
		private string filename;
		private Gnome.Scan.Format? format = null;

		private SList<unowned Gnome.Scan.Format> formats;

		public OptionOutputFilename(string name, string title, string desc, string domain, string group, OptionHint hint)
		{
			this.name	= name;
			this.title	= title;
			this.desc	= desc;
			this.domain	= domain;
			this.group	= group;
			this.hint	= hint;
		}

		public void install_format(Gnome.Scan.Format format)
		{
			this.formats.append(format);
			if (this.format == null) {
				this.format = this.formats.nth_data(0);
				this.update_value();
			}
		}

		public unowned SList<Gnome.Scan.Format> list_formats()
		{
			return this.formats;
		}

		private void update_value()
		{
			if (this.directory != null && this.filename != null	&& this.format != null)
				this.value = this.directory.concat(GLib.Path.DIR_SEPARATOR_S, this.filename, ".", this.format.suffixes[0]);
			else
				this.value = null;
		}
	}
}
