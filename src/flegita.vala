/* Flegita - Scanner utility
 * Copyright © 2006-2008  Étienne Bersac <bersace@gnome.org>
 *
 * Flegita is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Flegita is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Flegita. If not, write to:
 *
 *	the Free Software Foundation, Inc.
 *	51 Franklin Street, Fifth Floor
 *	Boston, MA 02110-1301, USA
 */
 
using Gtk;
using Gnome.Scan;
using Flegita;

public int main(string[] args) {
	Job job;
	Gnome.Scan.Dialog dialog;
	Flegita.Sink sink;
	Gdk.Screen screen;
	double screen_resolution;
	Value default_res;
	PaperSize ps;
	Value value;
	Gnome.Scan.EnumValue evalue;
	SList<Gnome.Scan.EnumValue?> ps_list = null;

	GLib.Environment.set_prgname("flegita");
	GLib.Environment.set_application_name(_("Scanner Utility"));

	Gnome.Scan.init(ref args);
	Gtk.init(ref args);

	Gnome.Scan.option_manager.register_rule_by_type(typeof(Flegita.OptionOutputFilename), typeof(Flegita.OutputFilenameWidget));

	sink = new Flegita.Sink();
	job = new Gnome.Scan.Job(sink);

	// Préselect résolution.
	default_res = Value(typeof(double));
	screen = Gdk.Screen.get_default();
	screen_resolution = screen.get_resolution();
	if (screen_resolution != -1)
		default_res.set_double(screen.get_resolution());
	else
		default_res.set_double(150);
	job.register_preselection(new PreselValue("resolution", default_res));

	// Préselect paper size
	string[] formats = new string[] {"om_small-photo", "iso_dl"};
	foreach(string format in formats) {
		ps = new PaperSize(format);
		value = Value(typeof(Gtk.PaperSize));
		value.set_boxed(ps);
		evalue = Gnome.Scan.EnumValue(value, ps.get_display_name(), null);
		ps_list.append(evalue);
	}
	job.register_preselection(new PreselEnumValues("paper-size", ps_list));

	dialog = new Gnome.Scan.Dialog(null, job);
	dialog.run();

	return 0;
}
