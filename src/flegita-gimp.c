/* FlegitaGimp - Gimp Scan Plugin
 *
 * main.c
 *
 * Copyright © 2006 Étienne Bersac
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gnome-scan.h>
#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#include <glib/gi18n.h>
#include "flegita-gimp-sink.h"

/*  Local function prototypes  */
#define	PROCEDURE_SCAN	"flegita_scan"
#define PROCEDURE_SCAN_AS_LAYER	"flegita_scan_as_layer"

static void	init			(void);
static void	quit			(void);
static void	query			(void);

static void	run			(const gchar      *name,
					 gint              nparams,
					 const GimpParam  *param,
					 gint             *nreturn_vals,
					 GimpParam       **return_vals);



/*  Local variables  */

GimpPlugInInfo PLUG_IN_INFO = {
  NULL,  /* init_proc  */
  NULL,  /* quit_proc  */
  query, /* query_proc */
  run,   /* run_proc   */
};

MAIN ()

static void
query (void)
{
  gchar *help_path;
  gchar *help_uri;

  static GimpParamDef scan_args[] = {
    { GIMP_PDB_INT32,    "run_mode",   "Interactive, non-interactive"    },
  };

  static GimpParamDef scan_return[] = {
    { GIMP_PDB_STATUS,   "status",     "Return status"                 },
    { GIMP_PDB_IMAGE,    "image",      "New image"                     },
  };

  static GimpParamDef scan_as_layer_args[] = {
    { GIMP_PDB_INT32,    "run_mode",   "Interactive, non-interactive"    },
    { GIMP_PDB_IMAGE,    "image",      "Input image"                     },
    { GIMP_PDB_DRAWABLE, "drawable",   "Input drawable"                  },
  };

  gimp_plugin_domain_register (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);

  help_path = g_build_filename (DATADIR, "help", NULL);
  help_uri = g_filename_to_uri (help_path, NULL, NULL);
  g_free (help_path);

  gimp_install_procedure (PROCEDURE_SCAN,
			  _("Scan a new image."),
			  _("Help"),
			  "Étienne Bersac <bersace03@laposte.net>",
			  "Étienne Bersac <bersace03@laposte.net>",
			  "2006-2007",
			  _("Scan ..."),
			  NULL,
			  GIMP_PLUGIN,
			  G_N_ELEMENTS (scan_args), G_N_ELEMENTS (scan_return),
			  scan_args, scan_return);

  gimp_plugin_menu_register (PROCEDURE_SCAN, "<Toolbox>/File/Acquire");
  gimp_plugin_icon_register (PROCEDURE_SCAN, GIMP_ICON_TYPE_IMAGE_FILE, ACTION_ICON_DIR "/scan.svg");

  gimp_install_procedure (PROCEDURE_SCAN_AS_LAYER,
			  _("Scan picture as new layer..."),
			  _("Help"),
			  "Étienne Bersac <bersace03@laposte.net>",
			  "Étienne Bersac <bersace03@laposte.net>",
			  "2006-2007",
			  _("Scan as Layer..."),
			  "RGB*",
			  GIMP_PLUGIN,
			  G_N_ELEMENTS (scan_as_layer_args), G_N_ELEMENTS (scan_return),
			  scan_as_layer_args, scan_return);

  gimp_plugin_menu_register (PROCEDURE_SCAN_AS_LAYER, "<Image>/File/Open");
  gimp_plugin_icon_register (PROCEDURE_SCAN_AS_LAYER, GIMP_ICON_TYPE_IMAGE_FILE, ACTION_ICON_DIR "/scan-as-layer.svg");
}

static void
run (const gchar      *name,
     gint              n_params,
     const GimpParam  *param,
     gint             *nreturn_vals,
     GimpParam       **return_vals)
{
  static GimpParam   values[2];
  GimpDrawable      *drawable;
  gint32             image_ID;
  gboolean	     is_new;
  GimpRunMode        run_mode;
  GimpPDBStatusType  status = GIMP_PDB_SUCCESS;
  gint 		     n_layers;

  gimp_ui_init (PACKAGE, FALSE);
  gnome_scan_init(NULL, NULL);
  textdomain (GETTEXT_PACKAGE);
  gdouble xres, yres, res;

  /* if we are scanning, create a new image */
  g_debug (G_STRLOC " " PROCEDURE_SCAN);
  if (g_str_equal (name, PROCEDURE_SCAN)) {
    image_ID = gimp_image_new (1, 1, GIMP_RGB);
    is_new = TRUE;
  }
  /* else use the existing one. */
  else {
    image_ID = param[1].data.d_image;
    is_new = FALSE;
    gimp_drawable_detach (gimp_drawable_get (param[2].data.d_drawable));
  }

  GnomeScanSettings* settings = gnome_scan_settings_new();
  GnomeScanSink *sink = flegita_gimp_sink_new(image_ID, is_new);
  GnomeScanJob *job = gnome_scan_job_new(settings, sink);

  /* set default scan resolution from existing image */
  if (!is_new) {
    if (gimp_image_get_resolution (image_ID, &xres, &yres)) {
      res = MAX (xres, yres);
      gnome_scan_settings_set_double (settings, "resolution", res);
    }
  }

  gnome_scan_job_set_sink (job, sink);
  gnome_scan_job_set_settings(job, settings);
  GtkWidget*dialog = gnome_scan_dialog_new (NULL, job);
  gnome_scan_dialog_run (GNOME_SCAN_DIALOG (dialog));

  gimp_image_get_layers (image_ID,
			 &n_layers);

  if (is_new) {
    /* if we have a layer, then display */
    if (n_layers == 1) {
      gimp_display_new (image_ID);
    }
    /* else, the scan has been cancel */
    else {
      gimp_image_delete (image_ID);
    }
  }
  gimp_displays_flush ();

  values[1].type = GIMP_PDB_IMAGE;
  values[1].data.d_image = image_ID;
  values[0].type = GIMP_PDB_STATUS;
  values[0].data.d_status = status;

  *nreturn_vals = 1;
  *return_vals  = values;
}
