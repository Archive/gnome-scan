[CCode (cprefix="", lower_case_prefix="", cheader_filename="config.h")]
namespace Config {
	[CCode (cname = "GETTEXT_PACKAGE")]
    public const string GETTEXT_PACKAGE;
	[CCode (cname = "MODULE_DIR")]
    public const string MODULE_DIR;
	[CCode (cname = "PACKAGE_VERSION")]
	public const string VERSION;
}