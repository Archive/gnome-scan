# Danish translation for gnome-scan.
# Copyright (C) 2010 gnome-scan og nedenstående oversættere.
# This file is distributed under the same license as the gnome-scan package.
# Joe Hansen (joedalton2@yahoo.dk), 2009, 2010.
# Korrekturlæst Kris Thomsen, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-scan master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-11-17 15:20+0100\n"
"PO-Revision-Date: 2010-11-16 02:19+0000\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/gnome-scan-common.vala:42
msgid "px"
msgstr "px"

#: ../lib/gnome-scan-common.vala:44
msgid "pt"
msgstr "p."

#: ../lib/gnome-scan-common.vala:46
msgid "mm"
msgstr "mm"

#. translators: Shortname for Inch unit
#: ../lib/gnome-scan-common.vala:49
msgid "in"
msgstr "\""

#: ../lib/gnome-scan-common.vala:51
msgid "bit"
msgstr "bit"

#: ../lib/gnome-scan-common.vala:53
msgid "dpi"
msgstr "dpi"

# µs = mikrosekund
#: ../lib/gnome-scan-common.vala:57
msgid "µs"
msgstr "µs"

#: ../lib/gnome-scan-dialog.vala:75
msgid "Scan"
msgstr "Skan"

#: ../lib/gnome-scan-dialog.vala:201
msgid "_General"
msgstr "_Generelt"

#: ../lib/gnome-scan-dialog.vala:225
msgid "Acquisition"
msgstr "Erhvervelse"

#: ../lib/gnome-scan-dialog.vala:226
msgid "The software now acquires and processes images according to settings."
msgstr "Programmet erhverver og bearbejder nu billeder jævnfør opsætning."

#: ../lib/gnome-scan-dialog.vala:243
msgid "_Advanced"
msgstr "_Avanceret"

#: ../lib/gnome-scan-dialog.vala:246
msgid "_Processing"
msgstr "_Behandler"

#: ../lib/gnome-scan-dialog.vala:249
msgid "_Output"
msgstr "_Uddata"

#: ../lib/gnome-scan-node.vala:81
msgid "Unknown"
msgstr "Ukendt"

#: ../lib/gnome-scan-node.vala:84
msgid "Failed"
msgstr "Mislykkedes"

#: ../lib/gnome-scan-node.vala:87
msgid "Initializing"
msgstr "Initialiserer"

#: ../lib/gnome-scan-node.vala:90
msgid "Unconfigured"
msgstr "Ukonfigureret"

#: ../lib/gnome-scan-node.vala:93
msgid "Ready"
msgstr "Klar"

#: ../lib/gnome-scan-node.vala:96
msgid "Processing"
msgstr "Arbejder"

#: ../lib/gnome-scan-node.vala:99
msgid "Done"
msgstr "Færdig"

#. translator: %s is the name of an option and is
#. prepended before the option widget. Accord ":" to
#. locale typographic rules.
#: ../lib/gnome-scan-option-box.vala:86
#, c-format
msgid "%s:"
msgstr "%s:"

#: ../lib/gnome-scan-scanner-selector.vala:70
msgid "Device"
msgstr "Enhed"

#: ../lib/gnome-scan-scanner-selector.vala:74
msgid "Status"
msgstr "Status"

#: ../modules/gsane/gsane-option-area.c:65
msgid "Origin"
msgstr "Oprindelse"

#: ../modules/gsane/gsane-option-area.c:65
msgid "Coordinate of the top left corner of the paper."
msgstr "Koordinat på det øverste venstre hjørne af papiret."

#. Translator: manual selection display label
#: ../modules/gsane/gsane-option-area.c:197
msgid "Manual"
msgstr "Manual"

#. Translator: maximum paper size display label
#: ../modules/gsane/gsane-option-area.c:203
msgid "Maximum"
msgstr "Maksimum"

#: ../modules/gsane/gsane-option-area.c:232
msgid "Paper-Size"
msgstr "Papirstørrelse"

#: ../modules/gsane/gsane-option-area.c:280
msgid "Page Orientation"
msgstr "Sideretning"

#: ../modules/gsane/gsane-option-area.c:280
msgid "Page orientation"
msgstr "Sideretning"

#.
#. * GSaneOptionSource has many jobs to fill.
#. *
#. * - It expose source option as a primary option, making some changes
#. *   to void backend inconsistency.
#. *
#. * - It knows whether the ADF has been selected in order to enable
#. *   mass acquisition.
#. *
#. * TODO:
#. *
#. * - Support ADF boolean option.
#. *
#. * - Support duplex scan (boolean and special source).
#.
#. some unlisted sources :
#. *
#. * - Slide(s)
#. * - TMA Slides
#. * - ADF Back
#. * - ADF Duplex (should be mapped to duplex boolean)
#.
#. List of known source used for flatbed
#: ../modules/gsane/gsane-option-source.c:62
msgid "Flatbed"
msgstr "Flatbed (lad)"

#. List of known source used for Automatic Document Feeder
#: ../modules/gsane/gsane-option-source.c:72
msgid "Automatic Document Feeder"
msgstr "Automatisk dokumentføder"

#. List of known source used for Transparency Adapter
#: ../modules/gsane/gsane-option-source.c:83
msgid "Transparency Adapter"
msgstr "Gennemsigtig adapter"

#. List of known source used for Negative Adapter
#: ../modules/gsane/gsane-option-source.c:93
msgid "Negative Adapter"
msgstr "Negativadapter"

#: ../modules/gsane/gsane-option-source.c:149
msgid "Source"
msgstr "Kilde"

#: ../modules/gsfile/gsfile-filenames-widget.c:88
msgid "Select files"
msgstr "Vælg filer"

#: ../modules/gsfile/gsfile-filenames-widget.c:101
msgid "Supported formats"
msgstr "Understøttede formater"

#: ../modules/gsfile/gsfile-filenames-widget.c:298
msgid "Preview"
msgstr "Forhåndsvisning"

#: ../modules/gsfile/gsfile-filenames-widget.c:305
msgid "Filename"
msgstr "Filnavn"

#. translator: this is the name of the file
#. loader backend which allow user to scan
#. a list a images
#: ../modules/gsfile/gsfile-scanner.c:49
msgid "Files"
msgstr "Filer"

#: ../modules/gsfile/gsfile-scanner.c:50
msgid "Import from files."
msgstr "Importer fra filer."

#: ../modules/gsfile/gsfile-scanner.c:115
msgid "PNG picture"
msgstr "PNG-billede"

#: ../modules/gsfile/gsfile-scanner.c:126
msgid "JPEG picture"
msgstr "JPEG-billede"

#. directory selector
#: ../src/flegita-option-widgets.vala:55
msgid "Select output directory"
msgstr "Vælg uddatamappe"

#: ../src/flegita-sink.vala:40
msgid "Output Filename"
msgstr "Uddatafilnavn"

#: ../src/flegita-sink.vala:40
msgid "Output filename"
msgstr "Uddatafilnavn"

#: ../src/flegita-sink.vala:42
msgid "PNG Picture"
msgstr "PNG-billede"

#: ../src/flegita-sink.vala:50
msgid "Scanned picture"
msgstr "Skannet billede"

#: ../src/flegita.vala:39 ../flegita.desktop.in.h:1
msgid "Scanner Utility"
msgstr "Skannerprogram"

#: ../src/flegita-gimp.c:89
msgid "Scan a new image."
msgstr "Skan et nyt billede."

#: ../src/flegita-gimp.c:90 ../src/flegita-gimp.c:105
msgid "Help"
msgstr "Hjælp"

#: ../src/flegita-gimp.c:94
msgid "Scan ..."
msgstr "Skan ..."

#: ../src/flegita-gimp.c:104
msgid "Scan picture as new layer..."
msgstr "Skan billede som nyt lag..."

#: ../src/flegita-gimp.c:109
msgid "Scan as Layer..."
msgstr "Skan som lag..."

#: ../src/flegita-gimp-sink.c:77
msgid "Layer"
msgstr "Lag"

#: ../src/flegita-gimp-sink.c:78
msgid "New layer name"
msgstr "Nyt lagnavn"

#: ../src/flegita-gimp-sink.c:80
msgid "Scanned image"
msgstr "Skannet billede"

#: ../flegita.desktop.in.h:2
msgid "Simply scan images"
msgstr "Skan nemt billeder"
