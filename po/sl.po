# Slovenijan translation of gnome-scan.
# Copyright (C) 2005-2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the program package.
#
# Matej Urbančič <mateju@svn.gnome.org>, 2007 - 2009.
# Andrej Žnidaršič <andrej.znidarsic@gmail.com>, 2009 - 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: libgnomekbd\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-scan&component=general\n"
"POT-Creation-Date: 2010-03-15 18:04+0000\n"
"PO-Revision-Date: 2010-05-14 21:46+0100\n"
"Last-Translator: Matej Urbančič <mateju@svn.gnome.org>\n"
"Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);\n"
"X-Poedit-Country: SLOVENIA\n"
"X-Poedit-Language: Slovenian\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../lib/gnome-scan-common.vala:42
msgid "px"
msgstr "točk"

#: ../lib/gnome-scan-common.vala:44
msgid "pt"
msgstr "točk pt"

#: ../lib/gnome-scan-common.vala:46
msgid "mm"
msgstr "mm"

#. translators: Shortname for Inch unit
#: ../lib/gnome-scan-common.vala:49
msgid "in"
msgstr "palcev"

#: ../lib/gnome-scan-common.vala:51
msgid "bit"
msgstr "bitov"

#: ../lib/gnome-scan-common.vala:53
msgid "dpi"
msgstr "dpi"

#: ../lib/gnome-scan-common.vala:57
msgid "µs"
msgstr "µs"

#: ../lib/gnome-scan-dialog.vala:75
msgid "Scan"
msgstr "Optično preberi"

#: ../lib/gnome-scan-dialog.vala:201
msgid "_General"
msgstr "_Splošno"

#: ../lib/gnome-scan-dialog.vala:225
msgid "Acquisition"
msgstr "Zajemanje"

#: ../lib/gnome-scan-dialog.vala:226
msgid "The software now acquires and processes images according to settings."
msgstr "Program zahteva obdelavo slik glede na nastavitve."

#: ../lib/gnome-scan-dialog.vala:243
msgid "_Advanced"
msgstr "_Napredno"

#: ../lib/gnome-scan-dialog.vala:246
msgid "_Processing"
msgstr "_Izvajanje"

#: ../lib/gnome-scan-dialog.vala:249
msgid "_Output"
msgstr "_Izpis"

#: ../lib/gnome-scan-node.vala:81
msgid "Unknown"
msgstr "Neznano"

#: ../lib/gnome-scan-node.vala:84
msgid "Failed"
msgstr "Spodletelo"

#: ../lib/gnome-scan-node.vala:87
msgid "Initializing"
msgstr "Začenjanje"

#: ../lib/gnome-scan-node.vala:90
msgid "Unconfigured"
msgstr "Ni nastavljeno"

#: ../lib/gnome-scan-node.vala:93
msgid "Ready"
msgstr "Pripravljeno"

#: ../lib/gnome-scan-node.vala:96
msgid "Processing"
msgstr "Izvajanje"

#: ../lib/gnome-scan-node.vala:99
msgid "Done"
msgstr "Končano"

#. translator: %s is the name of an option and is
#. prepended before the option widget. Accord ":" to
#. locale typographic rules.
#: ../lib/gnome-scan-option-box.vala:86
#, c-format
msgid "%s:"
msgstr "%s:"

#: ../lib/gnome-scan-scanner-selector.vala:70
msgid "Device"
msgstr "Naprava"

#: ../lib/gnome-scan-scanner-selector.vala:74
msgid "Status"
msgstr "Stanje"

#: ../modules/gsane/gsane-option-area.c:65
msgid "Origin"
msgstr "Izvor"

#: ../modules/gsane/gsane-option-area.c:65
msgid "Coordinate of the top left corner of the paper."
msgstr "Koordinate vrhnjega levega roba strani."

#. Translator: manual selection display label
#: ../modules/gsane/gsane-option-area.c:197
msgid "Manual"
msgstr "Ročno"

#. Translator: maximum paper size display label
#: ../modules/gsane/gsane-option-area.c:203
msgid "Maximum"
msgstr "Največja velikost"

#: ../modules/gsane/gsane-option-area.c:232
msgid "Paper-Size"
msgstr "Velikost papirja"

#: ../modules/gsane/gsane-option-area.c:280
msgid "Page Orientation"
msgstr "Usmerjenost strani"

#: ../modules/gsane/gsane-option-area.c:280
msgid "Page orientation"
msgstr "Usmerjenost strani"

#.
#. * GSaneOptionSource has many jobs to fill.
#. *
#. * - It expose source option as a primary option, making some changes
#. *   to void backend inconsistency.
#. *
#. * - It knows whether the ADF has been selected in order to enable
#. *   mass acquisition.
#. *
#. * TODO:
#. *
#. * - Support ADF boolean option.
#. *
#. * - Support duplex scan (boolean and special source).
#.
#. some unlisted sources :
#. *
#. * - Slide(s)
#. * - TMA Slides
#. * - ADF Back
#. * - ADF Duplex (should be mapped to duplex boolean)
#.
#. List of known source used for flatbed
#: ../modules/gsane/gsane-option-source.c:62
msgid "Flatbed"
msgstr "Ploski bralnik"

#. List of known source used for Automatic Document Feeder
#: ../modules/gsane/gsane-option-source.c:72
msgid "Automatic Document Feeder"
msgstr "Samodejni vir dokumentov"

#. List of known source used for Transparency Adapter
#: ../modules/gsane/gsane-option-source.c:83
msgid "Transparency Adapter"
msgstr "Vmesnik prosojnosti"

#. List of known source used for Negative Adapter
#: ../modules/gsane/gsane-option-source.c:93
msgid "Negative Adapter"
msgstr "Vmesnik negativa"

#: ../modules/gsane/gsane-option-source.c:149
msgid "Source"
msgstr "Vir"

#: ../modules/gsfile/gsfile-filenames-widget.c:88
msgid "Select files"
msgstr "Izbor datotek"

#: ../modules/gsfile/gsfile-filenames-widget.c:101
msgid "Supported formats"
msgstr "Podprti zapisi"

#: ../modules/gsfile/gsfile-filenames-widget.c:298
msgid "Preview"
msgstr "Predogled"

#: ../modules/gsfile/gsfile-filenames-widget.c:305
msgid "Filename"
msgstr "Ime datoteke"

#. translator: this is the name of the file
#. loader backend which allow user to scan
#. a list a images
#: ../modules/gsfile/gsfile-scanner.c:49
msgid "Files"
msgstr "Datoteke"

#: ../modules/gsfile/gsfile-scanner.c:50
msgid "Import from files."
msgstr "Uvoz iz datotek."

#: ../modules/gsfile/gsfile-scanner.c:115
msgid "PNG picture"
msgstr "Slika PNG"

#: ../modules/gsfile/gsfile-scanner.c:126
msgid "JPEG picture"
msgstr "Slika JPEG"

#. directory selector
#: ../src/flegita-option-widgets.vala:55
msgid "Select output directory"
msgstr "Izbor odvodne mape"

#: ../src/flegita-sink.vala:40
msgid "Output Filename"
msgstr "Odvodno ime datoteke"

#: ../src/flegita-sink.vala:40
msgid "Output filename"
msgstr "Odvodno ime datoteke"

#: ../src/flegita-sink.vala:42
msgid "PNG Picture"
msgstr "Slika PNG"

#: ../src/flegita-sink.vala:50
msgid "Scanned picture"
msgstr "Optično prebrana slika"

#: ../src/flegita.vala:39
#: ../flegita.desktop.in.h:1
msgid "Scanner Utility"
msgstr "Pripomoček za optično branje"

#: ../src/flegita-gimp.c:89
msgid "Scan a new image."
msgstr "Optično preberi novo sliko."

#: ../src/flegita-gimp.c:90
#: ../src/flegita-gimp.c:105
msgid "Help"
msgstr "Pomoč"

#: ../src/flegita-gimp.c:94
msgid "Scan ..."
msgstr "Optično preberi ..."

#: ../src/flegita-gimp.c:104
msgid "Scan picture as new layer..."
msgstr "Optično preberi sliko kot novo plast ..."

#: ../src/flegita-gimp.c:109
msgid "Scan as Layer..."
msgstr "Optično preberi kot plast ..."

#: ../src/flegita-gimp-sink.c:77
msgid "Layer"
msgstr "Plast"

#: ../src/flegita-gimp-sink.c:78
msgid "New layer name"
msgstr "Ime nove plasti"

#: ../src/flegita-gimp-sink.c:80
msgid "Scanned image"
msgstr "Optično prebrana slika"

#: ../flegita.desktop.in.h:2
msgid "Simply scan images"
msgstr "Enostavno optično branje slik"

#~ msgid "Configure printing"
#~ msgstr "Nastavitev tiskanja"
#~ msgid "Select directory"
#~ msgstr "Izbor mape"
