# Chinese translations for gnome-scan package
# gnome-scan 软件包的简体中文翻译.
# Copyright (C) 2009,2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-scan package.
#
# Aron Xu <happyaron.xu@gmail.com>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: gnome-scan master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"scan&component=general\n"
"POT-Creation-Date: 2010-07-11 17:19+0000\n"
"PO-Revision-Date: 2010-07-31 09:22+0800\n"
"Last-Translator: Aron Xu <happyaron.xu@gmail.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../lib/gnome-scan-common.vala:42
msgid "px"
msgstr "像素"

#: ../lib/gnome-scan-common.vala:44
msgid "pt"
msgstr "点"

#: ../lib/gnome-scan-common.vala:46
msgid "mm"
msgstr "毫米"

#. translators: Shortname for Inch unit
#: ../lib/gnome-scan-common.vala:49
msgid "in"
msgstr "英寸"

#: ../lib/gnome-scan-common.vala:51
msgid "bit"
msgstr "位"

#: ../lib/gnome-scan-common.vala:53
msgid "dpi"
msgstr "dpi"

#: ../lib/gnome-scan-common.vala:57
msgid "µs"
msgstr "微秒"

#: ../lib/gnome-scan-dialog.vala:75
msgid "Scan"
msgstr "扫描"

#: ../lib/gnome-scan-dialog.vala:201
msgid "_General"
msgstr "常规(_G)"

#: ../lib/gnome-scan-dialog.vala:225
msgid "Acquisition"
msgstr "结果"

#: ../lib/gnome-scan-dialog.vala:226
msgid "The software now acquires and processes images according to settings."
msgstr "按照设置软件将处理结果文件。"

#: ../lib/gnome-scan-dialog.vala:243
msgid "_Advanced"
msgstr "高级(_A)"

#: ../lib/gnome-scan-dialog.vala:246
msgid "_Processing"
msgstr "处理(_P)"

#: ../lib/gnome-scan-dialog.vala:249
msgid "_Output"
msgstr "输出(_O)"

#: ../lib/gnome-scan-node.vala:81
msgid "Unknown"
msgstr "未知"

#: ../lib/gnome-scan-node.vala:84
msgid "Failed"
msgstr "失败"

#: ../lib/gnome-scan-node.vala:87
msgid "Initializing"
msgstr "正在初始化"

#: ../lib/gnome-scan-node.vala:90
msgid "Unconfigured"
msgstr "未配置"

#: ../lib/gnome-scan-node.vala:93
msgid "Ready"
msgstr "就绪"

#: ../lib/gnome-scan-node.vala:96
msgid "Processing"
msgstr "正在处理"

#: ../lib/gnome-scan-node.vala:99
msgid "Done"
msgstr "完成"

#. translator: %s is the name of an option and is
#. prepended before the option widget. Accord ":" to
#. locale typographic rules.
#: ../lib/gnome-scan-option-box.vala:86
#, c-format
msgid "%s:"
msgstr "%s："

#: ../lib/gnome-scan-scanner-selector.vala:70
msgid "Device"
msgstr "设备"

#: ../lib/gnome-scan-scanner-selector.vala:74
msgid "Status"
msgstr "状态"

#: ../modules/gsane/gsane-option-area.c:65
msgid "Origin"
msgstr "来源"

#: ../modules/gsane/gsane-option-area.c:65
msgid "Coordinate of the top left corner of the paper."
msgstr "以纸张左上角为原点的坐标。"

#. Translator: manual selection display label
#: ../modules/gsane/gsane-option-area.c:197
msgid "Manual"
msgstr "手工"

#. Translator: maximum paper size display label
#: ../modules/gsane/gsane-option-area.c:203
msgid "Maximum"
msgstr "最大"

#: ../modules/gsane/gsane-option-area.c:232
msgid "Paper-Size"
msgstr "纸张大学"

#: ../modules/gsane/gsane-option-area.c:280
msgid "Page Orientation"
msgstr "页面定向"

#: ../modules/gsane/gsane-option-area.c:280
msgid "Page orientation"
msgstr "页面定向"

#.
#. * GSaneOptionSource has many jobs to fill.
#. *
#. * - It expose source option as a primary option, making some changes
#. *   to void backend inconsistency.
#. *
#. * - It knows whether the ADF has been selected in order to enable
#. *   mass acquisition.
#. *
#. * TODO:
#. *
#. * - Support ADF boolean option.
#. *
#. * - Support duplex scan (boolean and special source).
#.
#. some unlisted sources :
#. *
#. * - Slide(s)
#. * - TMA Slides
#. * - ADF Back
#. * - ADF Duplex (should be mapped to duplex boolean)
#.
#. List of known source used for flatbed
#: ../modules/gsane/gsane-option-source.c:62
msgid "Flatbed"
msgstr "平板式"

#. List of known source used for Automatic Document Feeder
#: ../modules/gsane/gsane-option-source.c:72
msgid "Automatic Document Feeder"
msgstr "自动填充文档"

#. List of known source used for Transparency Adapter
#: ../modules/gsane/gsane-option-source.c:83
msgid "Transparency Adapter"
msgstr "自适应透明度"

#. List of known source used for Negative Adapter
#: ../modules/gsane/gsane-option-source.c:93
msgid "Negative Adapter"
msgstr "被动适应"

#: ../modules/gsane/gsane-option-source.c:149
msgid "Source"
msgstr "源"

#: ../modules/gsfile/gsfile-filenames-widget.c:88
msgid "Select files"
msgstr "选择文件"

#: ../modules/gsfile/gsfile-filenames-widget.c:101
msgid "Supported formats"
msgstr "支持的格式"

#: ../modules/gsfile/gsfile-filenames-widget.c:298
msgid "Preview"
msgstr "预览"

#: ../modules/gsfile/gsfile-filenames-widget.c:305
msgid "Filename"
msgstr "文件名"

#. translator: this is the name of the file
#. loader backend which allow user to scan
#. a list a images
#: ../modules/gsfile/gsfile-scanner.c:49
msgid "Files"
msgstr "文件"

#: ../modules/gsfile/gsfile-scanner.c:50
msgid "Import from files."
msgstr "从文件导入。"

#: ../modules/gsfile/gsfile-scanner.c:115
msgid "PNG picture"
msgstr "PNG 图片"

#: ../modules/gsfile/gsfile-scanner.c:126
msgid "JPEG picture"
msgstr "JPEG 图片"

#. directory selector
#: ../src/flegita-option-widgets.vala:55
msgid "Select output directory"
msgstr "选择输出目录"

#: ../src/flegita-sink.vala:40
msgid "Output Filename"
msgstr "输出文件名"

#: ../src/flegita-sink.vala:40
msgid "Output filename"
msgstr "输出文件名"

#: ../src/flegita-sink.vala:42
msgid "PNG Picture"
msgstr "PNG 图片"

#: ../src/flegita-sink.vala:50
msgid "Scanned picture"
msgstr "已扫描图片"

#: ../src/flegita.vala:39 ../flegita.desktop.in.h:1
msgid "Scanner Utility"
msgstr "扫描仪工具集"

#: ../src/flegita-gimp.c:89
msgid "Scan a new image."
msgstr "扫描新图像。"

#: ../src/flegita-gimp.c:90 ../src/flegita-gimp.c:105
msgid "Help"
msgstr "帮助"

#: ../src/flegita-gimp.c:94
msgid "Scan ..."
msgstr "扫描..."

#: ../src/flegita-gimp.c:104
msgid "Scan picture as new layer..."
msgstr "将图片扫描为新图层..."

#: ../src/flegita-gimp.c:109
msgid "Scan as Layer..."
msgstr "作为图层扫描..."

#: ../src/flegita-gimp-sink.c:77
msgid "Layer"
msgstr "图层"

#: ../src/flegita-gimp-sink.c:78
msgid "New layer name"
msgstr "新图层名称"

#: ../src/flegita-gimp-sink.c:80
msgid "Scanned image"
msgstr "已扫描图像"

#: ../flegita.desktop.in.h:2
msgid "Simply scan images"
msgstr "扫描图片"

#~ msgid "Configure printing"
#~ msgstr "配置打印"

#~ msgid "Select directory"
#~ msgstr "选择目录"
